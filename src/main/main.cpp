
#include <array>
#include <cstdlib>
#include <variant>

#include <anchor/brush.hpp>
#include <anchor/component/button.hpp>
#include <anchor/component/scroll.hpp>
#include <anchor/component/text.hpp>
#include <anchor/event.hpp>
#include <anchor/font.hpp>
#include <anchor/layout/anchor_layout.hpp>
#include <anchor/platform/window.hpp>
#include <anchor/unit/color.hpp>

using namespace anchor;

struct Stripe {
	struct Data {
		Color color;

		explicit Data(const Color color) : color{color} {}
	};

	static void repaint_handler(Component &self, Data &data, Brush &brush) noexcept {
		const auto bounds = self.get_absolute_bounds();
		brush.set_clip(bounds);
		brush.set_color(data.color);
		brush.fill_rectangle(bounds);
	}

	constexpr static auto event_handler = propagate_event<Data>;
};

struct Striper {
	struct Data : public Button::Data {

		int count = 0;
		Component &rainbow;
		Component *last_stripe = nullptr;

		explicit Data(Component &rainbow) noexcept : rainbow{rainbow} {}
	};

	static void repaint_handler(Component &self, Data &data, Brush &brush) noexcept {
		Button::repaint_handler(self, data, brush);
	}

	static EventStatus event_handler(Component &self, Data &data, const Event &e) {
		std::visit(match_event {
			[&](const MouseClickEvent &e) noexcept {
				if (e.button == MouseClickEvent::Button::LEFT && e.type == MouseClickEvent::Type::RELEASE) {
					constexpr std::array COLORS{colors::CYAN, colors::GREEN, colors::MAGENTA, colors::RED, colors::YELLOW, colors::WHITE};
					constexpr Dimension STRIPE_OFFSET{0, 100};

					++data.count;
					data.count %= COLORS.size();
					const auto color = COLORS[data.count];

					auto &layout = *data.rainbow.get_layout<AnchorLayout>();
					auto &stripe = data.rainbow.add(Component::create<Stripe>(color));
					if (data.last_stripe == nullptr) {
						layout.set_point(stripe, Anchor::TOP_LEFT, data.rainbow, Anchor::TOP_LEFT);
						layout.set_point(stripe, Anchor::BOTTOM_RIGHT, data.rainbow, Anchor::TOP_RIGHT, STRIPE_OFFSET);
					} else {
						layout.set_point(stripe, Anchor::TOP_LEFT, *data.last_stripe, Anchor::BOTTOM_LEFT);
						layout.set_point(stripe, Anchor::BOTTOM_RIGHT, *data.last_stripe, Anchor::BOTTOM_RIGHT, STRIPE_OFFSET);
					}
					data.last_stripe = &stripe;
				}
				return EventStatus::CONSUMED;
			},
			propagate_other_events
		}, e);
		return Button::event_handler(self, data, e);
	}
};

int main() {
	Window window;
	auto &atlas = FontAtlas::instance();

	auto &container = window.host().root();
	auto &layout = container.use_layout<AnchorLayout>();

	auto &scroll = container.add(Component::create<Scroll>());
	layout.set_point(scroll, Anchor::TOP_RIGHT, container, Anchor::TOP_RIGHT, {-10, 10});
	layout.set_point(scroll, Anchor::BOTTOM_LEFT, container, Anchor::TOP_RIGHT, {-110, 310});
	scroll.use_layout<AnchorLayout>();

	auto &striper = container.add(Component::create<Striper>(scroll));
	layout.set_point(striper, Anchor::TOP_LEFT, container, Anchor::TOP_LEFT, {10, 10});
	layout.set_point(striper, Anchor::BOTTOM_RIGHT, container, Anchor::TOP_LEFT, {50, 50});

	const auto &font = *atlas.get("Arial", 16);
	auto &text = striper.add(Component::create<Text>(
		"add",
		font,
		Text::Wrap::WRAP,
		Text::Alignment::CENTER
	));
	striper.use_layout<AnchorLayout>().fill_parent(text, {5, 5});

	window.loop_redraw_when_dirty();

	return EXIT_SUCCESS;
}
