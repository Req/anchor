
#pragma once

#include <variant>

#include <anchor/platform/gl.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/util/enum_operators.hpp>

namespace anchor {
enum class EventStatus : bool {
	CONSUMED = true,
	PROPAGATE = false
};

struct KeyEvent final {
	using Type = gl::KeyType;
	using Modifier = gl::KeyModifier;

	[[maybe_unused]] Type type;
	[[maybe_unused]] Modifier modifiers;
	[[maybe_unused]] int key;

	KeyEvent(Type type, Modifier modifiers, int key) noexcept;
};

struct MouseClickEvent final {
	using Type = gl::ClickType;
	using Modifier = KeyEvent::Modifier;
	using Button = gl::ClickButton;

	[[maybe_unused]] Type type;
	[[maybe_unused]] Button button;
	[[maybe_unused]] Modifier modifiers;
	[[maybe_unused]] Point absolute_location;

	MouseClickEvent(Type type, Button button, Modifier modifiers, Point absolute_location) noexcept;
};

struct MouseMoveEvent final {
	[[maybe_unused]] Point absolute_endpoint;
	[[maybe_unused]] Dimension delta;
	/**
	 * If this is false, then the mouse is currently within the component.
	 * If true, the mouse was previously in the component but has now left.
	 */
	[[maybe_unused]] bool leaving;

	MouseMoveEvent(Point absolute_endpoint, Dimension delta, bool leaving) noexcept;
};

struct MouseDragEvent final {
	using Button = MouseClickEvent::Button;

	[[maybe_unused]] Point absolute_endpoint;
	[[maybe_unused]] Dimension delta;
	[[maybe_unused]] Button button;

	MouseDragEvent(Point absolute_endpoint, Dimension delta, Button button) noexcept;
};

struct MouseWheelEvent final {
	using Modifier = KeyEvent::Modifier;

	[[maybe_unused]] Modifier modifiers;
	[[maybe_unused]] Point absolute_location;
	[[maybe_unused]] BasicDimension<double> delta;

	MouseWheelEvent(Modifier modifiers, Point absolute_location, BasicDimension<double> delta) noexcept;
};

/**
 * Events usually originate on the component under the cursor or the currently-focused component.
 * Components can choose to consume them or propagate them further up the tree. Under normal
 * circumstances, a component shouldn't concern itself with checking if its children care
 * about an event, as it has probably reached that component by being propagated up by its children.
 *
 * Visit this type like so:
 * ```c++
 * anchor::Event e = ...;
 * std::visit(anchor::match_event {
 *     [](const KeyEvent &e) {
 *         // ...
 *     },
 *     anchor::propagate_other_events
 * }, e);
 * ```
 *
 * Additional lambdas can be provided for each type you care to match for.
 */
using Event = std::variant<KeyEvent, MouseClickEvent, MouseMoveEvent, MouseDragEvent, MouseWheelEvent>;

template<typename... Ts>
struct match_event : Ts... {
	using Ts::operator()...;
};

template<typename... Ts>
match_event(Ts...) -> match_event<Ts...>;

constexpr auto propagate_other_events = [](auto) {
	return EventStatus::PROPAGATE;
};
}
