
#pragma once

#include <memory>
#include <optional>
#include <vector>

#include <anchor/brush.hpp>
#include <anchor/component_fwd.hpp>
#include <anchor/event.hpp>
#include <anchor/layout.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
namespace detail {
class ComponentData;
}

/**
 * Have your `ComponentSpecialization::Data` type inherit
 * from this to set the default value of `enabled`.
 */
template<bool Enabled>
struct EnabledByDefault {
	constexpr static bool value = Enabled;
};

/**
 * Have your `ComponentSpecialization::Data` type inherit
 * from this to set the default value of `focusable`.
 */
template<bool Focusable>
struct FocusableByDefault {
	constexpr static bool value = Focusable;
};

struct BaseComponent {
	struct Data {};

	static Data& no_data() noexcept;

	static void repaint_handler(Component &component, Data&, Brush &brush) noexcept;
	static EventStatus event_handler(Component &component, Data&, const Event &event);
};

/**
 * A type-erased component. Specific component types are created via `create()`, but that
 * type is erased. It can be checked via `is_type`, but doing so involves a vtable call.
 *
 * Component references obtained from methods of this class are guaranteed to be valid
 * while the component exists as a child of the component (i.e., until it gets removed).
 */
class Component final {
private:
	ComponentId _id;
	std::unique_ptr<Layout> _layout;
	std::unique_ptr<detail::ComponentData> _data;
	std::vector<std::unique_ptr<Component>> _children;
	Component *_parent = nullptr;
	Rectangle _bounds;

	explicit Component(std::unique_ptr<detail::ComponentData> &&data, bool enabled, bool focusable);

	EventStatus propagate_event(const Event &event);

public:
	using ChildIterator = decltype(_children)::iterator;
	using ConstChildIterator = decltype(_children)::const_iterator;

	bool enabled;
	bool focusable;

	/**
	 * @param args Forwarded to the constructor of `Spec::Data`.
	 */
	template<ComponentSpecialization Spec, typename... Args>
	[[nodiscard]] static Component create(Args&&... args);

	[[nodiscard]] auto parent() noexcept -> Component*;
	[[nodiscard]] auto parent() const noexcept -> const Component*;

	void set_bounds(Rectangle bounds) noexcept;
	void set_location(Point location) noexcept;
	void set_size(Dimension size) noexcept;

	[[nodiscard]] auto get_absolute_location() const noexcept -> Point;
	[[nodiscard]] auto get_absolute_bounds() const noexcept -> Rectangle;
	[[nodiscard]] auto get_bounds() const noexcept -> Rectangle;
	[[nodiscard]] auto get_location() const noexcept -> Point;
	[[nodiscard]] auto get_size() const noexcept -> Dimension;

	/**
	 * Adds the component if this container and returns a reference to the added component.
	 * If the component belongs to another parent component, it will be removed from that parent.
	 */
	Component& add(Component &&component) noexcept;
	/**
	 * Removes and returns the component if it exists as a direct child.
	 */
	std::optional<Component> remove(Component &component) noexcept;

	/**
	 * @param location Coordinates within the size of this component. For example,
	 * if this component is at {1, 2, 3, 4}, then location must be within ([0,3), [0,4)).
	 * @return Null if the point is not part of this component or its children.
	 */
	[[nodiscard]] auto component_at(Point location) noexcept -> Component*;
	[[nodiscard]] auto component_at(Point location) const noexcept -> const Component*;

	[[nodiscard]] auto begin() noexcept -> ChildIterator;
	[[nodiscard]] auto begin() const noexcept -> ConstChildIterator;
	[[nodiscard]] auto end() noexcept -> ChildIterator;
	[[nodiscard]] auto end() const noexcept -> ConstChildIterator;

	[[nodiscard]] ComponentId id() const noexcept;

	template<typename T>
	T& use_layout();

	template<typename T = Layout>
	[[nodiscard]] T* get_layout() noexcept;

	EventStatus fire_event(const Event &event) noexcept;

	void repaint(Brush &brush) noexcept;
	/**
	 * Redoes the layout, if applicable, and
	 * calls `repaint` for all child components.
	 *
	 * This function is *not* called by `repaint`.
	 * It's separate and optional to allow component
	 * specializations flexibility in how they draw their children.
	 */
	void repaint_children(Brush &brush) noexcept;

	/**
	 * Checks if the component is of the given specialization.
	 * This involves a vtable dispatch but is otherwise efficient.
	 */
	template<ComponentSpecialization Spec>
	[[nodiscard]] bool is_type() const noexcept;

	/**
	 * Attempts to get a reference to the type-erased `Spec::Data`.
	 * This is a checked operation in debug builds. Checking involves
	 * a vtable dispatch and is omitted in release builds for performance.
	 */
	template<ComponentSpecialization Spec>
	[[nodiscard]] typename Spec::Data& data() noexcept;
	template<ComponentSpecialization Spec>
	[[nodiscard]] const typename Spec::Data& data() const noexcept;
};

[[nodiscard]] bool operator==(const Component &lhs, const Component &rhs) noexcept;

template<typename T>
constexpr RepaintHandler<T> paint_nothing = [](Component&, T&, Brush&) noexcept {};

template<typename T>
constexpr EventHandler<T> propagate_event = [](Component&, T&, const Event&) noexcept {
	return EventStatus::PROPAGATE;
};
}

#include <anchor/component.inl>
