
#pragma once

#include <anchor/brush.hpp>
#include <anchor/event.hpp>
#include <anchor/component_fwd.hpp>

namespace anchor {
/**
 * Both the base and new data types must be default-constructable.
 * Only a default constructor is created for the combination type.
 */
template<ComponentSpecialization Base, typename T, RepaintHandler<T> Repaint, EventHandler<T> OnEvent>
struct Extend {
	struct Data : public Base::Data, public T {
		explicit Data() : Base::Data{}, T{} {}
	};

	static void repaint_handler(Component &component, Data &data, Brush &brush) noexcept {
		Base::repaint_handler(component, data, brush);
		Repaint(component, data, brush);
	}

	static EventStatus event_handler(Component &component, Data &data, const Event &event) {
		auto status1 = Base::event_handler(component, data, event);
		auto status2 = OnEvent(component, data, event);
		if (status1 == EventStatus::CONSUMED || status2 == EventStatus::CONSUMED) {
			return EventStatus::CONSUMED;
		}
		return EventStatus::PROPAGATE;
	}
};

template<ComponentSpecialization Base, ComponentSpecialization Child>
using Inherit = Extend<Base, typename Child::Data, Child::repaint_handler, Child::event_handler>;

template<ComponentSpecialization Base, typename T, RepaintHandler<T> Repaint>
using ExtendRepaint = Extend<Base, T, Repaint, propagate_event<T>>;

template<ComponentSpecialization Base, typename T, EventHandler<T> OnEvent>
using ExtendEvent = Extend<Base, T, paint_nothing<T>, OnEvent>;
}
