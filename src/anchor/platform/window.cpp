
#include <array>
#include <cassert>
#include <cstdint>
#include <memory>
#include <stdexcept>
#include <vector>

#include <anchor/host.hpp>
#include <anchor/layout.hpp>
#include <anchor/platform/gl.hpp>
#include <anchor/platform/window.hpp>
#include <anchor/util/debug_gl.hpp>

struct GLFWContext { // NOLINT(cppcoreguidelines-special-member-functions)
	explicit GLFWContext() noexcept {
		glfwSetErrorCallback([](int error, const char *what) {
			throw std::runtime_error{std::format("GLFW error: {} ({})", what, error)};
		});
	}

	~GLFWContext() noexcept {
		glfwTerminate();
	}
};

namespace anchor {
Window::Window() {
	if (!glfwInit()) {
		throw std::runtime_error{"glfwInit failed"};
	}
	[[maybe_unused]] const static auto context = std::make_unique<GLFWContext>();

	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	_window = glfwCreateWindow(DEFAULT_BOUNDS.w, DEFAULT_BOUNDS.h, "", nullptr, nullptr);
	glfwSetWindowUserPointer(_window, this);
	set_location(DEFAULT_BOUNDS.location());
	if (_window == nullptr) {
		throw std::runtime_error{"glfwCreateWindowFailed"};
	}
	glfwMakeContextCurrent(_window);

	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		throw std::runtime_error{"glewInit failed"};
	}

	glfwSetKeyCallback(_window, key_callback);
	glfwSetCursorPosCallback(_window, mouse_motion_callback);
	glfwSetMouseButtonCallback(_window, mouse_button_callback);
	glfwSetScrollCallback(_window, mouse_wheel_callback);

	_host = std::make_unique<Host>();
	check_gl_error;
}

Window::~Window() noexcept {
	glfwMakeContextCurrent(_window);
	check_gl_error;
	// needs to be destroyed before the context (the window) is destroyed
	_host.reset();
	glfwDestroyWindow(_window);
}

Host& Window::host() noexcept {
	return *_host;
}

const Host& Window::host() const noexcept {
	return *_host;
}

void Window::set_title(const std::string_view title) noexcept {
	_title = title;
	// can't use `title` in the parameter because string_view isn't null-terminated
	glfwSetWindowTitle(_window, _title.c_str());
}

void Window::set_location(const Point location) noexcept {
	glfwSetWindowPos(_window, location.x, location.y);
}

void Window::set_size(const Dimension size) noexcept {
	glfwSetWindowSize(_window, size.w, size.h);
}

void Window::set_bounds(const Rectangle bounds) noexcept {
	set_location(bounds.location());
	set_size(bounds.size());
}

std::string_view Window::get_title() const noexcept {
	return _title;
}

Point Window::get_location() const noexcept {
	Point location;
	glfwGetWindowPos(_window, &location.x, &location.y);
	return location;
}

Dimension Window::get_size() const noexcept {
	Dimension size;
	glfwGetWindowSize(_window, &size.w, &size.h);
	return size;
}

Rectangle Window::get_bounds() const noexcept {
	return {get_location(), get_size()};
}

static void redraw_impl(const Window &self, GLFWwindow *window, Host &host) noexcept {
	glfwMakeContextCurrent(window); check_gl_error;
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); check_gl_error;
	const BasicColor<float> background{Window::DEFAULT_BACKGROUND_COLOR};
	glClearColor(background.r, background.g, background.b, background.a);
	const auto viewport_size = self.get_size();
	glViewport(0, 0, viewport_size.w, viewport_size.h); check_gl_error;

	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	host.set_size(viewport_size);
	host.redraw();

	glfwSwapBuffers(window); check_gl_error;
}

void Window::redraw() {
	glfwPollEvents(); check_gl_error;
	redraw_impl(*this, _window, *_host);
}

void Window::loop_redraw() {
	while (!glfwWindowShouldClose(_window)) {
		redraw();
	}
	check_gl_error;
}

void Window::loop_redraw_when_dirty() {
	while (!glfwWindowShouldClose(_window)) {
		glfwPollEvents();
		if (_host->is_dirty()) {
			redraw_impl(*this, _window, *_host);
		}
	}
	check_gl_error;
}

std::vector<std::uint8_t> Window::screenshot() noexcept {
	int width;
	int height;
	glfwGetFramebufferSize(_window, &width, &height);
	std::vector<std::uint8_t> buffer(width * height * 4);
	glReadPixels(0, 0, width, height, GL_RGBA, GL_UNSIGNED_BYTE, buffer.data());
	return buffer;
}

[[nodiscard]] static Window &self(GLFWwindow *window) noexcept {
	return *static_cast<Window*>(glfwGetWindowUserPointer(window));
}

void Window::key_callback(GLFWwindow *window, const int key, [[maybe_unused]] const int scancode, const int action, const int mods) noexcept {
	self(window)._host->fire_key_event(
		key,
		static_cast<KeyEvent::Type>(action),
		static_cast<KeyEvent::Modifier>(mods & GLFW_MOD_SHIFT) | static_cast<KeyEvent::Modifier>(mods & GLFW_MOD_CONTROL) | static_cast<KeyEvent::Modifier>(mods & GLFW_MOD_ALT)
	);
}

void Window::mouse_motion_callback(GLFWwindow *window_, const double w, const double h) noexcept {
	auto &window = self(window_);
	const auto endpoint = BasicPoint{w, h}.into<Point::ValueType>();

	bool dragging = false;
	for (const auto button : std::array{GLFW_MOUSE_BUTTON_LEFT, GLFW_MOUSE_BUTTON_RIGHT, GLFW_MOUSE_BUTTON_MIDDLE}) {
		/*
		 * If multiple buttons (e.g., left + right) are down, a separate event is fired for each of them.
		 * Event enum values are generally just assigned the value of GLFW constants. The constants for
		 * left/right/middle don't form a nice bitfield, so it would require extra machinery to allow
		 * for multiple buttons to be pressed in a single event.
		 */
		if (glfwGetMouseButton(window_, button) != GLFW_RELEASE) {
			window._host->fire_mouse_drag_event(endpoint, static_cast<MouseDragEvent::Button>(button));
			dragging = true;
		}
	}
	if (!dragging) {
		window._host->fire_mouse_motion_event(endpoint);
	}
}

void Window::mouse_button_callback(GLFWwindow *window, const int button, const int action, const int mods) noexcept {
	switch (button) {
		case GLFW_MOUSE_BUTTON_LEFT:
		case GLFW_MOUSE_BUTTON_RIGHT:
		case GLFW_MOUSE_BUTTON_MIDDLE:
			break;
		default:
			return;
	}

	double x;
	double y;
	glfwGetCursorPos(window, &x, &y);
	const auto location = BasicPoint{x, y}.into<Point::ValueType>();
	self(window)._host->fire_mouse_click_event(
		static_cast<MouseClickEvent::Button>(button),
		static_cast<MouseClickEvent::Type>(action),
		static_cast<MouseClickEvent::Modifier>(mods & GLFW_MOD_SHIFT) | static_cast<MouseClickEvent::Modifier>(mods & GLFW_MOD_CONTROL) | static_cast<MouseClickEvent::Modifier>(mods & GLFW_MOD_ALT),
		location
	);
}

void Window::mouse_wheel_callback(GLFWwindow *window, const double w, const double h) noexcept {
	double x;
	double y;
	glfwGetCursorPos(window, &x, &y);
	const auto location = BasicPoint{x, y}.into<Point::ValueType>();
	MouseWheelEvent::Modifier modifiers{};
	if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) || glfwGetKey(window, GLFW_KEY_RIGHT_SHIFT)) {
		modifiers |= decltype(modifiers)::SHIFT;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) || glfwGetKey(window, GLFW_KEY_RIGHT_CONTROL)) {
		modifiers |= decltype(modifiers)::CTRL;
	}
	if (glfwGetKey(window, GLFW_KEY_LEFT_ALT) || glfwGetKey(window, GLFW_KEY_RIGHT_ALT)) {
		modifiers |= decltype(modifiers)::ALT;
	}
	self(window)._host->fire_mouse_wheel_event({w, h}, location, modifiers);
}
}
