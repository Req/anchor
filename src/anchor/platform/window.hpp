
#pragma once

#include <cstdint>
#include <memory>
#include <string>
#include <string_view>

#include <anchor/brush.hpp>
#include <anchor/host.hpp>
#include <anchor/platform/gl.hpp>
#include <anchor/unit/color.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
class Window {
private:
	GLFWwindow *_window;
	// allocated to delay construction until gl is initialized
	std::unique_ptr<Host> _host;
	std::string _title;

	static void key_callback(GLFWwindow *window, int key, [[maybe_unused]] int scancode, int action, int mods) noexcept;
	static void mouse_motion_callback(GLFWwindow *window, double w, double h) noexcept;
	static void mouse_button_callback(GLFWwindow *window, int button, int action, int mods) noexcept;
	static void mouse_wheel_callback(GLFWwindow *window, double w, double h) noexcept;

public:
	constexpr static auto DEFAULT_BACKGROUND_COLOR = 0x111111_rgb;
	constexpr static Dimension DEFAULT_SIZE{500, 400};
	constexpr static Rectangle DEFAULT_BOUNDS{{100, 100}, DEFAULT_SIZE};

	explicit Window();
	~Window() noexcept;

	void redraw();
	/**
	 * Repeatedly calls `redraw` until the window is closed.
	 * When this function returns, the window should be discarded.
	 */
	void loop_redraw();
	/**
	 * Similar to `loop_redraw`, but only redraws when the `Host` is marked as dirty.
	 * When this function returns, the window should be discarded.
	 */
	void loop_redraw_when_dirty();
	[[nodiscard]] std::vector<std::uint8_t> screenshot() noexcept;

	[[nodiscard]] auto host() noexcept -> Host&;
	[[nodiscard]] auto host() const noexcept -> const Host&;

	void set_title(std::string_view title) noexcept;
	void set_location(Point location) noexcept;
	/**
	 * Sets the size of the entire window, including borders.
	 */
	void set_size(Dimension size) noexcept;
	void set_bounds(Rectangle bounds) noexcept;

	[[nodiscard]] std::string_view get_title() const noexcept;
	[[nodiscard]] Point get_location() const noexcept;
	/**
	 * Returns the size of the entire window, including borders.
	 */
	[[nodiscard]] Dimension get_size() const noexcept;
	[[nodiscard]] Rectangle get_bounds() const noexcept;
};
}
