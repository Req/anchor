
#pragma once

#include <optional>
#include <string>

#include <anchor/platform/config.hpp>

#include <GL/glew.h>

namespace anchor::gl {
using i32 = GLint;
using u32 = GLuint;
using isize = GLsizei;
using usize = unsigned int;
using value = GLenum;

constexpr inline u32 NONE = GL_NONE;

enum class Error : value {
	NO_ERROR                      [[maybe_unused]] = GL_NO_ERROR,
	INVALID_OPERATION             [[maybe_unused]] = GL_INVALID_OPERATION,
	INVALID_ENUM                  [[maybe_unused]] = GL_INVALID_ENUM,
	INVALID_VALUE                 [[maybe_unused]] = GL_INVALID_VALUE,
	OUT_OF_MEMORY                 [[maybe_unused]] = GL_OUT_OF_MEMORY,
	INVALID_FRAMEBUFFER_OPERATION [[maybe_unused]] = GL_INVALID_FRAMEBUFFER_OPERATION,
	STACK_UNDERFLOW               [[maybe_unused]] = GL_STACK_UNDERFLOW,
	STACK_OVERFLOW                [[maybe_unused]] = GL_STACK_OVERFLOW
};

[[nodiscard]] Error get_error() noexcept;

enum class Severity {
	HIGH = GL_DEBUG_SEVERITY_HIGH,
	MEDIUM = GL_DEBUG_SEVERITY_MEDIUM,
	LOW = GL_DEBUG_SEVERITY_LOW,
	NOTIFICATION = GL_DEBUG_SEVERITY_NOTIFICATION
};

using DebugCallback = void (*)(Severity, const char*) noexcept;
void enable_debug_logging(DebugCallback callback) noexcept;

enum class Feature : value {
	SCISSOR_TEST = GL_SCISSOR_TEST
};

void enable(Feature feature);
void disable(Feature feature);
[[nodiscard]] bool is_enabled(Feature feature);

using Texture = u32;

enum class Type : value {
	UNSIGNED_BYTE = GL_UNSIGNED_BYTE,
	UNSIGNED_SHORT = GL_UNSIGNED_SHORT,
	FLOAT = GL_FLOAT
};

enum class TexParameter : value {
	MIN_FILTER = GL_TEXTURE_MIN_FILTER,
	MAG_FILTER = GL_TEXTURE_MAG_FILTER,
	TEXTURE_WRAP_S = GL_TEXTURE_WRAP_S,
	TEXTURE_WRAP_T = GL_TEXTURE_WRAP_T
};

enum class TexValue : i32 {
	NEAREST = GL_NEAREST,
	LINEAR = GL_LINEAR,
	CLAMP_TO_EDGE = GL_CLAMP_TO_EDGE
};

enum class PixelFormat : i32 {
	RGBA = GL_RGBA
};

enum class TextureUnit : value {
	TEXTURE0 = GL_TEXTURE0
};

[[nodiscard]] Texture gen_texture();
void bind_texture(Texture texture);
void tex_parameter(TexParameter name, TexValue value);
void tex_image(i32 level, PixelFormat internal_format, isize width, isize height, i32 border, PixelFormat format, Type type, const void *data);
void delete_texture(Texture texture);
void active_texture(TextureUnit unit);

enum class ShaderType : value {
	FRAGMENT = GL_FRAGMENT_SHADER,
	VERTEX = GL_VERTEX_SHADER
};

using Shader = u32;
using Program = u32;

[[nodiscard]] Shader create_shader(ShaderType type);
void shader_source(Shader shader, const char *data);
void compile_shader(Shader shader);
void delete_shader(Shader shader);
[[nodiscard]] std::optional<std::string> shader_info_log(Shader shader);
[[nodiscard]] Program create_program();
void attach_shader(Program program, Shader shader);
void link_program(Program program);
void delete_program(Program program);
[[nodiscard]] std::optional<std::string> program_info_log(Program program);
void use_program(Program program);

[[nodiscard]] i32 get_uniform_location(Shader shader, const char *name);
void uniform(i32 location, i32 value);

[[nodiscard]] u32 gen_vertex_array();
void bind_vertex_array(u32 vao);
void delete_vertex_array(u32 vao);

enum class Target : value {
	ARRAY_BUFFER = GL_ARRAY_BUFFER,
	ELEMENT_ARRAY_BUFFER = GL_ELEMENT_ARRAY_BUFFER
};

enum class Usage : value {
	STREAM_DRAW = GL_STREAM_DRAW
};

[[nodiscard]] u32 gen_buffer();
void bind_buffer(Target type, u32 buffer);
void delete_buffer(u32 buffer);
void buffer_data(Target target, std::size_t length, const void *data, Usage usage);

[[nodiscard]] i32 get_attrib_location(Shader shader, const char *name);
void enable_vertex_attrib_array(i32 location);
void vertex_attrib_pointer(i32 location, i32 size, Type type, bool normalized, isize stride, const void *data);

void scissor(i32 x, i32 y, isize w, isize h);

enum class DrawMode {
	TRIANGLES = GL_TRIANGLES
};

void draw_elements(DrawMode mode, isize count, Type type, std::size_t index_offset);
}

#ifdef ANCHOR_PLATFORM_NATIVE
#include <anchor/platform/detail/glfw.inl>
#elif defined(ANCHOR_PLATFORM_WASM)
#include <anchor/platform/detail/wasm.inl>
#endif
