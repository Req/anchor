
#include <cstdint>
#include <optional>
#include <string>
#include <type_traits>

#include <anchor/platform/config.hpp>
#include <anchor/platform/detail/gl_native.hpp>
#include <anchor/util/debug_gl.hpp>

template<typename T>
static std::underlying_type_t<T> cast(const T value) noexcept {
	return static_cast<std::underlying_type_t<T>>(value);
}

namespace anchor::gl {
Error get_error() noexcept {
	return static_cast<Error>(glGetError());
}

// the gl callback can't be a capturing lambda. this lifts it to avoid needing to capture
thread_local DebugCallback debug_callback = nullptr; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)
void enable_debug_logging(DebugCallback callback) noexcept {
	glEnable(GL_DEBUG_OUTPUT);
	debug_callback = callback;
	glDebugMessageCallback([](auto, auto, auto, auto severity, auto, auto message, auto) noexcept {
		if (debug_callback != nullptr) {
			debug_callback(static_cast<Severity>(severity), message);
		}
	}, nullptr);
}

Texture gen_texture() {
	Texture texture;
	glGenTextures(1, &texture);
	check_gl_error;
	return texture;
}

void bind_texture(Texture texture) {
	glBindTexture(GL_TEXTURE_2D, texture);
	check_gl_error;
}

void tex_parameter(TexParameter name, TexValue value) {
	glTexParameteri(GL_TEXTURE_2D, cast(name), cast(value));
	check_gl_error;
}

void tex_image(i32 level, PixelFormat internal_format, isize width, isize height, i32 border, PixelFormat format, Type type, const void *data) {
	glTexImage2D(GL_TEXTURE_2D, level, cast(internal_format), width, height, border, cast(format), cast(type), data);
	check_gl_error;
}

Shader create_shader(ShaderType type) {
	const auto shader = glCreateShader(cast(type));
	check_gl_error;
	return shader;
}

void shader_source(Shader shader, const char *data) {
	glShaderSource(shader, 1, &data, nullptr);
	check_gl_error;
}

void compile_shader(Shader shader) {
	glCompileShader(shader);
	check_gl_error;
}

void delete_shader(Shader shader) {
	glDeleteShader(shader);
	check_gl_error;
}

std::optional<std::string> shader_info_log(Shader shader) {
	GLint length;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
	if (length <= 0) {
		return std::nullopt;
	}
	std::string error(length, '\0');
	glGetShaderInfoLog(shader, length, nullptr, error.data());
	glDeleteShader(shader);
	check_gl_error;
	return {error};
}

Program create_program() {
	const auto program = glCreateProgram();
	check_gl_error;
	return program;
}

void attach_shader(Program program, Shader shader) {
	glAttachShader(program, shader);
	check_gl_error;
}

void link_program(Program program) {
	glLinkProgram(program);
	check_gl_error;
}

std::optional<std::string> program_info_log(Program program) {
	GLint length = 0;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
	if (length <= 0) {
		return std::nullopt;
	}
	std::string error(length, '\0');
	glGetProgramInfoLog(program, length, nullptr, error.data());
	glDeleteProgram(program);
	check_gl_error;
	return {error};
}

i32 get_uniform_location(Shader shader, const char *name) {
	const auto location = glGetUniformLocation(shader, name);
	check_gl_error;
	return location;
}

u32 gen_vertex_array() {
	u32 vao;
	glGenVertexArrays(1, &vao);
	check_gl_error;
	return vao;
}

void bind_vertex_array(u32 vao) {
	glBindVertexArray(vao);
	check_gl_error;
}

u32 gen_buffer() {
	u32 buffer;
	glGenBuffers(1, &buffer);
	check_gl_error;
	return buffer;
}

void bind_buffer(Target type, u32 buffer) {
	glBindBuffer(cast(type), buffer);
	check_gl_error;
}

i32 get_attrib_location(Shader shader, const char *name) {
	const auto location = glGetAttribLocation(shader, name);
	check_gl_error;
	return location;
}

void enable_vertex_attrib_array(i32 location) {
	glEnableVertexAttribArray(location);
	check_gl_error;
}

void vertex_attrib_pointer(i32 location, i32 size, Type type, bool normalized, isize stride, const void *data) {
	glVertexAttribPointer(location, size, cast(type), normalized, stride, data);
	check_gl_error;
}

void delete_buffer(u32 buffer) {
	glDeleteBuffers(1, &buffer);
	check_gl_error;
}

void delete_texture(Texture texture) {
	glDeleteTextures(1, &texture);
	check_gl_error;
}

void delete_vertex_array(u32 vao) {
	glDeleteVertexArrays(1, &vao);
	check_gl_error;
}

void delete_program(Program program) {
	glDeleteProgram(program);
	check_gl_error;
}

void enable(Feature feature) {
	glEnable(cast(feature));
	check_gl_error;
}

void disable(Feature feature) {
	glDisable(cast(feature));
	check_gl_error;
}

bool is_enabled(Feature feature) {
	const auto enabled = glIsEnabled(cast(feature));
	check_gl_error;
	return enabled;
}

void active_texture(TextureUnit unit) {
	glActiveTexture(cast(unit));
	check_gl_error;
}

void use_program(u32 program) {
	glUseProgram(program);
	check_gl_error;
}

void uniform(i32 location, i32 value) {
	glUniform1i(location, value);
	check_gl_error;
}

void buffer_data(Target target, std::size_t length, const void *data, Usage usage) {
	glBufferData(cast(target), static_cast<GLsizeiptr>(length), data, cast(usage));
	check_gl_error;
}

void scissor(i32 x, i32 y, isize w, isize h) {
	glScissor(x, y, w, h);
	check_gl_error;
}

void draw_elements(DrawMode mode, isize count, Type type, std::size_t index_offset) {
	glDrawElements(cast(mode), count, cast(type), reinterpret_cast<void*>(static_cast<std::uintptr_t>(index_offset))); // NOLINT(performance-no-int-to-ptr)
	check_gl_error;
}
}
