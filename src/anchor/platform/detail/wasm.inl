
#include <anchor/util/enum_operators.hpp>

/*
namespace anchor::gl {
enum class KeyType {
	PRESS   [[maybe_unused]],
	HOLD    [[maybe_unused]],
	RELEASE [[maybe_unused]]
};

enum class KeyModifier {
	NONE  [[maybe_unused]] = 0,
	SHIFT [[maybe_unused]],
	CTRL  [[maybe_unused]],
	ALT   [[maybe_unused]],

	METADATA_ENABLE_OPERATOR_OR  [[maybe_unused]],
	METADATA_ENABLE_OPERATOR_AND [[maybe_unused]],
	METADATA_ENABLE_OPERATOR_XOR [[maybe_unused]]
};

enum class ClickType {
	PRESS   [[maybe_unused]],
	RELEASE [[maybe_unused]]
};

enum class ClickButton {
	LEFT   [[maybe_unused]],
	RIGHT  [[maybe_unused]],
	MIDDLE [[maybe_unused]]
};
}
*/

#include <anchor/platform/detail/glfw.inl>
