
#pragma once

#include <anchor/util/enum_operators.hpp>

#include <GLFW/glfw3.h>

namespace anchor::gl {
enum class KeyType {
	PRESS   [[maybe_unused]] = GLFW_PRESS,
	HOLD    [[maybe_unused]] = GLFW_REPEAT,
	RELEASE [[maybe_unused]] = GLFW_RELEASE
};

enum class KeyModifier {
	NONE  [[maybe_unused]] = 0,
	SHIFT [[maybe_unused]] = GLFW_MOD_SHIFT,
	CTRL  [[maybe_unused]] = GLFW_MOD_CONTROL,
	ALT   [[maybe_unused]] = GLFW_MOD_ALT,

	METADATA_ENABLE_OPERATOR_OR  [[maybe_unused]],
	METADATA_ENABLE_OPERATOR_AND [[maybe_unused]],
	METADATA_ENABLE_OPERATOR_XOR [[maybe_unused]]
};

enum class ClickType {
	PRESS   [[maybe_unused]] = GLFW_PRESS,
	RELEASE [[maybe_unused]] = GLFW_RELEASE
};

enum class ClickButton {
	LEFT   [[maybe_unused]] = GLFW_MOUSE_BUTTON_LEFT,
	RIGHT  [[maybe_unused]] = GLFW_MOUSE_BUTTON_RIGHT,
	MIDDLE [[maybe_unused]] = GLFW_MOUSE_BUTTON_MIDDLE
};
}
