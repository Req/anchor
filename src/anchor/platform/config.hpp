
#pragma once

#if defined(__EMSCRIPTEN__) || defined(BUILD_WASM)
#define ANCHOR_PLATFORM_WASM

#ifdef ANCHOR_PLATFORM_WASM
#include <emscripten/emscripten.h>
#define GLFW_INCLUDE_ES3
#endif
#else
#define ANCHOR_PLATFORM_NATIVE
#endif
