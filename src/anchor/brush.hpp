
#pragma once

#include <cstdint>
#include <optional>
#include <span>
#include <string_view>
#include <utility>
#include <vector>

#include <anchor/platform/gl.hpp>
#include <anchor/unit/color.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>
#include <anchor/util/tags.hpp>

namespace anchor {
namespace detail {
/**
 * Default constructor leaves values intentionally uninitialized.
 * This allows cleaner code because DrawInfo constructor calls do not need to be a massive initializer_list as a result.
 * Instead, the DrawInfo vectors are resized via constructor, then individual vertices can be assigned later.
 */
struct Vertex {
	[[maybe_unused]] BasicPoint<float> position{UninitializedTag{}};
	[[maybe_unused]] BasicPoint<float> uv{UninitializedTag{}};
	[[maybe_unused]] BasicColor<std::uint8_t> color{UninitializedTag{}};
};

struct DrawInfo {
	gl::Texture texture;
	Rectangle clip;
	std::uint16_t vertex_count;

	DrawInfo(gl::Texture texture, Rectangle clip, std::uint16_t vertex_count) noexcept;
};

/**
 * OpenGL expects coordinates in the cartesian plane.
 * Window coordinates have (0,0) at the top left.
 * The conversion should (not mandatory) be done by
 * constructing a WindowPosition and letting it
 * implicitly convert to a Vertex in cartesian coordinates.
 */
struct WindowPosition {
	decltype(Vertex::position)::ValueType x;
	decltype(Vertex::position)::ValueType y;

	[[nodiscard]] decltype(Vertex::position) as_cartesian() const noexcept;
};

using ClipLockId = std::size_t;

struct ClipLockState { // NOLINT(cppcoreguidelines-pro-type-member-init)
	Rectangle clip;
	ClipLockId id;
};
}

class Brush;

class ClipLock final {
private:
	Brush &_brush;
	std::optional<detail::ClipLockId> _id;

	explicit ClipLock(Brush &brush, detail::ClipLockId id) noexcept;

public:
	~ClipLock() noexcept;

	/**
	 * Releasing the lock does not modify the current clip value,
	 * nor will it relax constraints on locks placed after the
	 * released lock.
	 */
	void release() noexcept;

	friend class Brush;
};

/**
 * Converts primitive drawing operations into internal vertex buffers.
 */
class Brush : private DisableCopy, private DisableMove {
protected:
	Rectangle _bounds;
	Rectangle _clip;
	Color _color;

	struct
	{
		gl::Program shader;
		gl::i32 sampler;
		gl::u32 vao;
		gl::u32 vertex_buffer;
		gl::u32 index_buffer;
		gl::u32 white_pixel;
	} _state;

	std::vector<detail::DrawInfo> _draw_list;
	std::vector<detail::Vertex> _vertices;
	std::vector<std::uint16_t> _indices;
	std::vector<detail::ClipLockState> _clip_locks;

	constexpr static BasicPoint<float> WHITE_PIXEL_UV{0.f, 0.f};
	constexpr static BasicRectangle<float> WHITE_PIXEL_UV_RECT{0.f, 0.f, 0.f, 0.f};

	/**
	 * Call to allocate space for primitives.
	 * Vertices must be assigned after this call.
	 * Indices must be __added___ after this call.
	 * Example (for a rectangle):
	 *     auto [vertices, indices] = new_draw_command(white_pixel, clip, 4, 6);
	 *     vertices[0] = <something resulting in a Vertex>;
	 *     ...
	 *     vertices[3] = <...>;
	 *     indices[0] += 0;
	 *     indices[1] += 1;
	 *     indices[2] += 2; // this completes a triangle using vertices (0, 1, 2)
	 *     indices[3] += 0;
	 *     indices[4] += 1;
	 *     indices[5] += 3; // vertices (0, 1, 3)
	 *
	 * @param texture Use _state.white_pixel to represent no texture.
	 */
	[[nodiscard]] auto new_draw_command(gl::Texture texture, Rectangle clip, std::uint16_t vertex_count, std::uint16_t index_count) noexcept
		-> std::pair<std::span<detail::Vertex>, std::span<std::uint16_t>>;

	void primitive_draw_rect(Rectangle area, Color color, gl::Texture texture, BasicRectangle<float> uv) noexcept;

	void release_clip_lock(detail::ClipLockId id) noexcept;

public:
	explicit Brush();
	virtual ~Brush();

	void draw() noexcept;

	/**
	 * Sets the relative drawing space of the brush. For example, drawing
	 * at (1,1) when bounds are set to (20,20) will actually draw at (21,21).
	 */
	void set_bounds(Rectangle bounds) noexcept;
	void set_color(Color color) noexcept;
	/**
	 * Sets the clipping boundary of all drawing operations. The result of
	 * a drawing operation will not exceed the current clipping boundary.
	 */
	void set_clip(Rectangle clip) noexcept;
	/**
	 * Similar to `set_clip`, but locks the upper bounds of the clip.
	 * Until this lock is released, any calls to `set_clip` or `lock_clip`
	 * with a clip that exceeds the locked bounds will be reduced to fit.
	 */
	[[nodiscard]] ClipLock lock_clip(Rectangle clip) noexcept;

	[[nodiscard]] Rectangle get_bounds() const noexcept;
	[[nodiscard]] Rectangle get_clip() const noexcept;
	[[nodiscard]] Color get_color() const noexcept;

	/**
	 * @param thickness If 1, the result will be slightly larger to ensure
	 * the rendering implementation actually draws the thin line.
	 */
	void draw_line(Point a, Point b, std::uint32_t thickness) noexcept;
	/**
	 * Similar to multiple calls to `draw_line`.
	 */
	void draw_border(Rectangle bounds, std::uint32_t thickness) noexcept;
	void fill_rectangle(Rectangle area) noexcept;

	/**
	 * @param uv Must be in UV space coordinates ((0,0) bottom left; u,v in [0,1]).
	 */
	void draw_image(gl::Texture texture, Rectangle where, Color color = 0xffffffff_rgba, BasicRectangle<float> uv = {0, 0, 1, 1}) noexcept;

	friend class ClipLock;
};
}
