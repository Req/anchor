
#pragma once

#include <algorithm>
#include <cstdint>
#include <limits>
#include <type_traits>

#include <anchor/util/tags.hpp>

namespace anchor {
namespace detail {
template<typename To, typename From>
constexpr To convert_color_unit(const From from) noexcept {
	if constexpr (std::is_same_v<To, From>) {
		return from;
	}

	const auto from_percent = (static_cast<double>(from) - ColorUnit<From>::min) / (ColorUnit<From>::max - ColorUnit<From>::min);
	return std::clamp(
		static_cast<To>(ColorUnit<To>::min + from_percent * (ColorUnit<To>::max - ColorUnit<To>::min)),
		ColorUnit<To>::min,
		ColorUnit<To>::max
	);
}

template<typename T>
[[nodiscard]]
constexpr static T scale_color_unit(const T unit, const double weight) noexcept {
	return static_cast<T>(std::clamp(
		unit * weight,
		static_cast<double>(ColorUnit<T>::min),
		static_cast<double>(ColorUnit<T>::max)
	));
}
}

template<typename T>
template<typename From>
constexpr BasicColor<T>::BasicColor(const BasicColor<From> from) noexcept :
	r{detail::convert_color_unit<T>(from.r)},
	g{detail::convert_color_unit<T>(from.g)},
	b{detail::convert_color_unit<T>(from.b)},
	a{detail::convert_color_unit<T>(from.a)} {}

template<typename T>
BasicColor<T>::BasicColor(UninitializedTag) noexcept {}

template<typename T>
constexpr BasicColor<T>::BasicColor() noexcept : BasicColor{Traits::min, Traits::min, Traits::min, Traits::max} {}

template<typename T>
constexpr BasicColor<T>::BasicColor(const T e) noexcept : BasicColor{e, e, e} {}

template<typename T>
constexpr BasicColor<T>::BasicColor(const T r, const T g, const T b) noexcept : BasicColor{r, g, b, Traits::max} {}

template<typename T>
constexpr BasicColor<T>::BasicColor(const T r, const T g, const T b, const T a) noexcept : r{r}, g{g}, b{b}, a{a} {}

template<typename T>
constexpr bool BasicColor<T>::is_transparent() const noexcept {
	return a == 0;
}

template<typename T>
constexpr BasicColor<T> BasicColor<T>::as_greyscale(const float r_weight, const float g_weight, const float b_weight) const noexcept {
	const auto value = static_cast<T>(r * r_weight + g * g_weight + b * b_weight);
	return {value, value, value, a};
}

template<typename T>
constexpr BasicColor<T> BasicColor<T>::scale(const double weight) const noexcept {
	return {
		detail::scale_color_unit<T>(r, weight),
		detail::scale_color_unit<T>(g, weight),
		detail::scale_color_unit<T>(b, weight),
		a
	};
}

template<typename T>
constexpr bool operator==(const BasicColor<T> &lhs, const BasicColor<T> &rhs) noexcept {
	return
		lhs.r == rhs.r &&
		lhs.g == rhs.g &&
		lhs.b == rhs.b &&
		lhs.a == rhs.a;
}

template<typename T>
constexpr bool operator!=(const BasicColor<T> &lhs, const BasicColor<T> &rhs) noexcept {
	return !(lhs == rhs);
}

constexpr Color operator"" _rgba(const unsigned long long int hex) noexcept {
	const auto hex32 = static_cast<std::uint32_t>(hex);
	return {
		static_cast<std::uint8_t>((hex32 & 0xff000000) >> 24),
		static_cast<std::uint8_t>((hex32 & 0x00ff0000) >> 16),
		static_cast<std::uint8_t>((hex32 & 0x0000ff00) >> 8),
		static_cast<std::uint8_t>((hex32 & 0x000000ff) >> 0),
	};
}

constexpr Color operator"" _rgb(const unsigned long long int hex) noexcept {
	return operator""_rgba((hex << 8) | 0xff);
}
}

namespace std { // NOLINT(cert-dcl58-cpp)
template<typename T>
auto formatter<anchor::BasicColor<T>>::format(const anchor::BasicColor<T> color, format_context &ctx) const {
	return std::format_to(ctx.out(), "Color{{{}, {}, {}, {}}}", color.r, color.g, color.b, color.a);
}
}
