
#pragma once

#include <format>
#include <string>
#include <type_traits>

#include <anchor/util/tags.hpp>

namespace anchor {
template<typename T>
BasicDimension<T>::BasicDimension(UninitializedTag) noexcept {}

template<typename T>
constexpr BasicDimension<T>::BasicDimension() noexcept : BasicDimension<T>{0, 0} {}

template<typename T>
constexpr BasicDimension<T>::BasicDimension(const T w, const T h) noexcept : w{w}, h{h} {}

template<typename From>
template<typename To>
constexpr BasicDimension<To> BasicDimension<From>::into() const noexcept {
	return {static_cast<To>(w), static_cast<To>(h)};
}

template<typename T>
constexpr bool operator==(const BasicDimension<T> lhs, const BasicDimension<T> rhs) noexcept {
	return lhs.w == rhs.w && lhs.h == rhs.h;
}

template<typename T>
constexpr bool operator!=(const BasicDimension<T> lhs, const BasicDimension<T> rhs) noexcept {
	return !(lhs == rhs);
}

template<typename T>
constexpr BasicDimension<T> operator+(const BasicDimension<T> lhs, const BasicDimension<T> rhs) noexcept {
	return {lhs.w + rhs.w, lhs.h + rhs.h};
}

template<typename T>
constexpr BasicDimension<T> operator-(const BasicDimension<T> lhs, const BasicDimension<T> rhs) noexcept {
	return {lhs.w - rhs.w, lhs.h - rhs.h};
}

template<typename T, typename I>
constexpr BasicDimension<T> operator*(const BasicDimension<T> lhs, const I rhs) noexcept {
	static_assert(std::is_arithmetic_v<I>);
	return {static_cast<T>(lhs.w * rhs), static_cast<T>(lhs.h * rhs)};
}

template<typename T, typename I>
constexpr BasicDimension<T> operator/(const BasicDimension<T> lhs, const I rhs) noexcept {
	static_assert(std::is_arithmetic_v<I>);
	return {static_cast<T>(lhs.w / rhs), static_cast<T>(lhs.h / rhs)};
}

template<typename T>
constexpr BasicDimension<T>& operator+=(const BasicDimension<T> &lhs, const BasicDimension<T> rhs) noexcept {
	lhs = lhs + rhs;
	return lhs;
}

template<typename T>
constexpr BasicDimension<T>& operator-=(const BasicDimension<T> &lhs, const BasicDimension<T> rhs) noexcept {
	lhs = lhs - rhs;
	return lhs;
}

template<typename T, typename I>
constexpr BasicDimension<T>& operator*=(BasicDimension<T> &lhs, const I rhs) noexcept {
	lhs = lhs * rhs;
	return lhs;
}

template<typename T, typename I>
constexpr BasicDimension<T>& operator/=(BasicDimension<T> &lhs, const I rhs) noexcept {
	lhs = lhs / rhs;
	return lhs;
}
}

namespace std { // NOLINT(cert-dcl58-cpp)
template<typename T>
auto formatter<anchor::BasicDimension<T>>::format(const anchor::BasicDimension<T> dimension, format_context &ctx) const {
	return std::format_to(ctx.out(), "Dimension{{{}, {}}}", dimension.w, dimension.h);
}
}
