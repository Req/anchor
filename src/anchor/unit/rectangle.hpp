
#pragma once

#include <cstdint>
#include <format>
#include <string>
#include <type_traits>

#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/util/tags.hpp>

namespace anchor {
template<typename T>
struct alignas(sizeof(T) * 4) BasicRectangle final {
	static_assert(std::is_arithmetic_v<T>);
	using ValueType = T;

	T x;
	T y;
	T w;
	T h;

	explicit BasicRectangle(UninitializedTag) noexcept;
	constexpr BasicRectangle() noexcept;
	constexpr BasicRectangle(T x, T y, T w, T h) noexcept;
	constexpr BasicRectangle(BasicPoint<T> point, BasicDimension<T> dimension) noexcept;
	constexpr BasicRectangle(BasicPoint<T> a, BasicPoint<T> b) noexcept;

	constexpr BasicRectangle<T>& operator=(BasicPoint<T> point) noexcept;
	constexpr BasicRectangle<T>& operator=(BasicDimension<T> dimension) noexcept;

	template<typename To>
	constexpr BasicRectangle<To> into() const noexcept;

	/**
	 * Expands this rectangle to include all of the space occupied by `bounds`.
	 */
	constexpr void include(BasicRectangle<T> bounds) noexcept;
	/**
	 * Shrinks this rectangle to not exceed `bounds`.
	 */
	constexpr void restrict_by(BasicRectangle<T> bounds) noexcept;

	/**
	 * Inclusive on the lower bound and exclusive on the upper bound.
	 * e.g., (0, 0, 10, 10) contains (0, 0) but does not contain (10, 10).
	 */
	[[nodiscard]] constexpr bool contains(BasicPoint<T> point) const noexcept;
	/**
	 * Inclusive on both the lower and upper bounds.
	 * e.g., (0, 0, 10, 10) contains all of:
	 * - (0, 0, 10, 10)
	 * - (0, 0, 0, 0)
	 * - (10, 10, 0, 0)
	 */
	[[nodiscard]] constexpr bool contains(BasicRectangle<T> rectangle) const noexcept;

	[[nodiscard]] constexpr Point location() const noexcept;
	[[nodiscard]] constexpr Point far_location() const noexcept;
	[[nodiscard]] constexpr Dimension size() const noexcept;
};

template<typename T>
[[nodiscard]] constexpr bool operator==(BasicRectangle<T> lhs, BasicRectangle<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr bool operator!=(BasicRectangle<T> lhs, BasicRectangle<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicRectangle<T> operator+(BasicRectangle<T> lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicRectangle<T> operator-(BasicRectangle<T> lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicRectangle<T> operator+(BasicRectangle<T> lhs, BasicDimension<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicRectangle<T> operator-(BasicRectangle<T> lhs, BasicDimension<T> rhs) noexcept;

template<typename T>
constexpr BasicRectangle<T>& operator+=(BasicRectangle<T> &lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
constexpr BasicRectangle<T>& operator-=(BasicRectangle<T> &lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
constexpr BasicRectangle<T>& operator+=(BasicRectangle<T> &lhs, BasicDimension<T> rhs) noexcept;

template<typename T>
constexpr BasicRectangle<T>& operator-=(BasicRectangle<T> &lhs, BasicDimension<T> rhs) noexcept;

using Rectangle = BasicRectangle<std::int32_t>;
}

namespace std {
template<typename T>
struct formatter<anchor::BasicRectangle<T>> {
	auto format(anchor::BasicRectangle<T> rectangle, format_context &ctx) const;

	constexpr auto parse(format_parse_context &ctx) {
		return ctx.begin();
	}
};
}

#include <anchor/unit/rectangle.inl>
