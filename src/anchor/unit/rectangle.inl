
#pragma once

#include <cassert>
#include <cstdint>
#include <format>
#include <functional>
#include <string>

#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/util/tags.hpp>

namespace anchor {
template<typename T>
BasicRectangle<T>::BasicRectangle(UninitializedTag) noexcept {}

template<typename T>
constexpr BasicRectangle<T>::BasicRectangle() noexcept : BasicRectangle<T>{0, 0, 0, 0} {}

template<typename T>
constexpr BasicRectangle<T>::BasicRectangle(const T x, const T y, const T w, const T h) noexcept : x{x}, y{y}, w{w}, h{h} {}

template<typename T>
constexpr BasicRectangle<T>::BasicRectangle(const BasicPoint<T> point, const BasicDimension<T> dimension) noexcept : BasicRectangle<T>{point.x, point.y, dimension.w, dimension.h} {}

template<typename T>
constexpr BasicRectangle<T>::BasicRectangle(const BasicPoint<T> a, const BasicPoint<T> b) noexcept : BasicRectangle<T>{a.x, a.y, b.x - a.x, b.y - a.y} {}

template<typename T>
constexpr BasicRectangle<T>& BasicRectangle<T>::operator=(const BasicPoint<T> point) noexcept {
	x = point.x;
	y = point.y;
	return *this;
}

template<typename T>
constexpr BasicRectangle<T>& BasicRectangle<T>::operator=(const BasicDimension<T> dimension) noexcept {
	w = dimension.w;
	h = dimension.h;
	return *this;
}

template<typename T>
constexpr BasicRectangle<T> operator+(const BasicRectangle<T> lhs, const BasicPoint<T> rhs) noexcept {
	return {lhs.location() + rhs, lhs.size()};
}

template<typename T>
constexpr BasicRectangle<T> operator-(const BasicRectangle<T> lhs, const BasicPoint<T> rhs) noexcept {
	return {lhs.location() - rhs, lhs.size()};
}

template<typename T>
constexpr BasicRectangle<T> operator+(const BasicRectangle<T> lhs, const BasicDimension<T> rhs) noexcept {
	return {lhs.location(), lhs.size() + rhs};
}

template<typename T>
constexpr BasicRectangle<T> operator-(const BasicRectangle<T> lhs, const BasicDimension<T> rhs) noexcept {
	return {lhs.location(), lhs.size() - rhs};
}

template<typename T>
constexpr BasicRectangle<T>& operator+=(BasicRectangle<T> &lhs, const BasicPoint<T> rhs) noexcept {
	lhs = lhs + rhs;
	return lhs;
}

template<typename T>
constexpr BasicRectangle<T>& operator-=(BasicRectangle<T> &lhs, const BasicPoint<T> rhs) noexcept {
	lhs = lhs - rhs;
	return lhs;
}

template<typename T>
constexpr BasicRectangle<T>& operator+=(BasicRectangle<T> &lhs, const BasicDimension<T> rhs) noexcept {
	lhs = lhs + rhs;
	return lhs;
}

template<typename T>
constexpr BasicRectangle<T>& operator-=(BasicRectangle<T> &lhs, const BasicDimension<T> rhs) noexcept {
	lhs = lhs - rhs;
	return lhs;
}

template<typename From>
template<typename To>
constexpr BasicRectangle<To> BasicRectangle<From>::into() const noexcept {
	return {static_cast<To>(x), static_cast<To>(y), static_cast<To>(w), static_cast<To>(h)};
}

template<typename T>
constexpr void BasicRectangle<T>::include(const BasicRectangle<T> bounds) noexcept {
	if (bounds.x < x) {
		x = bounds.x;
	}
	if (bounds.y < y) {
		y = bounds.y;
	}
	const auto far = bounds.far_location();
	if (x + w < far.x) {
		w = far.x - x;
	}
	if (y + h < far.y) {
		h = far.y - y;
	}
	assert(contains(bounds));
}

namespace detail {
template<typename T, T BasicRectangle<T>::*Start, T BasicRectangle<T>::*Span>
constexpr void restrict_dimension(BasicRectangle<T> &target, const BasicRectangle<T> bounds) noexcept {
	const auto far = bounds.*Start + bounds.*Span;
	if (target.*Start < bounds.*Start) {
		// target begins before bounds and has to be shrunk into it
		if (target.*Start + target.*Span < bounds.*Start) {
			// target exists entirely before bounds and does not intersect it
			target.*Span = 0;
		} else {
			// target partially intersects bounds
			target.*Span -= bounds.*Start - target.*Start;
		}
		target.*Start = bounds.*Start;
	} else if (target.*Start > far) {
		// target begins after bounds (and cannot intersect it)
		target.*Start = far;
		target.*Span = 0;
	}
	const auto extent = target.*Start + target.*Span;
	if (extent > far) {
		// target begins within bounds but its extent exceeds bounds
		target.*Span -= extent - far;
	}
}

template<typename T, typename UpperComparator>
constexpr bool within(const BasicRectangle<T> lhs, const BasicPoint<T> rhs) noexcept {
	const auto far = lhs.far_location();
	return
		lhs.x <= rhs.x &&
		UpperComparator{}(rhs.x, far.x) &&
		lhs.y <= rhs.y &&
		UpperComparator{}(rhs.y, far.y);
}
}

template<typename T>
constexpr void BasicRectangle<T>::restrict_by(const BasicRectangle<T> bounds) noexcept {
	detail::restrict_dimension<T, &BasicRectangle<T>::x, &BasicRectangle<T>::w>(*this, bounds);
	detail::restrict_dimension<T, &BasicRectangle<T>::y, &BasicRectangle<T>::h>(*this, bounds);
	assert(bounds.contains(*this));
}

template<typename T>
constexpr bool BasicRectangle<T>::contains(const BasicPoint<T> point) const noexcept {
	return detail::within<T, std::less<T>>(*this, point);
}

template<typename T>
constexpr bool BasicRectangle<T>::contains(const BasicRectangle<T> rectangle) const noexcept {
	return
		detail::within<T, std::less_equal<T>>(*this, rectangle.location()) &&
		detail::within<T, std::less_equal<T>>(*this, rectangle.far_location());
}

template<typename T>
constexpr Point BasicRectangle<T>::location() const noexcept {
	return {x, y};
}

template<typename T>
constexpr Point BasicRectangle<T>::far_location() const noexcept {
	return {x + w, y + h};
}

template<typename T>
constexpr Dimension BasicRectangle<T>::size() const noexcept {
	return {w, h};
}

template<typename T>
constexpr bool operator==(const BasicRectangle<T> lhs, const BasicRectangle<T> rhs) noexcept {
	return
		lhs.x == rhs.x &&
		lhs.y == rhs.y &&
		lhs.w == rhs.w &&
		lhs.h == rhs.h;
}

template<typename T>
constexpr bool operator!=(const BasicRectangle<T> lhs, const BasicRectangle<T> rhs) noexcept {
	return !(lhs == rhs);
}
}

namespace std { // NOLINT(cert-dcl58-cpp)
template<typename T>
auto formatter<anchor::BasicRectangle<T>>::format(const anchor::BasicRectangle<T> rectangle, format_context &ctx) const {
	return std::format_to(ctx.out(), "Rectangle{{{}, {}}}", rectangle.location(), rectangle.size());
}
}
