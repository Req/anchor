
#pragma once

#include <cstdint>
#include <format>
#include <string>
#include <type_traits>

#include <anchor/unit/dimension.hpp>
#include <anchor/util/tags.hpp>

namespace anchor {
template<typename T>
struct alignas(sizeof(T) * 2) BasicPoint final {
	static_assert(std::is_arithmetic_v<T>);
	using ValueType = T;

	T x;
	T y;

	explicit BasicPoint(UninitializedTag) noexcept;
	constexpr BasicPoint() noexcept;
	constexpr BasicPoint(T x, T y) noexcept;

	template<typename To>
	constexpr BasicPoint<To> into() const noexcept;

	[[nodiscard]] constexpr BasicDimension<T> distance_to(BasicPoint<T> other) const noexcept;
};

template<typename T>
[[nodiscard]] constexpr bool operator==(BasicPoint<T> lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr bool operator!=(BasicPoint<T> lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicPoint<T> operator+(BasicPoint<T> point, BasicDimension<T> dimension) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicPoint<T> operator-(BasicPoint<T> point, BasicDimension<T> dimension) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicPoint<T> operator+(BasicPoint<T> lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicPoint<T> operator-(BasicPoint<T> lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
constexpr BasicPoint<T>& operator+=(BasicPoint<T> &point, BasicDimension<T> dimension) noexcept;

template<typename T>
constexpr BasicPoint<T>& operator-=(BasicPoint<T> &point, BasicDimension<T> dimension) noexcept;

template<typename T>
constexpr BasicPoint<T>& operator+=(BasicPoint<T> &lhs, BasicPoint<T> rhs) noexcept;

template<typename T>
constexpr BasicPoint<T>& operator-=(BasicPoint<T> &lhs, BasicPoint<T> rhs) noexcept;

using Point = BasicPoint<std::int32_t>;
}

namespace std {
template<typename T>
struct formatter<anchor::BasicPoint<T>> {
	auto format(anchor::BasicPoint<T> point, format_context &ctx) const;

	constexpr auto parse(format_parse_context &ctx) {
		return ctx.begin();
	}
};
}

#include <anchor/unit/point.inl>
