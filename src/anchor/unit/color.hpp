
#pragma once

#include <cstdint>
#include <format>

#include <anchor/util/tags.hpp>

namespace anchor {
/**
 * Specializations should define min and max values.
 */
template<typename T>
struct ColorUnit {};

template<>
struct ColorUnit<std::uint8_t> {
	constexpr static std::uint8_t min = 0;
	constexpr static std::uint8_t max = 255;
};

template<>
struct ColorUnit<float> {
	constexpr static float min = 0;
	constexpr static float max = 1;
};

template<>
struct ColorUnit<double> {
    constexpr static double min = 0;
    constexpr static double max = 1;
};

namespace detail {
template<typename To, typename From>
[[nodiscard]] constexpr To convert_color_unit(From from) noexcept;
}

template<typename T>
struct alignas(sizeof(T) * 4) BasicColor final {
	using ValueType = T;
	using Traits = ColorUnit<T>;

	T r;
	T g;
	T b;
	T a;

	explicit BasicColor(UninitializedTag) noexcept;

	constexpr BasicColor() noexcept;
	/**
	 * Greyscale. Default alpha to `Traits::max`.
	 */
	constexpr explicit BasicColor(T e) noexcept;
	/**
	 * RGB. Defaults alpha to `Traits::max`.
	 */
	constexpr BasicColor(T r, T g, T b) noexcept;
	/**
	 * RGBA.
	 */
	constexpr BasicColor(T r, T g, T b, T a) noexcept;

	/**
	 * Type-converting constructor.
	 * Converts values from one color unit to another.
	 * Conversion from floating point types may be lossy due to rounding.
	 */
	template<typename From>
	constexpr BasicColor(BasicColor<From> from) noexcept; // NOLINT(google-explicit-constructor)

	[[nodiscard]] constexpr bool is_transparent() const noexcept;
	[[nodiscard]] constexpr BasicColor<T> as_greyscale(float r_weight = 0.299f, float g_weight = 0.587f, float b_weight = 0.114f) const noexcept;
	[[nodiscard]] constexpr BasicColor<T> scale(double weight) const noexcept;
};

template<typename T>
[[nodiscard]] constexpr bool operator==(const BasicColor<T> &lhs, const BasicColor<T> &rhs) noexcept;
template<typename T>
[[nodiscard]] constexpr bool operator!=(const BasicColor<T> &lhs, const BasicColor<T> &rhs) noexcept;

using Color = BasicColor<std::uint8_t>;

/**
 * Converts an integer value to a Color. Convenience function for hex values.
 * Only 32bit RGBA values are supported.
 */
[[nodiscard]] constexpr Color operator"" _rgba(unsigned long long int hex) noexcept;
/**
 * See _rgba for details. Omit the two hex digits for alpha.
 */
[[nodiscard]] constexpr Color operator"" _rgb(unsigned long long int hex) noexcept;

struct ColorSet {
	Color normal;
	Color hinting;
	Color active;
};
}

namespace std {
template<typename T>
struct formatter<anchor::BasicColor<T>> {
	auto format(anchor::BasicColor<T> color, format_context &ctx) const;

	constexpr auto parse(format_parse_context &ctx) {
		return ctx.begin();
	}
};
}

#include <anchor/unit/color.inl>

namespace anchor::colors {
[[maybe_unused]] constexpr auto WHITE = 0xffffff_rgb;
[[maybe_unused]] constexpr auto BLACK = 0x000000_rgb;
[[maybe_unused]] constexpr auto GREY = Color{BasicColor<float>{0.5f, 0.5f, 0.5f, 1}};
[[maybe_unused]] constexpr auto LIGHT_GREY = Color{BasicColor<float>{0.75f, 0.75f, 0.75f, 1}};
[[maybe_unused]] constexpr auto DARK_GREY = Color{BasicColor<float>{0.25f, 0.25f, 0.25f, 1}};
[[maybe_unused]] constexpr auto RED = 0xff0000_rgb;
[[maybe_unused]] constexpr auto GREEN = 0x00ff00_rgb;
[[maybe_unused]] constexpr auto BLUE = 0x0000ff_rgb;
[[maybe_unused]] constexpr auto MAGENTA = 0xff00ff_rgb;
[[maybe_unused]] constexpr auto YELLOW = 0xffff00_rgb;
[[maybe_unused]] constexpr auto CYAN = 0x00ffff_rgb;
}
