
#pragma once

#include <cstdint>
#include <format>
#include <type_traits>

#include <anchor/util/tags.hpp>

namespace anchor {
template<typename T>
struct alignas(sizeof(T) * 2) BasicDimension final {
	static_assert(std::is_arithmetic_v<T>);
	using ValueType = T;

	T w;
	T h;

	explicit BasicDimension(UninitializedTag) noexcept;
	constexpr BasicDimension() noexcept;
	constexpr BasicDimension(T w, T h) noexcept;

	template<typename To>
	constexpr BasicDimension<To> into() const noexcept;
};

template<typename T>
[[nodiscard]] constexpr bool operator==(BasicDimension<T> lhs, BasicDimension<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr bool operator!=(BasicDimension<T> lhs, BasicDimension<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicDimension<T> operator+(BasicDimension<T> lhs, BasicDimension<T> rhs) noexcept;

template<typename T>
[[nodiscard]] constexpr BasicDimension<T> operator-(BasicDimension<T> lhs, BasicDimension<T> rhs) noexcept;

template<typename T, typename I>
[[nodiscard]] constexpr BasicDimension<T> operator*(BasicDimension<T> lhs, I rhs) noexcept;

template<typename T, typename I>
[[nodiscard]] constexpr BasicDimension<T> operator/(BasicDimension<T> lhs, I rhs) noexcept;

template<typename T>
constexpr BasicDimension<T>& operator+=(BasicDimension<T> &lhs, BasicDimension<T> rhs) noexcept;

template<typename T>
constexpr BasicDimension<T>& operator-=(BasicDimension<T> &lhs, BasicDimension<T> rhs) noexcept;

template<typename T, typename I>
constexpr BasicDimension<T>& operator*=(BasicDimension<T> &lhs, I rhs) noexcept;

template<typename T, typename I>
constexpr BasicDimension<T>& operator/=(BasicDimension<T> &lhs, I rhs) noexcept;

using Dimension = BasicDimension<std::int32_t>;
}

namespace std {
template<typename T>
struct formatter<anchor::BasicDimension<T>> {
	auto format(anchor::BasicDimension<T> dimension, format_context &ctx) const;

	constexpr auto parse(format_parse_context &ctx) {
		return ctx.begin();
	}
};
}

#include <anchor/unit/dimension.inl>
