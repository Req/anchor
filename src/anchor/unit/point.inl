
#pragma once

#include <format>
#include <string>

#include <anchor/unit/dimension.hpp>
#include <anchor/util/tags.hpp>

namespace anchor {
template<typename T>
BasicPoint<T>::BasicPoint(UninitializedTag) noexcept {}

template<typename T>
constexpr BasicPoint<T>::BasicPoint() noexcept : BasicPoint<T>{0, 0} {}

template<typename T>
constexpr BasicPoint<T>::BasicPoint(const T x, const T y) noexcept : x{x}, y{y} {}

template<typename From>
template<typename To>
constexpr BasicPoint<To> BasicPoint<From>::into() const noexcept {
	return {static_cast<To>(x), static_cast<To>(y)};
}

template<typename T>
constexpr BasicDimension<T> BasicPoint<T>::distance_to(const BasicPoint<T> other) const noexcept {
	return {other.x - x, other.y - y};
}

template<typename T>
constexpr bool operator==(const BasicPoint<T> lhs, const BasicPoint<T> rhs) noexcept {
	return lhs.x == rhs.x && lhs.y == rhs.y;
}

template<typename T>
constexpr bool operator!=(const BasicPoint<T> lhs, const BasicPoint<T> rhs) noexcept {
	return !(lhs == rhs);
}

template<typename T>
constexpr BasicPoint<T> operator+(const BasicPoint<T> point, const BasicDimension<T> dimension) noexcept {
	return {point.x + dimension.w, point.y + dimension.h};
}

template<typename T>
constexpr BasicPoint<T> operator-(const BasicPoint<T> point, const BasicDimension<T> dimension) noexcept {
	return {point.x - dimension.w, point.y - dimension.h};
}

template<typename T>
constexpr BasicPoint<T> operator+(const BasicPoint<T> lhs, const BasicPoint<T> rhs) noexcept {
	return {lhs.x + rhs.x, lhs.y + rhs.y};
}

template<typename T>
constexpr BasicPoint<T> operator-(const BasicPoint<T> lhs, const BasicPoint<T> rhs) noexcept {
	return {lhs.x - rhs.x, lhs.y - rhs.y};
}

template<typename T>
constexpr BasicPoint<T>& operator+=(BasicPoint<T> &point, const BasicDimension<T> dimension) noexcept {
	point = point + dimension;
	return point;
}

template<typename T>
constexpr BasicPoint<T>& operator-=(BasicPoint<T> &point, const BasicDimension<T> dimension) noexcept {
	point = point - dimension;
	return point;
}

template<typename T>
constexpr BasicPoint<T>& operator+=(BasicPoint<T> &lhs, const BasicPoint<T> rhs) noexcept {
	lhs = lhs + rhs;
	return lhs;
}

template<typename T>
constexpr BasicPoint<T>& operator-=(BasicPoint<T> &lhs, const BasicPoint<T> rhs) noexcept {
	lhs = lhs - rhs;
	return lhs;
}
}

namespace std { // NOLINT(cert-dcl58-cpp)
template<typename T>
auto formatter<anchor::BasicPoint<T>>::format(const anchor::BasicPoint<T> point, format_context &ctx) const {
	return std::format_to(ctx.out(), "Point{{{}, {}}}", point.x, point.y);
}
}
