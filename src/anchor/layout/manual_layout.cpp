
#include <optional>

#include <anchor/component.hpp>
#include <anchor/layout/manual_layout.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
void ManualLayout::redo(const Component&) noexcept {}

std::optional<Rectangle> ManualLayout::bounds_of(const Component&) noexcept {
	return std::nullopt;
}
}
