
#include <algorithm>
#include <optional>
#include <utility>
#include <vector>

#include <anchor/component.hpp>
#include <anchor/layout/anchor_layout.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
namespace detail {
AnchorPoint::AnchorPoint(const Anchor anchor, const Component &relative_component, const Anchor relative_anchor, const Dimension offset) noexcept :
	anchor{anchor},
	relative_anchor{relative_anchor},
	relative_component{relative_component.id()},
	offset{offset} {}
}

[[nodiscard]] static bool is_relationship_legal(const Component &component, const Component &relative_component, const ComponentId layout_owner) noexcept {
	if (component.parent() == nullptr) {
		return false;
	}
	const auto target_is_child_of_relative = component.parent()->id() == layout_owner;
	const auto target_is_sibling_of_relative = relative_component.parent() != nullptr && component.parent()->id() == relative_component.parent()->id();
	return target_is_child_of_relative || target_is_sibling_of_relative;
}

void AnchorLayout::set_point_impl(const Component &component, Anchor anchor, const Component &relative_component, Anchor relative_anchor, Dimension offset) noexcept {
	// valid branch as long as clear_points erases the entries instead of clearing them
	if (auto it = _anchors.find(component.id()); it != _anchors.end()) {
		it->second.emplace_back(anchor, relative_component, relative_anchor, offset);
	} else {
		_anchors[component.id()].emplace_back(anchor, relative_component, relative_anchor, offset);
		_order.emplace_back(component.id());
	}
}

bool AnchorLayout::set_point(const Component &component, const Anchor anchor, const Component &relative_component, const Anchor relative_anchor, const Dimension offset) noexcept {
	if (!is_relationship_legal(component, relative_component, _owner)) {
		return false;
	}
	set_point_impl(component, anchor, relative_component, relative_anchor, offset);
	return true;
}

bool AnchorLayout::find_cycle(const Component &adding, const ComponentId vertex) noexcept {
	/*
	 * If component A is anchored to relative component B, then the edge A->B is being added.
	 * The goal here is to look for a path B--->A, so the search begins on `relative_component`.
	 * This logic could be reversed, though, and the result would remain the same.
	 */
	if (vertex == adding.id()) {
		// a path B--->A has been found
		return true;
	}
	if (const auto it = _anchors.find(vertex); it != _anchors.end()) {
		for (const auto &anchor : it->second) {
			if (find_cycle(adding, anchor.relative_component)) {
				return true;
			}
		}
	}
	return false;
}

bool AnchorLayout::set_point_acyclic(const Component &component, Anchor anchor, const Component &relative_component, Anchor relative_anchor, Dimension offset) noexcept {
	if (!is_relationship_legal(component, relative_component, _owner)) {
		return false;
	}
	/*
	 * The DFS is performed every time. There's no cache because `clear_points`
	 * would invalidate most/all caches. I don't think it's appropriate for
	 * `clear_points` to have the performance hit for rebuilding the cache,
	 * and I don't want to keep partially-incomplete caches around.
	 */
	if (find_cycle(component, relative_component.id())) {
		return false;
	}
	set_point_impl(component, anchor, relative_component, relative_anchor, offset);
	return true;
}

bool AnchorLayout::clear_points(const Component &component, const bool purge) noexcept {
	if (component.parent() == nullptr || component.parent()->id() != _owner) {
		// not owned by this layout's owner
		return false;
	}

	if (auto it = _anchors.find(component.id()); it != _anchors.end()) {
		// need to actually remove the entry instead of clearing the vector because set_point checks for its existence
		_anchors.erase(it);
	}
	if (purge) {
		for (auto &[_, points] : _anchors) {
			points.erase(std::remove_if(points.begin(), points.end(), [&component](const auto &point) noexcept {
				return point.relative_component == component.id();
			}), points.end());
		}
	}
	return true;
}

bool AnchorLayout::fill_parent(const Component &component, const Dimension inset) noexcept {
	const auto *parent = component.parent();
	if (parent == nullptr) {
		// can't fill a parent that doesn't exist
		return false;
	}
	const auto success =
		set_point(component, Anchor::TOP_LEFT, *parent, Anchor::TOP_LEFT, inset) &&
		set_point(component, Anchor::BOTTOM_RIGHT, *parent, Anchor::BOTTOM_RIGHT, inset * -1);
	if (!success) {
		clear_points(component, true);
		return false;
	}
	return true;
}

[[nodiscard]] static Point position_of(const Rectangle bounds, const Anchor &anchor) noexcept {
	Point ret;
	if ((anchor & Anchor::TOP) > 0) {
		ret.y = bounds.y;
	} else if ((anchor & Anchor::BOTTOM) > 0) {
		ret.y = bounds.y + bounds.h;
	} else {
		ret.y = bounds.y + bounds.h / 2;
	}
	if ((anchor & Anchor::LEFT) > 0) {
		ret.x = bounds.x;
	} else if ((anchor & Anchor::RIGHT) > 0) {
		ret.x = bounds.x + bounds.w;
	} else {
		ret.x = bounds.x + bounds.w / 2;
	}
	return ret;
}

template<typename T>
[[nodiscard]] static std::pair<T, T> halves(const T whole) noexcept {
	const auto some_half = whole / 2;
	// may not divide perfectly even, so make sure that both halves equal the whole
	const auto other_half = whole - some_half;
	return {some_half, other_half};
}

void AnchorLayout::redo(const Component &owner) noexcept {
	_assigned_bounds.clear();
	_assigned_bounds.emplace(_owner, Rectangle{{0, 0}, owner.get_size()});
	for (const auto component : _order) {
		Point top_left{0, 0};
		Point bottom_right{0, 0};
		for (const auto &point : _anchors[component]) {
			if (const auto it = _assigned_bounds.find(point.relative_component); it != _assigned_bounds.end()) {
				const auto size = top_left.distance_to(bottom_right);
				const auto target = position_of(it->second, point.relative_anchor) + point.offset;
				switch (point.anchor) {
					case Anchor::CENTER: {
						// just center it in whatever state it's currently in
						const auto [left_half, right_half] = halves(size);
						top_left = target - left_half;
						bottom_right = target + right_half;
						break;
					}
					case Anchor::TOP: {
						// set x coordinates, then set top y, then check that bottom y is below top y
						const auto [left_size, right_size] = halves(size.w);
						top_left.x = target.x - left_size;
						bottom_right.x = target.x + right_size;
						top_left.y = target.y;
						if (bottom_right.y <= top_left.y) {
							bottom_right.y = top_left.y + size.h;
						}
						break;
					}
					case Anchor::BOTTOM: {
						// set x coordinates, then set bottom y, then check that top y is above bottom y
						const auto [left_size, right_size] = halves(size.w);
						top_left.x = target.x - left_size;
						bottom_right.x = target.x + right_size;
						bottom_right.y = target.y;
						if (top_left.y >= bottom_right.y) {
							top_left.y = bottom_right.y - size.h;
						}
						break;
					}
					case Anchor::LEFT: {
						// set y coordinates, then set left x, then check that right x is right of left x
						const auto [top_size, bottom_size] = halves(size.h);
						top_left.y = target.y - top_size;
						bottom_right.y = target.y + bottom_size;
						top_left.x = target.x;
						if (bottom_right.x <= top_left.x) {
							bottom_right.x = top_left.x + size.w;
						}
						break;
					}
					case Anchor::RIGHT: {
						// set y coordinates, then set right x, then check that left x is left of right x
						const auto [top_size, bottom_size] = halves(size.h);
						top_left.y = target.y - top_size;
						bottom_right.y = target.y + bottom_size;
						bottom_right.x = target.x;
						if (top_left.x >= bottom_right.x) {
							top_left.x = bottom_right.x - size.w;
						}
						break;
					}
					case Anchor::TOP_LEFT:
						// assign top left, then check that bottom right is below and right of top left
						top_left = target;
						if (bottom_right.x <= top_left.x) {
							bottom_right.x = top_left.x + size.w;
						}
						if (bottom_right.y <= top_left.y) {
							bottom_right.y = top_left.y + size.h;
						}
						break;
					case Anchor::TOP_RIGHT:
						// assign right x and top y, then check that left x is left of right x and bottom y is below top y
						bottom_right.x = target.x;
						top_left.y = target.y;
						if (top_left.x >= bottom_right.x) {
							top_left.x = bottom_right.x - size.w;
						}
						if (bottom_right.y <= top_left.y) {
							bottom_right.y = top_left.y + size.h;
						}
						break;
					case Anchor::BOTTOM_LEFT:
						// assign left x and bottom y, then check that right x is right of left x and top y is above bottom y
						top_left.x = target.x;
						bottom_right.y = target.y;
						if (bottom_right.x <= top_left.x) {
							bottom_right.x = top_left.x + size.w;
						}
						if (top_left.y >= bottom_right.y) {
							top_left.y = bottom_right.y - size.h;
						}
						break;
					case Anchor::BOTTOM_RIGHT:
						// assign bottom right, then check that top left is above and left of bottom right
						bottom_right = target;
						if (top_left.x >= bottom_right.x) {
							top_left.x = bottom_right.x - size.w;
						}
						if (top_left.y >= bottom_right.y) {
							top_left.y = bottom_right.y - size.h;
						}
						break;
				}
			}
		}

		const Rectangle bounds{top_left, top_left.distance_to(bottom_right)};
		_assigned_bounds.emplace(component, bounds);
	}
}

std::optional<Rectangle> AnchorLayout::bounds_of(const Component &component) noexcept {
	if (auto it = _assigned_bounds.find(component.id()); it != _assigned_bounds.end()) {
		return it->second;
	}
	return std::nullopt;
}
}
