
#pragma once

#include <cstdint>
#include <optional>
#include <type_traits>
#include <unordered_map>
#include <vector>

#include <anchor/component_fwd.hpp>
#include <anchor/layout.hpp>
#include <anchor/util/enum_operators.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
/**
 * Anchoring a cardinal of a component to another component has a centering effect
 * around the reference anchor. For example, anchoring the top of component A to the
 * bottom right of component B will cause the top middle of A to touch the bottom right
 * corner of B.
 *
 * Intercardinal anchoring has a stretching or shrinking effect.
 */
enum class Anchor : std::uint8_t {
	CENTER       [[maybe_unused]] = 0,
	TOP          [[maybe_unused]] = 0b0000'0001,
	BOTTOM       [[maybe_unused]] = 0b0000'0010,
	LEFT         [[maybe_unused]] = 0b0000'0100,
	RIGHT        [[maybe_unused]] = 0b0000'1000,
	TOP_LEFT     [[maybe_unused]] = TOP | LEFT,
	TOP_RIGHT    [[maybe_unused]] = TOP | RIGHT,
	BOTTOM_LEFT  [[maybe_unused]] = BOTTOM | LEFT,
	BOTTOM_RIGHT [[maybe_unused]] = BOTTOM | RIGHT,

	METADATA_ENABLE_OPERATOR_OR [[maybe_unused]],
	METADATA_ENABLE_OPERATOR_AND [[maybe_unused]],
	METADATA_ENABLE_OPERATOR_XOR [[maybe_unused]]
};

template<typename T>
[[nodiscard]] constexpr bool operator>(const Anchor lhs, const T rhs) noexcept {
	static_assert(std::is_integral_v<T>);
	return static_cast<T>(lhs) > rhs;
}

static_assert(!((Anchor::TOP & Anchor::BOTTOM & Anchor::LEFT & Anchor::RIGHT) > 0));

namespace detail {
struct AnchorPoint final {
	Anchor anchor;
	Anchor relative_anchor;
	ComponentId relative_component;
	Dimension offset;

	AnchorPoint(Anchor anchor, const Component &relative_component, Anchor relative_anchor, Dimension offset) noexcept;
};
}

/**
 * A layout wherein components are placed relative to other components via anchors.
 */
class AnchorLayout : public Layout {
private:
	std::vector<ComponentId> _order;
	std::unordered_map<ComponentId, std::vector<detail::AnchorPoint>> _anchors;
	std::unordered_map<ComponentId, Rectangle> _assigned_bounds;

	void set_point_impl(const Component &component, Anchor anchor, const Component &relative_component, Anchor relative_anchor, Dimension offset) noexcept;
	[[nodiscard]] bool find_cycle(const Component &adding, ComponentId vertex) noexcept;

public:
	using Layout::Layout;

	/**
	 * Declares a positional relationship between two components.
	 *
	 * Dependencies are not resolved automatically. If component A requires component B to be placed first,
	 * then component B should have its points set before those of component A. Components are placed by
	 * order of which components first set points.
	 *
	 * @return False if setting this point would be illegal, such as if the components are unrelated.
	 */
	bool set_point(const Component &component, Anchor anchor, const Component &relative_component, Anchor relative_anchor, Dimension offset = {0, 0}) noexcept;
	/**
	 * This function is equivalent to `set_point` but takes additional time to
	 * make sure that setting the point wouldn't create a dependency cycle.
	 *
	 * Component anchor dependencies should create a DAG, as cycles prevent anchors from being satisfied.
	 * Theorem: In a DAG, if a directed path exists from vertex B to vertex A, then adding the directed edge A->B creates a cycle.
	 *
	 * Be aware that checking this involves searching the tree for a specific path via DFS.
	 * This means a time complexity of at least O(V+E), where vertices are components and edges are anchors.
	 *
	 * @reutrn False if setting this point would create a cycle, or if `set_point` returns false.
	 */
	bool set_point_acyclic(const Component &component, Anchor anchor, const Component &relative_component, Anchor relative_anchor, Dimension offset = {0, 0}) noexcept;
	/**
	 * Clears anchored points for the given component.
	 * @param purge If true, looks for references to this component in other component's anchors and also removes them.
	 * @return True if successful.
	 */
	bool clear_points(const Component &component, bool purge = false) noexcept;

	/**
	 * A convenience wrapped that calls the following (simplified for demonstration):
	 * - set_point(component, TOP_LEFT, component.parent, TOP_LEFT)
	 * - set_point(component, BOTTOM_RIGHT, component.parent, BOTTOM_RIGHT)
	 * The result of set_point is not checked and none, one, or both points may actually have been set.
	 *
	 * @return True if successful.
	 */
	bool fill_parent(const Component &component, Dimension inset = {0, 0}) noexcept;

	void redo(const Component &owner) noexcept override;
	[[nodiscard]] std::optional<Rectangle> bounds_of(const Component &component) noexcept override;
};
}
