
#pragma once

#include <optional>

#include <anchor/component_fwd.hpp>
#include <anchor/layout.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
/**
 * Does not manage the layout.
 * This allows manual management via set_bounds.
 */
class [[maybe_unused]] ManualLayout : public Layout {
public:
	using Layout::Layout;

	void redo(const Component&) noexcept override;

	std::optional<Rectangle> bounds_of(const Component&) noexcept override;
};
}
