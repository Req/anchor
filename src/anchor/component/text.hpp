
#pragma once

#include <cstdint>
#include <optional>
#include <string>
#include <string_view>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/event.hpp>
#include <anchor/font.hpp>
#include <anchor/unit/color.hpp>
#include <anchor/unit/point.hpp>

namespace anchor {
struct Text : public EnabledByDefault<false>, public FocusableByDefault<false> {
	struct DefaultStyle {
		constexpr static auto TEXT_COLOR = colors::WHITE;
	};

	enum class Alignment {
		LEFT   [[maybe_unused]],
		RIGHT  [[maybe_unused]],
		CENTER [[maybe_unused]]
	};

	enum class Wrap : bool {
		WRAP    [[maybe_unused]],
		NO_WRAP [[maybe_unused]]
	};

	struct Data {
		std::string text;
		const Font &font;
		Alignment alignment = Alignment::LEFT;
		Wrap wrap = Wrap::NO_WRAP;
		Point last_endpoint;

		explicit Data(std::string_view text, const Font &font) noexcept;

		/**
		 * `text` and `font` are always required. All other configuration values
		 * are optional and can be provided in any order via this constructor.
		 */
		template<typename T, typename... Args>
		explicit Data(std::string_view text, const Font &font, T value, const Args... args) noexcept;

	private:
		void initialize(Alignment alignment) noexcept;
		void initialize(Wrap wrap) noexcept;
	};

	template<typename Style>
	static void default_repaint(Component &self, Data &data, Brush &brush) noexcept;

	constexpr static RepaintHandler<Data> repaint_handler = default_repaint<DefaultStyle>;
	constexpr static EventHandler<Data> event_handler = propagate_event<Data>;
};
}

#include <anchor/component/text.inl>
