
#pragma once

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/unit/color.hpp>

namespace anchor {
template<typename Style>
void Button::default_repaint(Component &self, Data &data, Brush &brush) noexcept {
	const auto bounds = self.get_absolute_bounds();
	brush.set_clip(bounds);

	Color ColorSet::*theme;
	if (data.pressed) {
		theme = &ColorSet::active;
	} else if (data.hovered) {
		theme = &ColorSet::hinting;
	} else {
		theme = &ColorSet::normal;
	}

	brush.set_color(Style::BACKGROUND_COLOR.*theme);
	brush.fill_rectangle(bounds);
	if (Style::BORDER_THICKNESS > 0) {
		brush.set_color(Style::BORDER_COLOR.*theme);
		brush.draw_border(bounds, Style::BORDER_THICKNESS);
	}

	BaseComponent::repaint_handler(self, BaseComponent::no_data(), brush);
}
}
