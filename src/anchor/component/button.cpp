
#include <variant>

#include <anchor/component.hpp>
#include <anchor/event.hpp>
#include <anchor/component/button.hpp>

namespace anchor {
Button::Data::Data() noexcept : hovered{false}, pressed{false} {}

EventStatus Button::event_handler(Component &self, Data &data, const Event &event) {
	return std::visit(match_event {
		[&](const MouseClickEvent &event) {
			switch (event.type) {
			case MouseClickEvent::Type::PRESS:
				data.pressed = true;
				break;
			case MouseClickEvent::Type::RELEASE:
				data.pressed = false;
				if (!self.get_absolute_bounds().contains(event.absolute_location)) {
					data.hovered = false;
				}
				break;
			}
			return EventStatus::CONSUMED;
		},
		[&](const MouseMoveEvent &event) {
			data.hovered = !event.leaving;
			return EventStatus::CONSUMED;
		},
		propagate_other_events
	}, event);
}
}
