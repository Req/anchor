
#pragma once

#include <string>

#include <anchor/brush.hpp>
#include <anchor/component_fwd.hpp>
#include <anchor/event.hpp>
#include <anchor/unit/color.hpp>
#include <anchor/component/text.hpp>

namespace anchor {
struct Input {
	struct DefaultStyle {
		constexpr static auto TEXT_COLOR = colors::WHITE;
	};

	struct Data {
		Text::Data text;

		/**
		 * This component uses a `Text` component internally.
		 * Its constructor forwards to `Text::Data`.
		 */
		template<typename... Args>
		explicit Data(const Args... args) noexcept;
	};

	template<typename Style>
	static void default_repaint(Component &self, Data &data, Brush &brush) noexcept;

	constexpr static RepaintHandler<Data> repaint_handler = default_repaint<DefaultStyle>;

	static EventStatus event_handler(Component &self, Data &data, const Event &event) noexcept;
};
}

#include <anchor/component/input.inl>
