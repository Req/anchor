
#pragma once

#include <anchor/brush.hpp>
#include <anchor/event.hpp>
#include <anchor/component_fwd.hpp>
#include <anchor/unit/color.hpp>

namespace anchor {
struct Button {
	struct DefaultStyle {
		struct detail {
			constexpr static auto BACKGROUND_COLOR = 0x0074d9_rgb;
			constexpr static auto BORDER_COLOR = 0x000f1e_rgb;
		};

		constexpr static ColorSet BACKGROUND_COLOR{
			.normal = detail::BACKGROUND_COLOR,
			.hinting = detail::BACKGROUND_COLOR.scale(0.8),
			.active = detail::BACKGROUND_COLOR.scale(0.6)
		};
		constexpr static ColorSet BORDER_COLOR{
			.normal = detail::BORDER_COLOR,
			.hinting = detail::BORDER_COLOR.scale(0.8),
			.active = detail::BORDER_COLOR.scale(0.6)
		};
		constexpr static auto BORDER_THICKNESS = 3;
	};

	struct Data {
		bool hovered;
		bool pressed;

		Data() noexcept;
	};

	template<typename Style>
	static void default_repaint(Component &self, Data &data, Brush &brush) noexcept;

	constexpr static RepaintHandler<Data> repaint_handler = default_repaint<DefaultStyle>;
	static EventStatus event_handler(Component &self, Data &data, const Event &event);
};
}

#include <anchor/component/button.inl>
