
#include <cctype>
#include <cstdint>
#include <numeric>
#include <optional>
#include <ranges>
#include <string_view>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/component/text.hpp>
#include <anchor/font.hpp>
#include <anchor/unit/dimension.hpp>

namespace anchor {
Text::Data::Data(const std::string_view text, const Font &font) noexcept : text{text}, font{font} {}

void Text::Data::initialize(const Alignment alignment) noexcept {
	this->alignment = alignment;
}

void Text::Data::initialize(const Wrap wrap) noexcept {
	this->wrap = wrap;
}

[[nodiscard]] static std::string_view trim(std::string_view text) noexcept {
	std::size_t removing = 0;
	for (const auto c : text) {
		if (std::isspace(c)) {
			++removing;
		} else {
			break;
		}
	}
	text.remove_prefix(removing);
	removing = 0;
	for (const auto c : std::ranges::reverse_view(text)) {
		if (std::isspace(c)) {
			++removing;
		} else {
			break;
		}
	}
	text.remove_suffix(removing);
	return text;
}

namespace detail {
std::string_view TextFitter::consume(const std::size_t count) noexcept {
	const auto sub = trim(_buffer.substr(0, count));
	_buffer = trim(_buffer.substr(count));
	return sub;
}

TextFitter::TextFitter(const std::string_view buffer, const Font &font) noexcept : _buffer{trim(buffer)}, _font{font} {}

std::optional<std::string_view> TextFitter::next(std::int32_t available_width) noexcept {
	if (_buffer.empty()) {
		return std::nullopt;
	}
	if (static_cast<std::int32_t>(_font[_buffer[0]].advance) >= available_width) {
		return {consume(1)};
	}

	auto last_space = std::string_view::npos;
	for (std::size_t k = 0; k < _buffer.length(); ++k) {
		switch (_buffer[k]) {
		case '\n':
			return {consume(k + 1)};
		case ' ':
			last_space = k;
			break;
		}

		available_width -= static_cast<std::int32_t>(_font[_buffer[k]].advance);
		if (available_width <= 0) {
			if (last_space == std::string_view::npos) {
				return {consume(k + 1)};
			}
			return {consume(last_space)};
		}
	}
	return {consume(_buffer.length())};
}

std::int32_t render_text_ltr(const std::string_view text, const Font &font, Brush &brush, const Rectangle bounds) noexcept {
	auto left = 0;
	for (const auto c : trim(text)) {
		const auto &metrics = font[c];
		const auto location = bounds.location() + Dimension{
			left + metrics.bearing.w,
			font.max_y_bearing - metrics.bearing.h
		};
		brush.draw_image(font.texture(), {location, metrics.size}, brush.get_color(), metrics.uv);
		left += static_cast<int>(metrics.advance);
	}
	return left;
}

std::int32_t render_text_rtl(const std::string_view text, const Font &font, Brush &brush, const Rectangle bounds) noexcept {
	auto right = bounds.w;
	for (const auto c : std::ranges::reverse_view(trim(text))) {
		const auto &metrics = font[c];
		const auto location = bounds.location() + Dimension{
			static_cast<Dimension::ValueType>(right - metrics.advance + metrics.bearing.w),
			font.max_y_bearing - metrics.bearing.h
		};
		brush.draw_image(font.texture(), {location, metrics.size}, brush.get_color(), metrics.uv);
		right -= static_cast<int>(metrics.advance);
	}
	return right;
}

std::int32_t render_text_centered(const std::string_view text_, const Font &font, Brush &brush, const Rectangle bounds) noexcept {
	const auto text = trim(text_);
	const auto width = std::accumulate(text.begin(), text.end(), std::size_t{}, [&font](const auto width, const auto c) noexcept {
		return width + font[c].advance;
	});
	const auto left = static_cast<Dimension::ValueType>(bounds.x + (bounds.w - width) / 2);
	return render_text_ltr(text, font, brush, {{left, bounds.y}, bounds.size()});
}
}
}
