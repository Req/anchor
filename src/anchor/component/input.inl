
#include <utility>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/component/text.hpp>
#include <anchor/unit/dimension.hpp>

namespace anchor {
template<typename... Args>
Input::Data::Data(const Args... args) noexcept : text{std::forward<Args...>(args...)} {}

namespace detail {
[[nodiscard]] bool should_draw_caret() noexcept;
}

template<typename Style>
void Input::default_repaint(Component &self, Data &data, Brush &brush) noexcept {
	/*
	 * TODO
	 *
	 * need to pass in whether or not the component is focused
	 *
	 * this might be an opportunity to introduce a State argument to repaint
	 *
	 * also create a TextDrawCursor (with a better name?) that keeps track of the current
	 * position as characters are drawn. should just be able to loop:
	 * ```
	 * cursor cursor(font, align, etc);
	 * for c in s
	 *     brush.draw_image(..., cursor.next(c));
	 * ```
	 */

	if (self.enabled) {
		// default_text_paint<Style::Enabled>
	} else {
		// default_text_paint<Style::Disabled>
	}

	detail::default_text_paint<Style>(self, data.text, brush);
	if (detail::should_draw_caret()) {
		brush.set_color(colors::WHITE);
		brush.draw_line(data.text.last_endpoint, data.text.last_endpoint + Dimension{data.text.font.max_y_bearing, 0}, 1);
	}
}
}
