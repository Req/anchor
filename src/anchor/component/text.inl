
#pragma once

#include <cstdint>
#include <limits>
#include <optional>
#include <string_view>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/font.hpp>
#include <anchor/unit/color.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
namespace detail {
class TextFitter {
private:
	std::string_view _buffer;
	const Font &_font;

	[[nodiscard]] std::string_view consume(std::size_t count) noexcept;

public:
	explicit TextFitter(std::string_view buffer, const Font &font) noexcept;

	[[nodiscard]] std::optional<std::string_view> next(std::int32_t available_width) noexcept;
};

using AlignedRenderer = std::int32_t (*)(const std::string_view, const Font&, Brush&, const Rectangle) noexcept;

std::int32_t render_text_ltr(std::string_view text, const Font &font, Brush &brush, Rectangle bounds) noexcept;
std::int32_t render_text_rtl(std::string_view text, const Font &font, Brush &brush, Rectangle bounds) noexcept;
std::int32_t render_text_centered(std::string_view text_, const Font &font, Brush &brush, Rectangle bounds) noexcept;

template<typename Style>
void default_text_paint(Component &self, Text::Data &data, Brush &brush) noexcept {
	if (data.text.empty()) {
		data.last_endpoint = self.get_absolute_location();
		return;
	}

	brush.set_color(Style::TEXT_COLOR);
	const auto bounds = self.get_absolute_bounds();
	brush.set_clip(bounds);

	detail::AlignedRenderer renderer;
	switch (data.alignment) {
	case Text::Alignment::LEFT:
		renderer = &detail::render_text_ltr;
		break;
	case Text::Alignment::RIGHT:
		renderer = &detail::render_text_rtl;
		break;
	case Text::Alignment::CENTER:
		renderer = &detail::render_text_centered;
		break;
	}

	std::int32_t available_width;
	if (data.wrap == Text::Wrap::WRAP) {
		available_width = bounds.w;
	} else {
		available_width = std::numeric_limits<std::int32_t>::max();
	}

	detail::TextFitter fitter{data.text, data.font};
	auto top = bounds.y;
	std::int32_t endpoint = 0;
	while (true) {
		const auto text = fitter.next(available_width);
		if (!text.has_value()) {
			break;
		}

		endpoint = renderer(*text, data.font, brush, {{bounds.x, top}, bounds.size()});
		top += data.font.max_y_bearing;
	}
	data.last_endpoint = {endpoint, top};
}
}

template<typename T, typename... Args>
Text::Data::Data(const std::string_view text, const Font &font, const T value, const Args... args) noexcept : Data{text, font, args...} { // NOLINT(cppcoreguidelines-pro-type-member-init)
	initialize(value);
}

template<typename Style>
void Text::default_repaint(Component &self, Data &data, Brush &brush) noexcept {
	detail::default_text_paint<Style>(self, data, brush);
	BaseComponent::repaint_handler(self, BaseComponent::no_data(), brush);
}
}
