
#pragma once

#include <cstdint>

#include <anchor/brush.hpp>
#include <anchor/component_fwd.hpp>
#include <anchor/event.hpp>
#include <anchor/unit/point.hpp>

namespace anchor {
/**
 * A scrollable container for other components.
 *
 * This component has to move its children around when drawing them,
 * and if no layout exists to reset their positions each repaint,
 * they're likely to scroll to infinity very quickly, among other issues.
 *
 * As a result, this component expects to have a layout that manages all
 * of its children. To comply with `noexcept`-correctness for `repaint`,
 * this is enforced only by a debug assertion.
 */
struct Scroll {
	struct Data {
		enum class Interaction : std::uint8_t {
			GRAB_VSCROLL,
			GRAB_HSCROLL,
			NONE
		};

		Point top_left_offset;
		Rectangle total_bounds;
		Rectangle vscroll_bounds;
		Rectangle hscroll_bounds;
		Interaction interaction = Interaction::NONE;

		Data() noexcept = default;

		void vscroll_to(const Component &self, float percent) noexcept;
		void hscroll_to(const Component &self, float percent) noexcept;
	};

	constexpr static auto SCROLL_SPEED = 20;

	static void repaint_handler(Component &self, Data &data, Brush &brush) noexcept;
	static EventStatus event_handler(Component &self, Data &data, const Event &event);
};
}
