
#include <algorithm>
#include <cassert>
#include <cmath>
#include <utility>
#include <variant>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/component/scroll.hpp>
#include <anchor/event.hpp>

namespace anchor {
inline static void clamp_scroll(Component &self, Scroll::Data &data) noexcept {
	// sanity checking: x,y should be within (-infinity, 0]
	if (data.top_left_offset.x > 0) {
		data.top_left_offset.x = 0;
	}
	if (data.top_left_offset.y > 0) {
		data.top_left_offset.y = 0;
	}

	const auto physical_bounds = self.get_bounds();
	const Rectangle virtual_bounds{data.top_left_offset + physical_bounds.location(), data.total_bounds.size()};
	if (!virtual_bounds.contains(physical_bounds)) {
		/*
		 * Possible scenario, if virtual size exceeds actual size:
		 * `top_left` is too far away and we've scrolled past the end.
		 * Each dimension (x or y) gets adjusted individually to try
		 * preserving the current scroll distance as much as possible.
		 */
		if (virtual_bounds.w <= physical_bounds.w) {
			data.top_left_offset.x = 0;
		} else {
			const auto x_diff = (physical_bounds.x + physical_bounds.w) - (virtual_bounds.x + virtual_bounds.w);
			if (x_diff > 0) {
				data.top_left_offset.x += x_diff;
			}
		}
		if (virtual_bounds.h <= physical_bounds.h) {
			data.top_left_offset.y = 0;
		} else {
			const auto y_diff = (physical_bounds.y + physical_bounds.h) - (virtual_bounds.y + virtual_bounds.h);
			if (y_diff > 0) {
				data.top_left_offset.y += y_diff;
			}
		}
	}
}

[[nodiscard]] inline static Rectangle find_total_bounds(Component &self) noexcept {
	Rectangle total_bounds;
	auto *layout = self.get_layout();
	for (auto &child : self) {
		auto bounds = child->get_bounds();
		if (layout != nullptr) {
			if (const auto result = layout->bounds_of(*child); result.has_value()) {
				bounds = *result;
				child->set_bounds(bounds);
			} else {
				assert("Children of Scroll components are expected to be managed by the Scroll's layout" && false);
			}
		}
		total_bounds.include(bounds);
	}
	return total_bounds;
}

inline static void draw_children_offset(Component &self, Brush &brush, const Rectangle bounds, const Point offset) noexcept {
	auto lock = brush.lock_clip(bounds);
	/*
	 * This is done in a separate iteration because `top_left` has to be
	 * adjusted before it can be applied, but determining whether it
	 * needs to be adjusted requires `total_bounds`, which can only be
	 * obtained by first iterating over all children.
	 */
	for (auto &child : self) {
		const auto bounds = child->get_bounds();
		child->set_bounds(bounds + offset);
		child->repaint(brush);
	}
}

template<typename T>
struct Range {
	const T lower_bound;
	const T distance;

	constexpr Range(const T lower_bound, const T upper_bound) noexcept : lower_bound{lower_bound}, distance{upper_bound - lower_bound} {
		assert(lower_bound <= upper_bound);
	}

	[[nodiscard]] constexpr float to_percent(const T value) const noexcept {
		assert(lower_bound <= value);
		assert(value <= lower_bound + distance);
		const auto index = static_cast<float>(value) - static_cast<float>(lower_bound);
		return index / distance;
	}

	[[nodiscard]] constexpr T from_percent(const float percent) const noexcept {
		assert(0.0f <= percent);
		assert(percent <= 1.0f);
		const auto value = static_cast<T>(std::round(static_cast<float>(distance)) * percent);
		return value + lower_bound;
	}

	[[nodiscard]] constexpr T clamp(const T value) const noexcept {
		return std::clamp(value, lower_bound, lower_bound + distance);
	}
};

constexpr auto TAB_THICKNESS = 20;
// the tab will be smallest when virtual size exceeds physical size by this factor
constexpr auto TAB_SIZE_MAX_FACTOR = 10.0f;
// the tab will not be smaller than this percent of the physical size
constexpr auto MIN_TAB_SIZE_PERCENT = 0.01f;
constexpr Range SIZE_RANGE{MIN_TAB_SIZE_PERCENT, 1.0f};

template<Rectangle::ValueType Rectangle::*Start, Rectangle::ValueType Rectangle::*Span>
[[nodiscard]] static std::pair<Rectangle::ValueType, Rectangle::ValueType> tab_length_and_location(const Rectangle virtual_bounds, const Rectangle physical_bounds, const Point::ValueType top_left_offset) noexcept {
	const auto vspan = static_cast<float>(virtual_bounds.*Span);
	const auto pspan = static_cast<float>(physical_bounds.*Span);

	/*
	 * As the ratio of `virtual_size / physical_size` approaches `TAB_SIZE_MAX_FACTOR`,
	 * the tab size will approach `physical_size * MIN_TAB_SIZE_PERCENT`.
	 *
	 * The (top left of the) tab can be placed anywhere from `start` to `span - tab_size`.
	 */
	const auto size_factor = 1.0f / std::clamp(vspan / pspan, 1.0f, TAB_SIZE_MAX_FACTOR);
	const auto tab_length = static_cast<Rectangle::ValueType>(SIZE_RANGE.from_percent(size_factor) * pspan);
	const Range scroll_range{0.0f, vspan - pspan};
	const Range span_range{physical_bounds.*Start, physical_bounds.*Start + physical_bounds.*Span - tab_length};
	const auto scroll_location = span_range.from_percent(scroll_range.to_percent(static_cast<float>(-top_left_offset)));
	return {tab_length, scroll_location};
}

void Scroll::repaint_handler(Component &self, Data &data, Brush &brush) noexcept {
	auto *layout = self.get_layout();
	if (layout != nullptr) {
		layout->redo(self);
	} else {
		assert("Scroll components are expected to have a layout" && false);
	}
	data.total_bounds = find_total_bounds(self);
	clamp_scroll(self, data);

	const auto bounds = self.get_absolute_bounds();
	draw_children_offset(self, brush, bounds, data.top_left_offset);
	brush.set_clip(bounds);

	brush.set_color(colors::GREY);
	if (data.total_bounds.w > bounds.w) {
		const auto [length, location] = tab_length_and_location<&Rectangle::x, &Rectangle::w>(data.total_bounds, bounds, data.top_left_offset.x);
		data.hscroll_bounds = {location, bounds.y + bounds.h - TAB_THICKNESS, length, TAB_THICKNESS};
		brush.fill_rectangle(data.hscroll_bounds);
	}
	if (data.total_bounds.h > bounds.h) {
		const auto [length, location] = tab_length_and_location<&Rectangle::y, &Rectangle::h>(data.total_bounds, bounds, data.top_left_offset.y);
		data.vscroll_bounds = {bounds.x + bounds.w - TAB_THICKNESS, location, TAB_THICKNESS, length};
		brush.fill_rectangle(data.vscroll_bounds);
	}
}

template<Rectangle::ValueType Rectangle::*Start, Rectangle::ValueType Rectangle::*Span>
[[nodiscard]] static float scroll_percent(const Rectangle physical_bounds, const Rectangle tab_bounds, Point::ValueType location) noexcept {
	// the mouse is some percent within the range [physical.y, physical.far_y]
	const Range span_range{physical_bounds.*Start, physical_bounds.*Start + physical_bounds.*Span - tab_bounds.*Span};
	return span_range.to_percent(span_range.clamp(location));
}

template<Rectangle::ValueType Rectangle::*Span>
[[nodiscard]] static Point::ValueType scroll_offset(const Rectangle physical_bounds, const Rectangle virtual_bounds, const float percent) noexcept {
	const Range delta_range{0, virtual_bounds.*Span - physical_bounds.*Span};
	return -delta_range.from_percent(percent);
}

void Scroll::Data::vscroll_to(const Component &self, float percent) noexcept {
	top_left_offset.y = scroll_offset<&Rectangle::h>(self.get_absolute_bounds(), total_bounds, std::clamp(percent, 0.0f, 1.0f));
}

void Scroll::Data::hscroll_to(const Component &self, float percent) noexcept {
	top_left_offset.x = scroll_offset<&Rectangle::w>(self.get_absolute_bounds(), total_bounds, std::clamp(percent, 0.0f, 1.0f));
}

EventStatus Scroll::event_handler(Component &self, Data &data, const Event &event) {
	return std::visit(match_event {
		[&](const MouseWheelEvent &e) {
			// validity of the top left is checked in the repaint handler
			data.top_left_offset += (e.delta * SCROLL_SPEED).into<decltype(data.top_left_offset)::ValueType>();
			return EventStatus::CONSUMED;
		},
		[&](const MouseClickEvent &e) {
			if (e.type == MouseClickEvent::Type::PRESS && data.interaction == Data::Interaction::NONE) {
				if (data.vscroll_bounds.contains(e.absolute_location)) {
					data.interaction = Data::Interaction::GRAB_VSCROLL;
					return EventStatus::CONSUMED;
				}
				if (data.hscroll_bounds.contains(e.absolute_location)) {
					data.interaction = Data::Interaction::GRAB_HSCROLL;
					return EventStatus::CONSUMED;
				}
			} else if (e.type == MouseClickEvent::Type::RELEASE) {
				data.interaction = Data::Interaction::NONE;
				return EventStatus::CONSUMED;
			}
			return EventStatus::PROPAGATE;
		},
		[&](const MouseDragEvent &e) {
			switch (data.interaction) {
			case Data::Interaction::GRAB_VSCROLL: {
				const auto bounds = self.get_absolute_bounds();
				data.top_left_offset.y = scroll_offset<&Rectangle::h>(
					bounds,
					data.total_bounds,
					scroll_percent<&Rectangle::y, &Rectangle::h>(
						bounds,
						data.vscroll_bounds,
						data.vscroll_bounds.y + e.delta.h
					)
				);
				return EventStatus::CONSUMED;
			}
			case Data::Interaction::GRAB_HSCROLL: {
				const auto bounds = self.get_absolute_bounds();
				data.top_left_offset.x = scroll_offset<&Rectangle::w>(
					bounds,
					data.total_bounds,
					scroll_percent<&Rectangle::x, &Rectangle::w>(
						bounds,
						data.hscroll_bounds,
						data.hscroll_bounds.x + e.delta.w
					)
				);
				return EventStatus::CONSUMED;
			}
			default:
				return EventStatus::PROPAGATE;
			}
		},
		propagate_other_events
	}, event);
}
}
