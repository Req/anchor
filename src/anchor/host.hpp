
#pragma once

#include <chrono>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/event.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>

namespace anchor {
/**
 * A `Host` serves as a platform-agnostic container for a window.
 * The driving `Window`-like type for a given platform should
 * include a `Host` and interact with the UI through it.
 *
 * Input events should be driven through this class. While they can
 * be manually constructed and propagated to components, the `Host`
 * will be unaware of this and not update tracking references and flags.
 */
class Host {
private:
	Brush _brush;
	Component _root;
	Component *_focus;
	Component *_last_motion = nullptr;
	Point _last_mouse_location;
	bool _dirty = true;
	std::chrono::milliseconds _last_redraw = std::chrono::milliseconds{0};

public:
	explicit Host();

	/**
	 * Use it responsibly. Don't try adding it to another component.
	 */
	[[nodiscard]] auto root() noexcept -> Component&;
	[[nodiscard]] auto root() const noexcept -> const Component&;

	void redraw();

	[[nodiscard]] auto brush() noexcept -> Brush&;
	[[nodiscard]] auto brush() const noexcept -> const Brush&;

	/**
	 * The driver is responsible for calling this any time the native
	 * window is resized. Failing to do so will result in the UI
	 * not being resized.
	 */
	void set_size(Dimension size) noexcept;
	[[nodiscard]] Dimension get_size() const noexcept;

	/**
	 * Requests that the given `Component` receive focus.
	 * Some input events are only propagated to the focus.
	 * @return True if focus was granted.
	 */
	bool request_focus(Component &component) noexcept;
	[[nodiscard]] auto current_focus() noexcept -> Component&;
	[[nodiscard]] auto current_focus() const noexcept -> const Component&;
	void clear_focus() noexcept;

	void mark_dirty() noexcept;
	[[nodiscard]] bool is_dirty() noexcept;

	void fire_key_event(int key, KeyEvent::Type type, KeyEvent::Modifier modifiers) noexcept;
	void fire_mouse_motion_event(Point absolute_endpoint) noexcept;
	void fire_mouse_drag_event(Point absolute_endpoint, MouseDragEvent::Button button) noexcept;
	void fire_mouse_click_event(MouseClickEvent::Button button, MouseClickEvent::Type type, MouseClickEvent::Modifier modifiers, Point absolute_location) noexcept;
	void fire_mouse_wheel_event(BasicDimension<double> delta, Point absolute_location, MouseWheelEvent::Modifier modifiers) noexcept;
};
}
