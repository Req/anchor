
#pragma once

#include <concepts>
#include <cstdint>

#include <anchor/event.hpp>

namespace anchor {
using ComponentId = std::uint64_t;

class Brush;
class Component;

template<typename Data>
using EventHandler = EventStatus (*)(Component &listener, Data &data, const Event &event);
template<typename Data>
using RepaintHandler = void (*)(Component &component, Data &data, Brush &brush) noexcept;

/**
 * Requires:
 * - Some member type `Data`. There are no further restrictions on it, but it should be reasonable to construct.
 * - A `static EventHandler<Data>` named `event_handler`.
 * - A `static RepaintHandler<Data>` named `repaint_handler`.
 */
template<typename T>
concept ComponentSpecialization = requires (T) {
	typename T::Data;

	{T::repaint_handler} -> std::convertible_to<RepaintHandler<typename T::Data>>;
	{T::event_handler} -> std::convertible_to<EventHandler<typename T::Data>>;
};
}
