
#include <algorithm>
#include <array>
#include <cmath>
#include <cstddef>
#include <cstdint>
#include <format>
#include <iostream>
#include <span>
#include <stdexcept>
#include <type_traits>
#include <utility>

#include <anchor/brush.hpp>
#include <anchor/platform/gl.hpp>
#include <anchor/unit/color.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>
#include <anchor/util/debug_gl.hpp>
#include <anchor/util/tags.hpp>

constexpr static const char *FRAGMENT_SRC = R"(
#version 430

layout(location = 0) out vec4 color;

in vec2 fragment_uv;
in vec4 fragment_color;

uniform sampler2D sampler;

void main() {
	color = fragment_color * texture(sampler, fragment_uv);
}
)";

constexpr static const char *VERTEX_SRC = R"(
#version 430

layout(location = 0) in vec2 position;
layout(location = 1) in vec2 uv;
layout(location = 2) in vec4 color;

out vec2 fragment_uv;
out vec4 fragment_color;

void main() {
	fragment_uv = uv;
	fragment_color = color;
	gl_Position = vec4(position, 0, 1);
}
)";

namespace gl = anchor::gl;

[[nodiscard]] static gl::Texture create_white_pixel() {
	const auto id = gl::gen_texture();
	gl::bind_texture(id);
	gl::tex_parameter(gl::TexParameter::MIN_FILTER, gl::TexValue::NEAREST);
	gl::tex_parameter(gl::TexParameter::MAG_FILTER, gl::TexValue::NEAREST);
	constexpr std::array<std::uint8_t, 4> pixel{255, 255, 255, 255};
	gl::tex_image(0, gl::PixelFormat::RGBA, 1, 1, 0, gl::PixelFormat::RGBA, gl::Type::UNSIGNED_BYTE, pixel.data());
	return id;
}

[[nodiscard]] static gl::Shader compile(gl::ShaderType type, const char *src) {
	const auto id = gl::create_shader(type);
	gl::shader_source(id, src);
	gl::compile_shader(id);
	if (const auto error = gl::shader_info_log(id); error.has_value()) {
		throw std::runtime_error{std::format("Failed to compile shader: {}", *error)};
	}
	return id;
}

[[nodiscard]] static gl::Program compile(const char *fragment_src, const char *vertex_src) {
	const auto fragment = compile(gl::ShaderType::FRAGMENT, fragment_src);
	const auto vertex = compile(gl::ShaderType::VERTEX, vertex_src);
	const auto id = gl::create_program();
	gl::attach_shader(id, fragment);
	gl::attach_shader(id, vertex);
	gl::link_program(id);
	gl::delete_shader(fragment);
	gl::delete_shader(vertex);
	if (const auto error = gl::program_info_log(id); error.has_value()) {
		throw std::runtime_error{std::format("Failed to link shader: {}", *error)};
	}
	return id;
}

namespace anchor {
namespace detail {
DrawInfo::DrawInfo(const gl::Texture texture, const Rectangle clip, const std::uint16_t vertex_count) noexcept :
	texture{texture},
	clip{clip},
	vertex_count{vertex_count}
	{}

decltype(Vertex::position) WindowPosition::as_cartesian() const noexcept {
	return {
		x * 2 - 1,
		-(y * 2 - 1)
	};
}
}

ClipLock::ClipLock(Brush &brush, detail::ClipLockId id) noexcept : _brush{brush}, _id{id} {}

ClipLock::~ClipLock() noexcept {
	release();
}

void ClipLock::release() noexcept {
	if (_id.has_value()) {
		_brush.release_clip_lock(*_id);
		_id.reset();
	}
}

Brush::Brush() { // NOLINT(cppcoreguidelines-pro-type-member-init)
	check_gl_error;
	_draw_list.reserve(64);
	_vertices.reserve(256);
	_indices.reserve(512);

#ifndef NDEBUG
	gl::enable_debug_logging([](gl::Severity severity, const char *message) noexcept {
		switch (severity)
		{
		case gl::Severity::HIGH: [[fallthrough]]; // NOLINT(bugprone-branch-clone)
		case gl::Severity::MEDIUM: [[fallthrough]];
		case gl::Severity::LOW:
			std::cout << "Debug message: " << message << "\n";
			break;
		case gl::Severity::NOTIFICATION:
			break;
		}
	});
#endif

	_state.shader = compile(FRAGMENT_SRC, VERTEX_SRC);
	_state.sampler = gl::get_uniform_location(_state.shader, "sampler");
	_state.white_pixel = create_white_pixel();

	_state.vao = gl::gen_vertex_array();
	gl::bind_vertex_array(_state.vao);

	_state.index_buffer = gl::gen_buffer();
	_state.vertex_buffer = gl::gen_buffer();
	gl::bind_buffer(gl::Target::ARRAY_BUFFER,        _state.vertex_buffer);
	gl::bind_buffer(gl::Target::ELEMENT_ARRAY_BUFFER, _state.index_buffer);

	const auto position = gl::get_attrib_location(_state.shader, "position");
	const auto       uv = gl::get_attrib_location(_state.shader, "uv");
	const auto    color = gl::get_attrib_location(_state.shader, "color");
	gl::enable_vertex_attrib_array(position);
	gl::enable_vertex_attrib_array(uv);
	gl::enable_vertex_attrib_array(color);
	gl::vertex_attrib_pointer(position, 2, gl::Type::FLOAT,         false, sizeof(detail::Vertex), reinterpret_cast<void*>(offsetof(detail::Vertex, position))); // NOLINT(performance-no-int-to-ptr)
	gl::vertex_attrib_pointer(      uv, 2, gl::Type::FLOAT,         false, sizeof(detail::Vertex), reinterpret_cast<void*>(offsetof(detail::Vertex, uv))); // NOLINT(performance-no-int-to-ptr)
	gl::vertex_attrib_pointer(   color, 4, gl::Type::UNSIGNED_BYTE,  true, sizeof(detail::Vertex), reinterpret_cast<void*>(offsetof(detail::Vertex, color))); // NOLINT(performance-no-int-to-ptr)
	check_gl_error;
}

Brush::~Brush() {
	check_gl_error;
	gl::delete_buffer(_state.index_buffer);
	gl::delete_buffer(_state.vertex_buffer);
	gl::delete_texture(_state.white_pixel);
	gl::delete_vertex_array(_state.vao);
	gl::delete_program(_state.shader);
}

void Brush::draw() noexcept {
	check_gl_error;
	if (_draw_list.empty()) {
		return;
	}

	gl::use_program(_state.shader);
	gl::bind_vertex_array(_state.vao);

	gl::buffer_data(gl::Target::ARRAY_BUFFER,        _vertices.size() * sizeof(decltype(_vertices)::value_type), _vertices.data(), gl::Usage::STREAM_DRAW);
	gl::buffer_data(gl::Target::ELEMENT_ARRAY_BUFFER, _indices.size() * sizeof(decltype(_indices )::value_type),  _indices.data(), gl::Usage::STREAM_DRAW);

	gl::active_texture(gl::TextureUnit::TEXTURE0);
	gl::uniform(_state.sampler, 0);

	const auto enabling_scissor = !gl::is_enabled(gl::Feature::SCISSOR_TEST);
	if (enabling_scissor) {
		gl::enable(gl::Feature::SCISSOR_TEST);
	}

	std::uint16_t index_offset = 0;
	for (const auto &info : _draw_list) {
		gl::bind_texture(info.texture);
		gl::scissor(
			info.clip.x,
			// the scissor box has (0,0) in the bottom (not top) left
			_bounds.h - info.clip.y - info.clip.h,
			info.clip.w,
			info.clip.h
		);
		gl::draw_elements(gl::DrawMode::TRIANGLES, info.vertex_count, gl::Type::UNSIGNED_SHORT, index_offset * sizeof(decltype(_indices)::value_type));
		index_offset += info.vertex_count;
	}

	if (enabling_scissor) {
		gl::disable(gl::Feature::SCISSOR_TEST);
	}

	gl::bind_vertex_array(gl::NONE);

	check_gl_error;
	_draw_list.clear();
	_vertices.clear();
	_indices.clear();
	_clip_locks.clear();
}

auto Brush::new_draw_command(const gl::Texture texture, const Rectangle clip, const std::uint16_t vertex_count, const std::uint16_t index_count) noexcept -> std::pair<std::span<detail::Vertex>, std::span<std::uint16_t>> {
	_indices.resize(_indices.size() + index_count, static_cast<std::uint16_t>(_vertices.size()));
	_vertices.resize(_vertices.size() + vertex_count);

	std::span<detail::Vertex> vertex_span{
		_vertices.data() + _vertices.size() - vertex_count,
		vertex_count
	};
	std::span<std::uint16_t> index_span{
		_indices.data() + _indices.size() - index_count,
		index_count
	};

	if (!_draw_list.empty()) {
		auto &last = _draw_list.back();
		if (last.clip == clip && last.texture == texture) {
			// there's no functional difference between this and the last one, so just merge them to reduce draw calls
			last.vertex_count += index_count;
			return {vertex_span, index_span};
		}
	}
	// it's empty, or the last one differs
	_draw_list.emplace_back(texture, clip, index_count);
	return {vertex_span, index_span};
}

void Brush::set_bounds(const Rectangle bounds) noexcept {
	_bounds = bounds;
}

void Brush::set_clip(Rectangle clip) noexcept {
	if (!_clip_locks.empty()) {
		clip.restrict_by(_clip_locks.back().clip);
	}
	_clip = clip;
}

static auto next_lock_id() noexcept {
	static detail::ClipLockId id = 0;
	// unsigned will wrap upon overflow
	static_assert(std::is_unsigned_v<decltype(id)>);
	return id++;
}

ClipLock Brush::lock_clip(Rectangle clip) noexcept {
	if (!_clip_locks.empty()) {
		clip.restrict_by(_clip_locks.back().clip);
	}
	const auto id = next_lock_id();
	_clip_locks.emplace_back(clip, id);
	set_clip(clip);
	return ClipLock{*this, id};
}

void Brush::release_clip_lock(detail::ClipLockId id) noexcept {
	const auto it = std::find_if(_clip_locks.begin(), _clip_locks.end(), [=](const auto &lock) noexcept {
		return lock.id == id;
	});
	if (it != _clip_locks.end()) {
		_clip_locks.erase(it);
	}
}

void Brush::set_color(const Color color) noexcept {
	_color = color;
}

Rectangle Brush::get_bounds() const noexcept {
	return _bounds;
}

Rectangle Brush::get_clip() const noexcept {
	return _clip;
}

Color Brush::get_color() const noexcept {
	return _color;
}

void Brush::draw_line(const Point a, const Point b, const std::uint32_t thickness_) noexcept {
	auto [vertices, indices] = new_draw_command(_state.white_pixel, get_clip(), 4, 6);

	BasicPoint<float> a1{UninitializedTag{}};
	BasicPoint<float> a2{UninitializedTag{}};
	BasicPoint<float> b1{UninitializedTag{}};
	BasicPoint<float> b2{UninitializedTag{}};

	const auto thickness = static_cast<float>(thickness_);
	const auto fa = a.into<float>();
	const auto fb = b.into<float>();
	if (a.x == b.x) {
		// vertical line - normal is shaped like _
		const auto delta = thickness / 2.f;
		a1 = {std::round(fa.x - delta), fa.y};
		a2 = {std::round(fa.x + delta), fa.y};
		b1 = {a1.x, fb.y};
		b2 = {a2.x, fb.y};
	} else if (a.y == b.y) {
		// horizontal line - normal is shaped like |
		const auto delta = thickness / 2.f;
		a1 = {fa.x, std::round(fa.y - delta)};
		a2 = {fa.x, std::round(fa.y + delta)};
		b1 = {fb.x, a1.y};
		b2 = {fb.x, a2.y};
	} else {
		/*
		 * Diagonal line.
		 * https://math.stackexchange.com/questions/566029/in-a-right-triangle-given-slope-and-length-of-hypotenuse-find-length-of-legs
		 * The inverse slope is used here because we need the slope of the normal, and resulting lengths are halved.
		 * See https://i.imgur.com/AoHv5lc.png for an illustration. Red areas are the triangles resulting from halving the outputs.
		 */
		const auto slope = (fb.x - fa.x) / (fb.y - fa.y);
		const auto horizontal = thickness / std::sqrt(static_cast<float>(std::pow(slope, 2) + 1)) / 2.f;
		const auto vertical = horizontal * slope / 2.f;
		if (slope < 0) {
			// normal is shaped like /
			a1 = {fa.x - horizontal, fa.y + vertical};
			a2 = {fa.x + horizontal, fa.y - vertical};
			b1 = {fb.x - horizontal, fb.y + vertical};
			b2 = {fb.x + horizontal, fb.y - vertical};
		} else {
			// normal is shaped like \.
			a1 = {fa.x - horizontal, fa.y - vertical};
			a2 = {fa.x + horizontal, fa.y + vertical};
			b1 = {fb.x - horizontal, fb.y - vertical};
			b2 = {fb.x + horizontal, fb.y + vertical};
		}
	}

	const auto bounds = _bounds.into<float>();
	vertices[0] = {detail::WindowPosition{a1.x / bounds.w, a1.y / bounds.h}.as_cartesian(), WHITE_PIXEL_UV, _color};
	vertices[1] = {detail::WindowPosition{a2.x / bounds.w, a2.y / bounds.h}.as_cartesian(), WHITE_PIXEL_UV, _color};
	vertices[2] = {detail::WindowPosition{b1.x / bounds.w, b1.y / bounds.h}.as_cartesian(), WHITE_PIXEL_UV, _color};
	vertices[3] = {detail::WindowPosition{b2.x / bounds.w, b2.y / bounds.h}.as_cartesian(), WHITE_PIXEL_UV, _color};

	constexpr std::array<std::uint16_t, 6> buffer{0, 1, 2, 3, 1, 2};
	for (int k = 0; k < 6; ++k) {
		indices[k] += buffer[k];
	}
}

void Brush::draw_border(const Rectangle bounds, const std::uint32_t thickness) noexcept {
	if (thickness == 0) {
		return;
	}

	const auto &top_left = bounds.location();
	const auto top_right = top_left + Dimension{bounds.w, 0};
	const auto bottom_left = top_left + Dimension{0, bounds.h};
	const auto bottom_right = top_left + bounds.size();
	if (thickness == 1) {
		draw_line(top_left, top_right, 1);
		draw_line(top_left, bottom_left, 1);
		draw_line(bottom_left, bottom_right, 1);
		draw_line(top_right, bottom_right, 1);
	} else {
		fill_rectangle({top_left, top_right + Dimension{0, static_cast<typename Dimension::ValueType>(thickness)}});
		fill_rectangle({top_left, bottom_left + Dimension{static_cast<typename Dimension::ValueType>(thickness), 0}});
		fill_rectangle({bottom_left, bottom_right - Dimension{0, static_cast<typename Dimension::ValueType>(thickness)}});
		fill_rectangle({top_right, bottom_right - Dimension{static_cast<typename Dimension::ValueType>(thickness), 0}});
	}
}

void Brush::primitive_draw_rect(const Rectangle area_, const Color color, const gl::Texture texture, const BasicRectangle<float> uv) noexcept {
	auto [vertices, indices] = new_draw_command(texture, get_clip(), 4, 6);

	const auto area = area_.into<float>();
	const auto bounds = _bounds.into<float>();
	// top left, top right, bottom left, bottom right
	vertices[0] = {
		detail::WindowPosition{area.x / bounds.w, area.y / bounds.h}.as_cartesian(),
		{uv.x, uv.y + uv.h},
		color
	};
	vertices[1] = {
		detail::WindowPosition{(area.x + area.w) / bounds.w, area.y / bounds.h}.as_cartesian(),
		{uv.x + uv.w, uv.y + uv.h},
		color
	};
	vertices[2] = {
		detail::WindowPosition{area.x / bounds.w, (area.y + area.h) / bounds.h}.as_cartesian(),
		{uv.x, uv.y},
		color
	};
	vertices[3] = {
		detail::WindowPosition{(area.x + area.w) / bounds.w, (area.y + area.h) / bounds.h}.as_cartesian(),
		{uv.x + uv.w, uv.y},
		color
	};

	constexpr std::array<std::uint16_t, 6> buffer{0, 1, 2, 3, 1, 2};
	for (int k = 0; k < 6; ++k) {
		indices[k] += buffer[k];
	}
}

void Brush::fill_rectangle(const Rectangle area) noexcept {
	primitive_draw_rect(area, _color, _state.white_pixel, WHITE_PIXEL_UV_RECT);
}

void Brush::draw_image(const gl::Texture texture, const Rectangle where, const Color color, const BasicRectangle<float> uv) noexcept {
	primitive_draw_rect(where, color, texture, uv);
}
}
