namespace anchor::baked_fonts {
[[nodiscard]] std::shared_ptr<anchor::Font> Arial() {
	constexpr std::string_view DATA = "iVBORw0KGgoAAAANSUhEUgAAAzoAAAASCAYAAACTpqtyAAAqXUlEQVR4nO3cB3QV1cK38QlJaKEXKQKiVGkiTcWGSK8KIlgoAoIIYsNCFSmCSBMEERApIqKAgBQpCiK9V+kd6SEQCCH1fM/fk/3d7bznJEEQuXflWet3M3vPnDnTEgiJN8Dj8bRzHKcHhmM+lmAGvsd0fIH+MKVHJpyGSotsOAl36RCPKPhKr7uCaLgLwh04De3DV6mQHWGIhbv78CO+RjeYtN9wZEYYouEuCNrO3/vXRQO0h10jdIeu2RHHcZ6E3c/Ijb4YjGkwpUF22NeyEAbgEeg4lqIrTsFXn0L7bwpVFV1QFmobPsZy569VcbzHXQYRmI8PcR7+0j5noxu+gd0wFIW7NegHU2b0QANkxR4MwEKoOzEeidUJB6HKQsddEXFYiZ44ALuHoXltH4Ef0QcXoO5CJWyBrnt5bMIh+CsANZEWPyOxohCLHhiL00hu2RCPi7ieMiIzIhGK6+lBfAddnwqIhTudRy3YabtTWICh0HsnVnbo3MLgbhCawS4aOpeNGIk9MOl+tnISbxWew7vohMSqhn3wVQgyQPckCna90BaP4zDcFcIyjMVG6GMXfAd3ubAB30DPga63Xh8DuzsxG9mh924Gf9uaRqMedM/uxyX46ieUwEx48Ax6YDLc5cE6fIZBcKfPmRxQ56H9udPrmsEuHvq83YuJjuPMhb/0HjkRh1D4qh9awJ1eo2dxIz7GQfgqFXQeurba3l0ftHKSbiYioGN5CH/AXSEsg56T0ngAFXAevsqELdBrolEX9+Ei7AbjWSzCy7BLj11YiJ7QvjyoilC4G4l6aIyX0BD+nhHVHP0xHn2wHrmRWK+iLcpB/YJu0PFEwe4zNIDah2owhSADzsBUDB2QnIbiDBajLO7HIZgyoh7uRSCOYg7OQOl14XgGSaXX10YlxGM19Pqk+g0H0crxFoAaeBBaXgPtR/fUXQakw3mY9bpmz6IIjuFbFIPOtQuuohF8tQC6P09D6X3DkR72vLvljvfvRl/hAvbhBdwFLX8DvV73oD7Uj9iK6gjD72iCIjiF73EWpkDUQQXo+q7DInigquMiDuE55MUuzEAU7ELwLIpA20/D/dD1To9zuIrncQC6htFQ5VEdGXEUc3Eadk9iJfS+taB9rYBdbVyBjj8nfoC7eghHJuijvY/80DOyHjoO02NIjZMoiR3YA9PdqIBd0DkUgN47FnbVEI/deAT+WoMTsMuGOOiaZYWOJTMCoHv0n/hG522oD1EKaiKegPoEjqUPNkPL0gmnoGVbQXyDEcgNx5IDq6Ei8CY0b3sH2xAAjd1a4yzUZbwHzdsqQn0OjeUh6HXZ8CmmQvNuhaEyQWO3nlBatrWD0sfX4K4zmkO9Dr3G0HgntCx5cBqHPd5r1B2h2I8M0DY2vV7Ng8ZPIR4b8Rq0fhvi0ADaRmogFmvRHt0Qij0IgbZxS4tdUO2gOUP3LBynsMllMLSNZISO5xL64hWsh46vNrRNAWzyIwbXoG20bUlcwQF0xts4gXPIB20jdaDXHoK26YKj2Ids0DYtPd5eg85P6aPWuWXGG9gH1QtJ1QeBiE7wDSpD+/OnI47BdNjj8XSA1vlTDhPwB+zCMANVoe2SMh7noBpDc26zoL7GZEyB3uN3qKUIgLa13YMvEQrTaQxCRmgbGQ/1PSYn+BarEQc9Cw2hbWUklI5L2/ryPrRtf6gfoXlf7oS2NbJhIA7CFIf1aAFtI0OhikBjt2JQg9EIqjW0zi0v1FjMggqG1hk6zv2IhvanOX/b2qbD1ByacysE0yRMhLqIO+G45IPqD42Nuzzeex4Gk5Yn4G5oG2M8lH3f9YzNxkWo16BtbbquU6CvL6azGAV9bdU2hubUGHxsGYrFiMc55IW2NwrhW1yB6TSGIwu0jbSAjtv4FWoVNDbawxxLAei1bsWg9Ly0huoIrfOlDZSehQ5QdaB1tmO4hosIguaMJ6FegcbNoGZCY1tzqA+hcW6E4gLc1100p3UHEALNfYrJflyGqoU1OI5OqI+deBPah60mtM1+aBvNGbMRi7uhsRTHyGS6y+N9TT6cwlhoLBVwFlexHtsRh3BonbbZjhXQcmJyYiPUDuyB+gGB0Db+HMB8aDkzlkPtxDbE42dkgJMgFaZC69TH0HwhHEEsNkHnfALrsBlbsRKOH8uxCE6CtdBY+9LXcs35shdLoP3Pw0Gcw3GoxXgVUdgHfdQx1oRe8yP24gL0WqXl4tD+78QW6Hx3YjeUziUbtI32sxwnoPXLPN6vs9o+F5wEhXAEkfgFh7Aba7EQ2n4OQqGuISsc9IbaB21/GXp+qsFJ0BHK3C+d1xJo2XYAer8uUOWgeaMwVFfMwRkEQOtkANTH0Fj0XJzFGOTBeRxECLQ+LXbiDHKjLVRdaL2RDVHQe9RCuOUSzkHrVV3oNZIJq6BGoxlUWmisViAztP3/+UanANRnKA/VC9rOWICx0LJMxI9wEmTEPNjFYjACoG2+wA7cgQaIR0VonWgf56F1GrtVhV4zCY9iClQ9aL1xL9Qn0DgYu/EWNM4GXdC60FgKYDgeQATKYwy0rdYbPaG07PYpnkMm3O2iOW3jywIMgZZlNKKg12ksOq54vAeNRTd0HJTW6fpr/gB2IQ00lvQ4hN3QWLbhIPSgaCy1odpDY7cRiIBqB80ZhaDc8279EYsHoLGE4DRWQWN/XoZq6fGO5UtEIx80ltJQg6BxAA57vH/xzwHNyZ24hFHQuBTeg+7/fdByWWidoW30bFxBLGajBvQenZLwALSPXOiOo1Cb0RrpoPXG61BzURXVMA/qHWgbWxCGIx5b0AVVoOvxEF7BcqipSAvHD92TcAzGISyF5t1mQQVDY9t4qCehsaHrdQXa/0i0hp65mVBbkQna1uxD10xjWzmchfaVH5rT/lRBOEnoD1UGGielJE4iHrPxGlp6PJ4PPN4/dNRn0LZDoYpAY7diULq+jaB0HbTOLS/UWMyCCobWyZ3Yj0jUhebE17Zu0xGNA/gRmnPrjhNQkzARStdhARyXfFD9obE8govQe32N9miL8dBxX0YNaFvRvMoFjW15cQb63LWf4QaIRAS+wMvQ+0xFDM6jErStjIIqAI3dOkENgMZyJ85Bx6t73RFvYibisR3poG3dmkC19HjHtlFQBaCxWzGowcgAvf8aaJ0vK3AGwSgC1R9aZ5SAGgH1CDRv9IG6BxrLZKjW0Fi0nytYjkA4CZ6H+gEa2+YgFpWhcWK6QU2GxjrvTdCyDMYiaNmXZR7vX8K0LIUQhwgMg+ZuRHnoGFNB4w04hlzQWPTc6XwnQ+PtWAEtJ+Y7RKE6NJbOUK2hsT8HMB9anoJrqAmNpQqu4gs4CepDzUZLj/fPDs2vRCjug8ZBGAO1GVuxElp3PTZhNbTsy14sgfavBiAQWvcl1BHcAweFcAXzYF4zEEHQ+jpQ46Dxr4hAFTgJ6iMaM6Cx2c8wBEBzDyAS06Gx/IoLuBca6z0nQS3ETqjhKIjKcJADsRgCjSUnjkHPksbSHyoDNDbXRsu2A9D75UIMhkLzxoeIxZ1oDXU/tE42IQ72ez8EVQsaN4LSuWg8EvEw6zMhAtOgsdEBqjg0dnsCUdDrAqA56QLt/02UQDOotNC4E3TM70Lb//mNzqtQ70PjS9CG6aGNG0Pzxlm0g5ZlJz7weJdlEmKgi/YJemAcVFtom2PoCS3LUWg7LcsHHu+/DmjZlxFQ2aBxDig9fBobwYhGKzjQeen400Fj6Y89CITG5TAf56CLqY8LcS9yWT6Gyg2NJQMKYz/yQPtLLt2kq6gOjUXv/SO0bFuHrdByGpxHBF7FEcxDNug47OtqjIMKhs77Q7SA1hk6HzUAGttqIhadoOznQZ6BKg+N/TmJ76FlWxXUhuNHEehaTYfGxmKcgJZtujbmfe6F0jlrbJuCUARAY38aYzmUzqEP8sG5AYGojwWIQygGIT+CcB47oO20vWh5Iy5C22hOdPzTcRkvQHP+1MMFzILGvrSCegz9EI+icFy0DxUMjW21odpCY9E1C8ch3OXxztlehxoFjcdD6dnU2K0R1GBoPBKqIJwk9IcqA40To8/VA4hAVWjOFow5UI9gKFQRaL1bMSgdtzmH1tA6t7xQYzELKhhalw/7oeOqBs0Z7m19mY5o9EcUMkPzth0YAjUJE6E+g3Ift45JaZ8a696F4g+UheZs90LrwlEAmhsPpddq7PY51H3QuBB0DXQttKw52wMIw2lkheZGQZn3dMsONRcaiz4/1YPQ2PYOVEdo7NYEqqXHO7aNgvJ3LPbzovE4KF/nqrl46Fg1lqNYDsfyFnTPc0Ef+0PzxnLsh2PJiIO4DL1PevyOs8gLbWMzz2BTaCzPQbnfz5eG0LlsQFpobg02QctSHZFIB43dlnm8f2fRsozAYY/3L0XhyATN3wyp8ApqQWPbQSyBlrdjBbKgI/qiKYKh9aLnNBbjoLERgCGoA40lBK3RH/qoa3EA85EDsfgc2tb2BaKQHhp3gioFjaUEVFdobKRBKDZjK1ZC79vS4/F8hM7IDW1bDVWhZamBKtiE1XAsldAEd2MvlkD7v4QgaBupB9UFGhvan66vXnMB9mvkCJZ5/nNeA+G4jEE87oT2cwbB0DpD1zMGWVAIqh+0zsiEq1iIndDnjXs/BaHGw15XHLoGWq6MWVDNUQnm2mi97QD0flqeg5MIhMZyEGb9HYjDe9A4B3Te30HzOjfN67zCkRoaywRom+6IxyBo3piMq8gIjWUN1kLLbiUQhtVIC80Zk7APWpZmUPZ2uh5febzLf/5e8QWoMKgxWAX9vt94bER1NEEb5EQuNEEzFEcmVEUQmuJrTIB+dzAUHaH9N4TKiHCYIqA5lQ1voSf8pd/FUzWhykIdgF0MxmENAvEudGyRMI1HUTwNtRla3on9WIl62I3TFu1LnYLGorl4PAfNX0+PQf0GlR85sAXutqE00kC/j/gdimE0TBdQBP3grhTOIQZx+ACTYdcEag3ssuMrDMEK+Er3Q783eRlvQsegZyU1TAUd7+/vL0UqPAK9Z0ksd7y/C+6vUYjF67BbjzvxCEx1kB4boLJBHYe7c9D6fPBXIGbgIbyKAuiFE7ArnAS9j53uw4+og6LYg3fQHnoOVmMEtJ1Jy8sd7++liuk11Ec1TIXSPemGTtD+MqI6NkPvWR8vwFdtcBwrMRkB0HFdT5Wg1sHUBRnxCo7Cnc5Xx1ccAUiq2bgAnc8/WWsUgq7nL3AXg46IwqO4FemZXYZcqI2l+LtNR2o0hF1JlMI0uHsfhzAU+eAvPdPZ0BZb4W43dH31XHRHckoDpeuteiI9muEg3K3DG8gFfUxO5aD+gKkwYrAB7kZjF7Lhn+5LqBfgrgX0uTMepqWohGCYamEVzmAlNDbp+j6AxbC7jBeRDhMwDMXRHCfhrgNCMQJZkR1a3oLeTuLdiyk4i6dxDb5agXg8jqTKjJcwPkEw2uJmpeMYAx1TFcdx2mEwluEuBMKUBzvwHupiGjYhJ1QpaPu1sPPgbSyAKoxd+AK18BHWIwPU/dB+imG4i+ZSowRqojxUFcd7zZWZWwu7KGyE6Q5sxXA8A33cjZL4GP1gGow+cKf3XIXn8QfsDiIWpmtQh2EXCZ2vOoBY2F2Ezrks1Aq4W+54P4fug1qDGNitRxBKowzURtiFYxtMe+DezxHH+3efNjiPGWiN8zDnpnF1KF3LF5GcvkIeVIV6GPdA8+os1qEmVDVEYyBS4XGoelgArTN1xhHHe183oDvsvkI6NIIqggcx0fm/5Yb2H4aGuAa79AhHYl1FCP4sCBegwqDeg6k91AyUQSBUDyidvOY6oRLqIw0uwE4X5E5EQh3CPVDByAfNqfewFYvhr3F4C59DN64n1uNLuOsIVQv5MRt2h7EdbTAD6h3kxkPQulcxEtrGpHN9CvbcVug85HrT8S13/nNT80CdhrszSIU7cBw6vuT2LB7EIPiqO7S+NoZhLuzG4Rx6ojh8VRZqB0KRFlnxO2riBO6GCsRWlIbpW7RyvF883VVP0AunYfcRimAR5iEQdTEFw6H+gCoBdyWhcuA4fBUPPXfNoetTGaOxBnb7kVh90Qt2mdESHVAcm7EEp9EA7oJRDcdxAUr76IMeWIcADMXrOArVHSOg61XFcZxfMQFvYyrsiuIR9Ec89mE1Wjne/VyDu3cRB5UWpfAU+mAHTJo7gyXwlQcVoI/JSce3BxURAFMLnIev5uAPmBrgfrhbgHNQTRGF8fDXCWTDVQyF0vlfhLusuJHyYxEKYwhW4EbaDl3HJpgMUzPsxRa4i0BrLIOuSy34qjF0vRfCX4twANr2FSRWZTTBEcd7bEF4GhuxCf76BsOg134AU2nkgNIzFIL70R1RGA3TVjyNT9Db+esfvhEohVvRWvyOF9AHJh2/nv0V2AfTUrRGOaxDejwGcx0WYSBy4QwqIS0Ww90a9EVvx7uPAdDrfaV9dcI09IP2mQHNEQN/ZcYPSIPaOAF/RWG54/1z5ickVlvoGCbgAqajMz5FHG5GL+IzZEYodmIVisOuMEbgTcSjKnT8w6B96B6pK0isCciM8tiO1PgKz0NlhMqOUrCLxc/Qx/dxP1RPhOMHpIO6BneRMBXBcLyHaOh8luItJKcGmI5ZeAGxsLsKX8XBXzoOdx6otFCX4c5cc3MPwuHOHE86pIEyc3aXYNLXCF81RCvH+/W2PhojBv3QB21xBt1QFDq+vfCVvgaY5uMsXsQSNEcY5sCkZb1HCGpgNTbjJKpCy/dhIOx0DPtwD84hBnbLHcc54njfexJa4Bq+hV0I5iEzKuMc3AUjDoml9WngjR/rVICqDo0T0x8boGV5HcehZeMQTiIf9COuDtC8rQ30Yyz9mEs/gvsDWZEbEXgU2i4xXWCagBBo3p9PEYXU0Ng2AteQBhoHoyAcFEIqaNnWE0rLN8MudIaW5TGol6CxrQdUYWhsO4J5cHx4AhHYgvTQnC0z1mI9ojEfeaF10ga6TqWhcRmodtDYOIBTeBgaB6AVYrEcDhpCXcGXuMvj/XW/gVDDoe3clnm8P7bOBI1t+bEQkViOVYjGdGSDk0DnF4HHPd6xNEMcVCVoLjGZ8QYOQG3By0gPrW+ShJLQdlIWY6FrEY1pqAyt8ycNZiMejaE56Ygz0HqNu0K9CY11L4ZCr4tFCDRfFcq+TjIAqgg0lpehWnq8Y2MW/BWGd5AK2lbvq36CxskxHioXNPZlEVRajERSPQG9rj8SqzK0neg+bYCWk2MolK5BqA8XoQajEVRr6LVueaHGYhbUYY/311x3IxoVoW1tZttgaOzLdOj1Wu6NKGSGk2A/eiMV1CRMhAqAgxFQbaFxPqj+yAg1D1qXmK+h8mM81BqsSLASOh6l46wOva4o1BfQODE/Iw5pMAr+iscqVIZeZ+h8NkJdw1L0wMNIBW3jTxOolh7v2DYKqgA0disGNRgay1tQFaGxPO7x1hwaG7kQj7ehcV2ostBYH1ULaKxzikEmaOx2L+KhnoTmEjMTsdBrzNclf/RczYVqB83Z1mATtGy8ht3Qstsyj/fXhgJxBDPhJNDXfdUEGt+ogoiGnp3CcCwnsRwOtuM0gqGxMQF6fUaUh+oMrbPVxoMoBNUHmjey4Brm4zGoV6B1iXkdSvvVWPReqhk0tm1OsBVhCILmjb34DZuwGpoTnf8KmPl6iMJkBELbyF4sgfa/EpozqkE9BY2Nn7ELW7ESmrNtwSroPVVTaN7WHupxj3c/P0HztregSsJc41ZwXDZiIfQMLoeThBA0xDaoEtB8f6gM0HgPlnm8y7Zz0PtpWYYgHJlxAZ9B88a9UHVxFN2hed2LHdBzE4VM0LyheTUHSmPN23ojDrlxEN/Csehez0MMqkFzvizFL9CyNINKC41lmcd777XspOJ7nVCoMCRVBWyEqSI2wO41ZMduNEV5FIDdl2iDsjgFfeem9++G3xIEoorj/RecEJiCMBqfYA/UHbgKlQq+KoeDiIa735EGJaH03egRx5teE49/svwogUUwxULpu1d3ugZKx5ncnsECHEQNXIW7S3gQlVDF8ZoBVQjD0QM7kFhFoO1XQelfTSY63p8WPI7CMP2OtjiKU3gfi9EeaWFXHFUc777CYReAH1EB5VDF8f5o9hHoGZoGU0tcxjLswB6MwVioq0gqXStdj6Koh7P4AifRDN8nYRdSYRW2oCGGoqDj/dXH1fBXMOZB76trNxOmBpiFKNyBnvgBw6A86IMA6BgioA5B5YUpEC1xxPHe0zoJrsCDV+Cr7MiYIAfux3wMgt5bZYC6hJuZ9hsLnb+pEnL7sQp2DVDUh81QaRCCMLh7Bp9aRqC38590HLo2bg/gRkqPJ/As4vEtMuFGmo7UaAhVAYUxDYn1Pg5iCPLDLitUGJLKbJMFpgCY4rAHH6E0lkCZ7S8iqfQeqWBfqxaohQYYCD1Lc1ALq2F3GZXRDmvwKPpiJU6gI/5O+txS9vna6ZiV2U5NQTRehKkldB1mwO4MduJhKJ3baWyD0kdtUwPqMaxFONylwTe4AK3/ClmRWB2gc9uB4Uis3o73X7XHYCyS008ojrvgr9rIjDEwrcdveAk3o3IIxngcgKkU8iAQprWIgd066PUloWsVhgawC4Gu/2u4H0rnYXcRu6A24jJeQADstJ9jyAp/rcAlvIQAmMqjLEw631jY6XWpkVhFMAPabhLicCtajSg8B3eau4pNUI8gO+yewR/YjQ24iKawK+j89Rr5qjZ0n6tBRWAOhkPlhq+uID/sSiEH7L5CRgxGVmhstxv70QEF8DPUUpRCGyxDOExFMQQL0BCLobHm7SY63memJ+7BJNiNRF3ovfV+vgpCSexEYmm9tgvGn18wL0CFIakqYCNMFbEBdvNRGlORBbowRxzvf0eSGaZpaIKOOApd1HbQRdDJ/IIZ0Mnr4t8N9Sl0IbqiBPQ+ddELOXEJH8BdPpyBr05DaZvkdhXncDOqhSPOX3/8aPadFe6yQYUiOb2H77AGj8LsO7FWQ9f2IRTAUOi+5MLABG9ANYLG2aE80PVxtxaqoPOf5+5naHu7X5AWBZ2/1gxqEtyVwX34FLthWo8xqIECUFpfEu9jHXSeJXDE8XYK9bAJTaHz07I+uovHfNTEvZiCu5CcAhCMFsiPXjiJpHoT1dDK8f6qgt39WA/VDOkwEnYXEYt1MGWCioSpNvKgoOM9R+MbBOBBlIW7y7iSIBRb0QJHHO+vrui14VDmfW9G+np2L/SF2gPTOZzxIxp2R6HXu12D0vYx8HXcD+IlS0e0cv75qmIndqAr7sFY3Ei7of09C9UMW7AXiXUVOvcMGI8AmCKgQpBUer26AlNDPJbgcdRHd+yD6TJURiSVeQ9zXOpXLMKP6IrWeAoa63PVXTTG4QlkQy0MQ2p8hm643iKh0sFX5vqZ7dQ56BibIhDp0QRTEQl3S/EwlI55MTxQ+qjx4whCZSyCr4agLF7HW9DXsbFIrLOIx2l44K+G6Inf0BnJbT8Ooyb8NQ9ZsQR2j6EObka/w4OXcQ+yoB7mIhaZYLoMdxFQaRCNAXgSn6IISmEaMmAYQqDM6+wuQV3FR3gEX6M0dGz98BwWIQz+0r67ogbm4Dm8hYWIgSkKf6ccmIpjGAed263oAoaiIUaiGErgKzyOgbgCpes8FxVQFOPxEHohHpH4ELUwAoVRGbpegUisNYjDaFRDXjyMt3EWa+GrFSiEwSiLxvgBl2C3ExvQBjuwCe7moi702g1QS6EqYDZMQdD9ikE7qJehc5gKrTcdcRxnuePd7iQWw9QJHbAMV/Ciy4OoiHkIxKdIrOFIBW1fUQvh2Ijz8Ncs7EY29MSuBEXQEVr+BqZ9eAVfYAjGoQnGw1+98BM2oBkqojSK4Tj6ICfaYyEGwoOXsRl6/VhkwB64S41o+MrMa5vkNgR34GZUC4tgd8Tx/uWqGNwVha7JFSTVMAzEFNTCJdjpHHo73k9md/rEUiEIxGW0ROsET0FVhsYZoXv0Bh6Gu0xQYdgDZebsgqGuwa42jmEz3GWGOgV3eo3KDpUGURiEtuiLk3gIJxAKbVsOuj45oGV9TKy9eA0fIznFoRKmIBrJrSG24WvYpYKO8SxUeXiwGnYFHe8XoHUwPYEwHIapNWJQDHlcnobqgOQUj2PIBL13JA6hFBLrYdRGWiRVLWTFPPxTebALJZEadl2g8zM240ZqhPXICZN5T/t52QfTcPyMpmiLG2k6qiMLmmAaktNvGIkaeBmmUJzCfUgqbXMFemaup4OIhF6fWAHQNkdxFf6agkl4HL2d/5QG5VEApggswlsojqN4BwG4no5DFYKvikEdh53+fM2FamiEDNCcr5biDlRxvH8R03HbaZwPDRCCxXDXGB3xDabiS8zGM2iNG0nXbwpOQPuLwfX0E2rh32wP3oCes4MIw1h8jDHQfdTXK5UT7vJAnYL6BO/jJezDDpRDI2zEaSjzOrscMA1EF9TGdujYNB6Fjkiqz9EGhTDR8f43wjqu3YjGjaTjaYP2uBuDcKvqgQ/QEnuwC3r+dW36wrQZei43YC8a4zVMgGk43oE+D/ZjBVbhiJN4F1Ef8ViCP7ASsaiDq/BVHyzE29iCyRiDFXD3FQKgj76aC/Ur4qBO4nd4YNar3o73m583oWNVx6Bz13xv5699hSB8DbNv9RDUE5iGKS5tofOviX44iMTS+j6ogbp//v5aMrTBTESiawL9zp7qCY1berzb2vqhA7Q8A3HIAo1tRRCF0tB4FDZAy9IX+1ABSvvVvFEAZ6AOe/7v77rKbuj3P7Xs1hSqBjS+lYJwEU9BY9s8nEI6aCx34Cq+gMZuR6DXOXgbagg09iULojAfGhupsAkX4Ot6ShmodtBYskO/Y/kTNDYCof3pfHTOmluHE0gPjY3VOIYAaCzpoP1OgsZuORGL76GxbSauISM0Po0foGWjIHQdhsHx4VM8By3/23R/R0DLbvocawQtT0UUtGx7F6oUNNa1PYQR0FjuQDRmQ2O3ABzEZWSC5mZBBUNjWxFEYjs0lhFQlaGxL8sRh7wYD5ULjksBHMBV3OXxzo2EKggnCf2hykDjxPSEagON/dmAIxgKVQSOD8WgBqMRVOsEqja0ndSCehezoIKhdcaduICrKAnNzYJyb2ubjmhoWQpD6TrGowA0nwpqEiZCBcCx6HN6P2Kg+kPzI6CqQmNfykFNhcbjoXzdd1++RzxKQGPjGRSHlutCmc/3UVAFoLEtC05C51IWmssPNQ4a+/IlVAg0tjWBaunxjm33Qc1HKmjO0HiZx3t+RaA5Q+uOYQL02o3QvC86pmjMRTxyQvPGHdD8EoQiFTRvFEQYDiETnAQ5oGt1Be7js8VgEbTspv3tQQTKQXP+rMEmaNnWAJcQBI2NZR7vfx+h5VslPe5DUbg/T2Q7wpENGou224CjCIDmjNQoBT3L9n3JCF2z2dDYKIw4zIfGRhC0Dx1bCDRnex2qEDSWYGgcCI1txzAXW7ESmrOtxzpswmpoTrZjBdzzUxCPqtB4L5ZAy/+k1CiZIAiaM+xzy4sySAONfUkHbZMTGh/AQmg5KXd5vM9/AWhs6wuVCRobeVAGIdD4dtMMSs+dxtcjAzpC1YXmxOwzLTSWmohHR2QI4rudBvgAgzENvvoSxbAVA6D0Hdsh9IWpHAbhY+i7UTu9tjFy4CLsPsQs7IAKgL6DNXkQiIOIQV30QjzUCWxAXWguM87D7gB0fL66C0rb3OoeRHr8DHe61ivwHd5FGoyCB58gsfKiP64gClp21xe6F9qnviMfgC+QEb2g69URuubJLRTj0AGfYDQy4ANof80RC9UNizAHXRGG9/AQXoYHppIIwjb46hx0Hp0xBGMRj1fQCB/hMtREx/v8dsRsFMVn0PvrGrgrjDoYiNuhevDXMeh81FY8j0pYD6Vz6YZI7EYwJjreZ1DPg0n3KRhT4Cvdm6/QF9p2FEyai4PS53IePI3U6AHTCLTHaFRxvM+iXQs8ju9wEqauiIBKi0KoCR1vSxyF3Ttw79uut3N9jYSenaHYh9/grhwKIBJ/t0WIgp7NvghCd8RiDvS1w1d/oD103aajIkzaj7k3djPg7gA2Q+e6CseQ3K7iJfwKu0Fo5Xj/9bOK4zhHnL+WB18jBh/h79QHDfENquMcVBfoWoxDXYRjMJJKz85rmIHxeADHsRPPYxS2wi4TnsA+ROB62ga91zP4Dboe56Fr8zz0/jqH/bCLx0TH+y/sGfAG/KVjWgtdh004B7uz2IKqmAnt2xQMPVd6jzrQdTSdx0tYCF3/yojB9dQXxbAajRP46jf46xekw0NIbLtbkT4XdE8TKz3m4E1cRjdUgL6eeWAXjZ1wp9d9jA8xGKORF5/DvQ8Viz24nnTv9Ro9ky/BpOckP/SeBXAz0vNbE1+iNG5V0diFpDqZILEisR1/p6MJfBUG1R2L8AvUqQS3Y0HojBXQM3S9XcEovIoXMB/+aoHdGAWefu//q5fSd+8a+/ML9C97WpbpCbRsFIeaAI37oQO0PBtXkRYaG6URjaLQWNrjIvQdnMYLMBNaHgI1DZVRD8s83n6B2oZ80PZGV6hc0Nj2LU5By7eartGv0LIvTXEOphOoDq3z5QjmoR2SKjMcBOITXIPpLNpD6/3RvxwovZfGRjCGw97fObSG1tvq4ghM4XgbjstTUI2hsS9BGIAImC7jA89f/+UrNSYgDqa1uBda73YPKkDLt4MV+AFadpuEVdByDoTiIJqhM45D9zYW/bADYagEvcbYBc2ngca+5EMctA+NZ8FdLPR+81EN2s72KpSO611om4bQ/YnHIeSCth0Pd1E4im9QEdrOGInklA79ocpAr01KJZyHzn8OOuAZdMZCxCMa72IoVBHotW7FoAajEVRraJ32qXto0vLz0LpZUMHQ2G0S1FiYbf3VAtMRDb3W0PEr3SeNJRWU9j8RKgCOD8Og+kNjqYNIXITma6MWPvB4v07Eojm0rYyHygWNk0PnE4vT6IGaqIfNML0FbSujoApAY190n9Xb0PgxXEMkxkHH3ATdcQhxeAra1k3bqZYe79gtDQbhIux0/3siENrO7W7EIwKZoDl/ekH1g8ZuA6DaQmNjMJSuq8a+jIAaCI3dYrAIWnabguT0CdZgE/Q6t2Ue7/OlZWOZ59b/RCcp2/E9psOk+94eWn89AtAXeiZVHEbjN8yHtkmu16EKQWPjc6gF+BhTEAmdR0ZsxUpoW9t6rMMmrIbmRK9bAfe8NIMag71YAs3/W/ydW3IdwEJo+UYUxGGPN11T5zYWiLU47PF+baoC5wbo+tnPSTOotNBYfoO20/Kf3+gkhz55LqIFNBbdsHegZdt8xONdjEBP9IEaBG1jm40J0LKRAbvwO5biEspA6wIxEJEwnUIbaP17iMTD0NgoBvUiNDYCcRajoPGtthFdoWV/glEaJaHj1dw/IQPK4Wa9TwjKohSCoDlf9HwVhbZNB83diLTQ86Jrlhqa8yU7yqMANP5vEY590LJbFajqcFARGxCLjdDnRWdEQZ8nM1EYugclkBt63a1UH1thp+ObjFzQNrejPBiNC7C7iAkoCm13o1KjTIJgaO6/XUnMQQxMekZ/QiVomxv1IJYiDqZozMJi6L1HISe0/d+hY/0F9nuobagNbXMjglAUFaGPN+Pr8v+SNdgELbu9B33N07KxzHP7faNjy4vSSAON/64MKIs7oPHf4e8bnVRojplYhQV4G+mh9ddL9+NXaPl29z7ehJb/jg88f/1Hoxuh+6A/g0Kg8e3sayxBY2h8I/T1ezO0LM2g9Pc+jWU9ZkHLToD+JxnpR04VsAv68ah6APtxAXZZoR81NkQqKP2IVD/SfBtaNqXD6/gaJ2CXAU0Qgtlwr9d8YcRAPwaLhykPfP34bhkiUA+m6liMMtiBW10l7EcYUkrpZjQTD+MJ7IYKgAcmfW5qLFo3DPqR8KNYh3+jnMgPfY3Yj0j8N6RreTf0tS8cBxGHlJIuAwohAAdxGTe7LLgbcTiAqwhAc7yDqjiHGykj7kZqnEyQ0j/fGqRGebjTfS+K9TAtQ06UQkq3R6exAfWRUkpJNQ33oixUM2guHa5BbYa+1j+LZH+j83fKjiE4iwEIw7+d/kBbAn2DdBhqNjx4Giml9L9QRixAWfTHWFyArypiEB7CC5iJlFJKKaX/htagJNZhPbrDV5qv4ni/ITqJlG90/v1eRR3Uhe7PR0gppaT6HPWRH/q7ezNMQzpcQwBOYi7a48+f1PxThWIbzuB2+CZH/QKdvD6p2uI+1IA+ppTS/0qX8ST0nHdDH2zATugbnkDkQSUUxWo8iK1IKaWUUvpv6XtshDoOf53EngSnkNK/313IgqEJUkopOS3DK1iKLxEP0/Noh9z4GX/2T/5E53YtF6bhBbyBgxiLlFL6Xywj9K9mD6MwskBfGM5hB+ZhPVJKKaWUUkoppZRu995AQyzEVvTFo3gDdaAfaPz/b57/H3nRSHdnATE4AAAAAElFTkSuQmCC";
	return std::make_shared<anchor::Font>(
		anchor::detail::create_font_texture(DATA, 826, 18),
		' ',
		'}',
		std::vector<anchor::Character>{
			anchor::Character{
				{0.0f, 0.0f, 0.0f, 0.0f},
				{0, 0},
				{0, 0},
				5
			},
			anchor::Character{
				{0.0f, 0.7222222f, 0.0036319613f, -0.7222222f},
				{1, 13},
				{3, 13},
				5
			},
			anchor::Character{
				{0.0036319613f, 0.2777778f, 0.007263922f, -0.2777778f},
				{0, 13},
				{6, 5},
				6
			},
			anchor::Character{
				{0.010895884f, 0.8333333f, 0.012106538f, -0.8333333f},
				{0, 14},
				{10, 15},
				10
			},
			anchor::Character{
				{0.023002421f, 0.9444444f, 0.01210654f, -0.9444444f},
				{0, 15},
				{10, 17},
				10
			},
			anchor::Character{
				{0.035108957f, 0.8333333f, 0.01694915f, -0.8333333f},
				{1, 14},
				{14, 15},
				16
			},
			anchor::Character{
				{0.052058112f, 0.8333333f, 0.014527846f, -0.8333333f},
				{0, 14},
				{12, 15},
				12
			},
			anchor::Character{
				{0.06658596f, 0.2777778f, 0.0036319643f, -0.2777778f},
				{0, 13},
				{3, 5},
				3
			},
			anchor::Character{
				{0.070217915f, 1.0f, 0.006053269f, -1.0f},
				{1, 14},
				{5, 18},
				6
			},
			anchor::Character{
				{0.076271184f, 1.0f, 0.006053269f, -1.0f},
				{1, 14},
				{5, 18},
				6
			},
			anchor::Character{
				{0.08232445f, 0.3888889f, 0.0084745735f, -0.3888889f},
				{0, 14},
				{7, 7},
				7
			},
			anchor::Character{
				{0.09079903f, 0.5f, 0.0108958855f, -0.5f},
				{1, 11},
				{9, 9},
				11
			},
			anchor::Character{
				{0.10169491f, 0.2777778f, 0.0036319643f, -0.2777778f},
				{1, 2},
				{3, 5},
				5
			},
			anchor::Character{
				{0.105326876f, 0.16666667f, 0.007263921f, -0.16666667f},
				{0, 6},
				{6, 3},
				6
			},
			anchor::Character{
				{0.1125908f, 0.11111111f, 0.0036319643f, -0.11111111f},
				{1, 2},
				{3, 2},
				5
			},
			anchor::Character{
				{0.11622276f, 0.8333333f, 0.007263921f, -0.8333333f},
				{0, 14},
				{6, 15},
				5
			},
			anchor::Character{
				{0.12348668f, 0.7777778f, 0.012106538f, -0.7777778f},
				{0, 13},
				{10, 14},
				10
			},
			anchor::Character{
				{0.13559322f, 0.7222222f, 0.0072639287f, -0.7222222f},
				{1, 13},
				{6, 13},
				10
			},
			anchor::Character{
				{0.14285715f, 0.7222222f, 0.012106538f, -0.7222222f},
				{0, 13},
				{10, 13},
				10
			},
			anchor::Character{
				{0.15496369f, 0.7777778f, 0.012106538f, -0.7777778f},
				{0, 13},
				{10, 14},
				10
			},
			anchor::Character{
				{0.16707022f, 0.7222222f, 0.012106538f, -0.7222222f},
				{0, 13},
				{10, 13},
				10
			},
			anchor::Character{
				{0.17917676f, 0.7777778f, 0.012106538f, -0.7777778f},
				{0, 13},
				{10, 14},
				10
			},
			anchor::Character{
				{0.19128329f, 0.7777778f, 0.012106538f, -0.7777778f},
				{0, 13},
				{10, 14},
				10
			},
			anchor::Character{
				{0.20338982f, 0.7222222f, 0.012106538f, -0.7222222f},
				{0, 13},
				{10, 13},
				10
			},
			anchor::Character{
				{0.21549636f, 0.7777778f, 0.012106538f, -0.7777778f},
				{0, 13},
				{10, 14},
				10
			},
			anchor::Character{
				{0.2276029f, 0.7777778f, 0.012106538f, -0.7777778f},
				{0, 13},
				{10, 14},
				10
			},
			anchor::Character{
				{0.23970944f, 0.5555556f, 0.0036319643f, -0.5555556f},
				{1, 10},
				{3, 10},
				5
			},
			anchor::Character{
				{0.2433414f, 0.7222222f, 0.0036319643f, -0.7222222f},
				{1, 10},
				{3, 13},
				5
			},
			anchor::Character{
				{0.24697337f, 0.5555556f, 0.012106538f, -0.5555556f},
				{0, 11},
				{10, 10},
				11
			},
			anchor::Character{
				{0.2590799f, 0.3888889f, 0.010895878f, -0.3888889f},
				{1, 10},
				{9, 7},
				11
			},
			anchor::Character{
				{0.26997578f, 0.5555556f, 0.012106538f, -0.5555556f},
				{0, 11},
				{10, 10},
				11
			},
			anchor::Character{
				{0.28208232f, 0.7777778f, 0.012106538f, -0.7777778f},
				{0, 14},
				{10, 14},
				10
			},
			anchor::Character{
				{0.29418886f, 1.0f, 0.021791756f, -1.0f},
				{0, 14},
				{18, 18},
				18
			},
			anchor::Character{
				{0.31598064f, 0.7222222f, 0.016949147f, -0.7222222f},
				{-1, 13},
				{14, 13},
				12
			},
			anchor::Character{
				{0.3329298f, 0.7222222f, 0.013317198f, -0.7222222f},
				{1, 13},
				{11, 13},
				12
			},
			anchor::Character{
				{0.346247f, 0.8333333f, 0.015738487f, -0.8333333f},
				{0, 14},
				{13, 15},
				13
			},
			anchor::Character{
				{0.36198547f, 0.7222222f, 0.014527857f, -0.7222222f},
				{1, 13},
				{12, 13},
				13
			},
			anchor::Character{
				{0.3765133f, 0.7222222f, 0.013317198f, -0.7222222f},
				{1, 13},
				{11, 13},
				12
			},
			anchor::Character{
				{0.3898305f, 0.7222222f, 0.012106538f, -0.7222222f},
				{1, 13},
				{10, 13},
				11
			},
			anchor::Character{
				{0.40193704f, 0.8333333f, 0.015738487f, -0.8333333f},
				{0, 14},
				{13, 15},
				14
			},
			anchor::Character{
				{0.41767555f, 0.7222222f, 0.013317198f, -0.7222222f},
				{1, 13},
				{11, 13},
				13
			},
			anchor::Character{
				{0.43099272f, 0.7222222f, 0.0036319494f, -0.7222222f},
				{1, 13},
				{3, 13},
				5
			},
			anchor::Character{
				{0.4346247f, 0.7777778f, 0.009685218f, -0.7777778f},
				{0, 13},
				{8, 14},
				9
			},
			anchor::Character{
				{0.44430992f, 0.7222222f, 0.013317198f, -0.7222222f},
				{1, 13},
				{11, 13},
				12
			},
			anchor::Character{
				{0.45762712f, 0.7222222f, 0.010895878f, -0.7222222f},
				{1, 13},
				{9, 13},
				10
			},
			anchor::Character{
				{0.468523f, 0.7222222f, 0.015738487f, -0.7222222f},
				{1, 13},
				{13, 13},
				15
			},
			anchor::Character{
				{0.4842615f, 0.7222222f, 0.013317198f, -0.7222222f},
				{1, 13},
				{11, 13},
				13
			},
			anchor::Character{
				{0.49757868f, 0.8333333f, 0.016949177f, -0.8333333f},
				{0, 14},
				{14, 15},
				14
			},
			anchor::Character{
				{0.51452786f, 0.7222222f, 0.013317168f, -0.7222222f},
				{1, 13},
				{11, 13},
				12
			},
			anchor::Character{
				{0.527845f, 0.8888889f, 0.016949177f, -0.8888889f},
				{0, 14},
				{14, 16},
				14
			},
			anchor::Character{
				{0.5447942f, 0.7222222f, 0.014527857f, -0.7222222f},
				{1, 13},
				{12, 13},
				13
			},
			anchor::Character{
				{0.55932206f, 0.8333333f, 0.014527857f, -0.8333333f},
				{0, 14},
				{12, 15},
				12
			},
			anchor::Character{
				{0.57384986f, 0.7222222f, 0.013317168f, -0.7222222f},
				{0, 13},
				{11, 13},
				11
			},
			anchor::Character{
				{0.5871671f, 0.7777778f, 0.013317168f, -0.7777778f},
				{1, 13},
				{11, 14},
				13
			},
			anchor::Character{
				{0.60048425f, 0.7222222f, 0.014527857f, -0.7222222f},
				{0, 13},
				{12, 13},
				12
			},
			anchor::Character{
				{0.6150121f, 0.7222222f, 0.020581126f, -0.7222222f},
				{0, 13},
				{17, 13},
				17
			},
			anchor::Character{
				{0.63559324f, 0.7222222f, 0.014527857f, -0.7222222f},
				{0, 13},
				{12, 13},
				12
			},
			anchor::Character{
				{0.6501211f, 0.7222222f, 0.014527857f, -0.7222222f},
				{0, 13},
				{12, 13},
				12
			},
			anchor::Character{
				{0.6646489f, 0.7222222f, 0.013317168f, -0.7222222f},
				{0, 13},
				{11, 13},
				11
			},
			anchor::Character{
				{0.6779661f, 0.9444444f, 0.004842639f, -0.9444444f},
				{1, 13},
				{4, 17},
				5
			},
			anchor::Character{
				{0.6828087f, 0.8333333f, 0.007263899f, -0.8333333f},
				{0, 14},
				{6, 15},
				5
			},
			anchor::Character{
				{0.69007266f, 0.9444444f, 0.004842639f, -0.9444444f},
				{0, 13},
				{4, 17},
				5
			},
			anchor::Character{
				{0.69491524f, 0.44444445f, 0.009685218f, -0.44444445f},
				{0, 14},
				{8, 8},
				8
			},
			anchor::Character{
				{0.7046005f, 0.11111111f, 0.014527857f, -0.11111111f},
				{-1, -2},
				{12, 2},
				10
			},
			anchor::Character{
				{0.7191283f, 0.16666667f, 0.006053269f, -0.16666667f},
				{0, 13},
				{5, 3},
				6
			},
			anchor::Character{
				{0.7251816f, 0.6111111f, 0.012106538f, -0.6111111f},
				{0, 10},
				{10, 11},
				10
			},
			anchor::Character{
				{0.7372881f, 0.7777778f, 0.010895908f, -0.7777778f},
				{1, 13},
				{9, 14},
				10
			},
			anchor::Character{
				{0.748184f, 0.6111111f, 0.010895908f, -0.6111111f},
				{0, 10},
				{9, 11},
				9
			},
			anchor::Character{
				{0.7590799f, 0.7777778f, 0.010895908f, -0.7777778f},
				{0, 13},
				{9, 14},
				10
			},
			anchor::Character{
				{0.7699758f, 0.6111111f, 0.012106538f, -0.6111111f},
				{0, 10},
				{10, 11},
				10
			},
			anchor::Character{
				{0.7820823f, 0.7777778f, 0.007263899f, -0.7777778f},
				{0, 14},
				{6, 14},
				5
			},
			anchor::Character{
				{0.7893462f, 0.7777778f, 0.010895908f, -0.7777778f},
				{0, 10},
				{9, 14},
				10
			},
			anchor::Character{
				{0.8002421f, 0.7222222f, 0.009685218f, -0.7222222f},
				{1, 13},
				{8, 13},
				10
			},
			anchor::Character{
				{0.80992734f, 0.7222222f, 0.0024213195f, -0.7222222f},
				{1, 13},
				{2, 13},
				4
			},
			anchor::Character{
				{0.81234866f, 0.9444444f, 0.004842639f, -0.9444444f},
				{-1, 13},
				{4, 17},
				4
			},
			anchor::Character{
				{0.8171913f, 0.7222222f, 0.009685218f, -0.7222222f},
				{1, 13},
				{8, 13},
				9
			},
			anchor::Character{
				{0.8268765f, 0.7222222f, 0.0024213195f, -0.7222222f},
				{1, 13},
				{2, 13},
				4
			},
			anchor::Character{
				{0.82929784f, 0.5555556f, 0.015738487f, -0.5555556f},
				{1, 10},
				{13, 10},
				15
			},
			anchor::Character{
				{0.8450363f, 0.5555556f, 0.009685218f, -0.5555556f},
				{1, 10},
				{8, 10},
				10
			},
			anchor::Character{
				{0.85472155f, 0.6111111f, 0.012106538f, -0.6111111f},
				{0, 10},
				{10, 11},
				10
			},
			anchor::Character{
				{0.8668281f, 0.7777778f, 0.010895908f, -0.7777778f},
				{1, 10},
				{9, 14},
				10
			},
			anchor::Character{
				{0.877724f, 0.7777778f, 0.010895908f, -0.7777778f},
				{0, 10},
				{9, 14},
				10
			},
			anchor::Character{
				{0.88861984f, 0.5555556f, 0.007263899f, -0.5555556f},
				{1, 10},
				{6, 10},
				6
			},
			anchor::Character{
				{0.8958838f, 0.6111111f, 0.010895908f, -0.6111111f},
				{0, 10},
				{9, 11},
				9
			},
			anchor::Character{
				{0.90677965f, 0.7777778f, 0.006053269f, -0.7777778f},
				{0, 13},
				{5, 14},
				5
			},
			anchor::Character{
				{0.9128329f, 0.6111111f, 0.009685218f, -0.6111111f},
				{1, 10},
				{8, 11},
				10
			},
			anchor::Character{
				{0.92251813f, 0.5555556f, 0.010895908f, -0.5555556f},
				{0, 10},
				{9, 10},
				9
			},
			anchor::Character{
				{0.93341404f, 0.5555556f, 0.015738487f, -0.5555556f},
				{0, 10},
				{13, 10},
				13
			},
			anchor::Character{
				{0.9491525f, 0.5555556f, 0.010895908f, -0.5555556f},
				{0, 10},
				{9, 10},
				9
			},
			anchor::Character{
				{0.96004844f, 0.7777778f, 0.010895908f, -0.7777778f},
				{0, 10},
				{9, 14},
				9
			},
			anchor::Character{
				{0.9709443f, 0.5555556f, 0.010895908f, -0.5555556f},
				{0, 10},
				{9, 10},
				9
			},
			anchor::Character{
				{0.9818402f, 1.0f, 0.007263899f, -1.0f},
				{0, 14},
				{6, 18},
				6
			},
			anchor::Character{
				{0.9891041f, 1.0f, 0.0036319494f, -1.0f},
				{1, 14},
				{3, 18},
				5
			},
			anchor::Character{
				{0.9927361f, 1.0f, 0.007263899f, -1.0f},
				{0, 14},
				{6, 18},
				6
			},
		},
		18,
		15
	);
}
}
