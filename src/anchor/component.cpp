
#include <algorithm>
#include <iostream>
#include <memory>
#include <optional>
#include <ranges>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/event.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
namespace detail {
bool operator==(const GenericIdentity &lhs, const GenericIdentity &rhs) noexcept {
	return
		lhs.callback == rhs.callback &&
		lhs.painter == rhs.painter &&
		lhs.data == rhs.data;
}

ComponentData::ComponentData(const GenericEventHandler callback, const GenericRepaintHandler painter) noexcept :
	_callback{callback},
	_painter{painter}
	{}

EventStatus ComponentData::fire_event(Component &component, const Event &event) {
	return _callback(component, data_ref(), event);
}

void ComponentData::repaint(Component &component, Brush &brush) noexcept {
	_painter(component, data_ref(), brush);
}
}

auto BaseComponent::no_data() noexcept -> Data& {
	static Data data;
	return data;
}

void BaseComponent::repaint_handler(Component &component, Data&, Brush &brush) noexcept {
	component.repaint_children(brush);
}

EventStatus BaseComponent::event_handler(Component&, Data&, const Event&) {
	return EventStatus::PROPAGATE;
}

[[nodiscard]] static ComponentId next_component_id() noexcept {
	static ComponentId id = 0;
	return id++;
}

Component::Component(std::unique_ptr<detail::ComponentData> &&data, const bool enabled, const bool focusable) :
	_id{next_component_id()},
	_data{std::move(data)},
	enabled{enabled},
	focusable{focusable}
	{}

Component* Component::parent() noexcept {
	return _parent;
}

const Component* Component::parent() const noexcept {
	return _parent;
}

void Component::set_bounds(const Rectangle bounds) noexcept {
	_bounds = bounds;
}

void Component::set_location(const Point location) noexcept {
	_bounds = location;
}

void Component::set_size(const Dimension size) noexcept {
	_bounds = size;
}

Point Component::get_absolute_location() const noexcept {
	const auto location = get_location();
	if (_parent == nullptr) {
		return location;
	}
	return _parent->get_absolute_location() + Dimension{location.x, location.y};
}

Rectangle Component::get_absolute_bounds() const noexcept {
	return {get_absolute_location(), get_size()};
}

Rectangle Component::get_bounds() const noexcept {
	return _bounds;
}

Point Component::get_location() const noexcept {
	return _bounds.location();
}

Dimension Component::get_size() const noexcept {
	return _bounds.size();
}

Component& Component::add(Component &&component) noexcept {
	if (component._parent != nullptr) {
		return add(*component._parent->remove(component));
	}
	component._parent = this;
	return *_children.emplace_back(std::make_unique<Component>(std::move(component)));
}

std::optional<Component> Component::remove(Component &component) noexcept {
	const auto it = std::remove_if(_children.begin(), _children.end(), [&component](const auto &c) noexcept {
		return *c == component;
	});
	if (it == _children.end()) {
		return {};
	}
	auto removed = std::optional{std::move(**it)};
	removed->_parent = nullptr;
	_children.erase(it, _children.end());
	return removed;
}

Component* Component::component_at(const Point location) noexcept {
	return const_cast<Component*>(const_cast<const Component*>(this)->component_at(location));
}

const Component* Component::component_at(const Point location) const noexcept {
	if (Rectangle{{0, 0}, get_size()}.contains(location)) {
		// later components get rendered topmost, so the search should begin at the end
		for (const auto &child : std::ranges::reverse_view(_children)) {
			// child components' bounds are relative to this component, so the location is still valid for them
			if (child->get_bounds().contains(location)) {
				// convert the location into coordinates respective to the child coordinate
				const auto offset = child->get_location();
				const auto adjusted_location = location - Dimension{offset.x, offset.y};
				if (const auto *component = child->component_at(adjusted_location); component != nullptr) {
					return component;
				}
			}
		}
		return this;
	}
	return nullptr;
}

auto Component::begin() noexcept -> ChildIterator {
	return _children.begin();
}

auto Component::begin() const noexcept -> ConstChildIterator {
	return _children.cbegin();
}

auto Component::end() noexcept -> ChildIterator {
	return _children.end();
}

auto Component::end() const noexcept -> ConstChildIterator {
	return _children.cend();
}

ComponentId Component::id() const noexcept {
	return _id;
}

EventStatus Component::propagate_event(const Event &event) {
	if (_parent == nullptr) {
		return EventStatus::PROPAGATE;
	}
	return _parent->fire_event(event);
}

EventStatus Component::fire_event(const Event &event) noexcept {
	if (!enabled) {
		return propagate_event(event);
	}
	try {
		if (_data->fire_event(*this, event) == EventStatus::CONSUMED) {
			return EventStatus::CONSUMED;
		}
		return propagate_event(event);
	} catch (const std::exception &e) {
		std::cerr << std::format("Event handler caught exception: {}\n", e.what());
		return EventStatus::CONSUMED;
	}
}

void Component::repaint(Brush &brush) noexcept {
	_data->repaint(*this, brush);
}

void Component::repaint_children(Brush &brush) noexcept {
	if (_layout != nullptr) {
		_layout->redo(*this);
	}
	for (const auto &child : _children) {
		if (_layout != nullptr) {
			const auto bounds = _layout->bounds_of(*child);
			if (bounds.has_value()) {
				child->set_bounds(*bounds);
			}
		}
		child->repaint(brush);
	}
}

bool operator==(const Component &lhs, const Component &rhs) noexcept {
	return lhs.id() == rhs.id();
}
}
