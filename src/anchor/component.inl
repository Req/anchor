
#pragma once

#include <cassert>
#include <exception>
#include <format>
#include <memory>
#include <typeinfo>
#include <type_traits>
#include <utility>

#include <anchor/brush.hpp>
#include <anchor/event.hpp>

namespace anchor {
namespace detail {
using GenericEventHandler = EventStatus (*)(Component&, void*, const Event&);
using GenericRepaintHandler = void (*)(Component&, void*, Brush&) noexcept;

struct GenericIdentity {
	const void *const callback;
	const void *const painter;
	const std::type_info &data;
};

[[nodiscard]] bool operator==(const GenericIdentity &lhs, const GenericIdentity &rhs) noexcept;

class ComponentData {
protected:
	GenericEventHandler _callback;
	GenericRepaintHandler _painter;

	/*
	 * Strict aliasing isn't violated here because the pointer is
	 * never dereferenced as anything but the original type,
	 * although this might technically be illegal pointer arithmetic
	 * depending on how the standard feels at any given moment.
	 */
	[[nodiscard]] virtual auto data_ref() noexcept -> void* = 0;
	[[nodiscard]] virtual auto data_ref() const noexcept -> const void* = 0;

public:
	ComponentData(GenericEventHandler callback, GenericRepaintHandler painter) noexcept;

	virtual ~ComponentData() noexcept = default;

	EventStatus fire_event(Component &component, const Event &event);
	void repaint(Component &component, Brush &brush) noexcept;

	[[nodiscard]] virtual GenericIdentity virtual_identity() const noexcept = 0;

	template<ComponentSpecialization Spec>
	const typename Spec::Data& data() const noexcept;
};

/*
 * Reinterpreting `void (*)(auto, Data&, auto)` as `void (*)(auto, void*, auto)` and then
 * calling the latter would be UB. This is just a wrapper to create legal void handlers.
 */
template<ComponentSpecialization Spec>
struct VoidWrapper final {
	inline static EventStatus callback(Component &component, void *data, const Event &event) {
		return Spec::event_handler(component, *reinterpret_cast<typename Spec::Data*>(data), event);
	}

	inline static void painter(Component &component, void *data, Brush &brush) noexcept {
		Spec::repaint_handler(component, *reinterpret_cast<typename Spec::Data*>(data), brush);
	}
};

template<ComponentSpecialization Spec>
class ComponentDataImpl final : public ComponentData {
private:
	using Data = typename Spec::Data;
	constexpr static EventHandler<Data> callback = Spec::event_handler;
	constexpr static RepaintHandler<Data> painter = Spec::repaint_handler;

	Data _data;

	[[nodiscard]] virtual auto data_ref() noexcept -> void* {
		return &_data;
	}

	[[nodiscard]] virtual auto data_ref() const noexcept -> const void* {
		return &_data;
	}

public:
	template<typename... Args>
	explicit ComponentDataImpl(Args&&... args) :
		ComponentData{
			VoidWrapper<Spec>::callback,
			VoidWrapper<Spec>::painter
		},
		_data{std::forward<Args>(args)...} {}

	[[nodiscard]] static GenericIdentity static_identity() noexcept {
		return {
			reinterpret_cast<void*>(callback),
			reinterpret_cast<void*>(painter),
			typeid(Data)
		};
	}

	[[nodiscard]] GenericIdentity virtual_identity() const noexcept override {
		return static_identity();
	}
};

template<ComponentSpecialization Spec>
const typename Spec::Data& ComponentData::data() const noexcept {
	return *static_cast<const typename Spec::Data*>(data_ref());
}

template<ComponentSpecialization Spec>
[[nodiscard]] constexpr bool enabled_by_default() noexcept {
	if constexpr (std::is_base_of_v<EnabledByDefault<false>, Spec>) {
		return false;
	}
	return true;
}

template<ComponentSpecialization Spec>
[[nodiscard]] constexpr bool focusable_by_default() noexcept {
	if constexpr (std::is_base_of_v<FocusableByDefault<false>, Spec>) {
		return false;
	}
	return true;
}
}

template<ComponentSpecialization Spec, typename... Args>
Component Component::create(Args&&... args) {
	return Component(
		std::make_unique<detail::ComponentDataImpl<Spec>>(std::forward<Args>(args)...),
		detail::enabled_by_default<Spec>(),
		detail::focusable_by_default<Spec>()
	);
}

template<typename T>
T& Component::use_layout() {
	static_assert(!std::is_same_v<T, Layout> && std::is_base_of_v<Layout, T>);
	_layout = std::make_unique<T>(*this);
	return *static_cast<T*>(_layout.get());
}

template<typename T>
T* Component::get_layout() noexcept {
	static_assert(std::is_base_of_v<Layout, T>);
	assert(_layout == nullptr || dynamic_cast<T*>(_layout.get()) != nullptr);
	return dynamic_cast<T*>(_layout.get());
}

template<ComponentSpecialization Spec>
bool Component::is_type() const noexcept {
	const auto &lhs = _data->virtual_identity();
	const auto &rhs = detail::ComponentDataImpl<Spec>::static_identity();
	return lhs == rhs;
}

template<ComponentSpecialization Spec>
typename Spec::Data& Component::data() noexcept {
	assert(is_type<Spec>());
	return const_cast<typename Spec::Data&>(const_cast<const Component*>(this)->data<Spec>());
}

template<ComponentSpecialization Spec>
const typename Spec::Data& Component::data() const noexcept {
	return _data->data<Spec>();
}
}
