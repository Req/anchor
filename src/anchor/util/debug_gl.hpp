
#pragma once

namespace anchor::detail {
void check_gl_error_(const char *file, int line);
}

#ifndef NDEBUG
#define check_gl_error anchor::detail::check_gl_error_(__FILE__, __LINE__)
#else
#define check_gl_error
#endif
