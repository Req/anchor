
#pragma once

#include <functional>
#include <memory>
#include <string>
#include <typeindex>
#include <utility>
#include <vector>

namespace anchor {
void hash_combine(std::size_t&) noexcept;

template<typename T, typename... Rest>
void hash_combine(std::size_t &seed, const T &t, Rest... rest) noexcept;

template<typename A, typename B>
struct PairHash {
	std::size_t operator()(const std::pair<A, B> &value) const noexcept;
};

template<typename T>
using PairHashOf = PairHash<typename T::first_type, typename T::second_type>;
}

#include <anchor/util/hash.inl>
