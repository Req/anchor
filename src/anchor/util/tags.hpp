
#pragma once

namespace anchor {
struct UninitializedTag {};

struct DisableMove {
	DisableMove() noexcept = default;
	DisableMove(DisableMove&&) = delete;
	DisableMove& operator=(DisableMove&&) = delete;
};

struct DisableCopy {
	DisableCopy() noexcept = default;
	DisableCopy(DisableCopy&) = delete;
	DisableCopy& operator=(DisableCopy&) = delete;
};
}
