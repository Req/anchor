
#pragma once

#include <type_traits>

/*
 * Blanket implementations of bitwise operators for enum types.
 * Add the following keys to your enum to enable them:
 * - METADATA_ENABLE_OPERATOR_OR
 * - METADATA_ENABLE_OPERATOR_AND
 * - METADATA_ENABLE_OPERATOR_XOR
 * - METADATA_ENABLE_OPERATOR_LSH
 * - METADATA_ENABLE_OPERATOR_RSH
 * You may need to add using declarations for ADL to find these operators.
 */

#define IMPL_OP(symbol, function, name) \
template<typename T, T enabled = T::METADATA_ENABLE_OPERATOR_##name> \
[[nodiscard]] constexpr T function(const T lhs, const T rhs) noexcept { \
	using U [[maybe_unused]] = std::underlying_type_t<T>; \
    return static_cast<T>(static_cast<U>(lhs) symbol static_cast<U>(rhs)); \
} \
template<typename T, typename L, T enabled = T::METADATA_ENABLE_OPERATOR_##name> \
[[nodiscard]] constexpr T function(const T lhs, const L rhs) noexcept { \
	using U [[maybe_unused]] = std::underlying_type_t<T>; \
    static_assert(std::is_same_v<U, L>); \
    return static_cast<T>(static_cast<U>(lhs) symbol static_cast<U>(rhs)); \
}

#define IMPL_OP_EQ(symbol, function, name) \
template<typename T, T enabled = T::METADATA_ENABLE_OPERATOR_##name> \
constexpr T& function(T &lhs, const T rhs) noexcept { \
	using U [[maybe_unused]] = std::underlying_type_t<T>; \
	lhs = lhs symbol rhs; \
	return lhs; \
} \
template<typename T, typename L, T enabled = T::METADATA_ENABLE_OPERATOR_##name> \
constexpr T& function(T &lhs, const L rhs) noexcept { \
	using U [[maybe_unused]] = std::underlying_type_t<T>; \
	static_assert(std::is_same_v<U, L>); \
	lhs = lhs symbol rhs; \
	return lhs; \
}

namespace anchor {
IMPL_OP(|, operator|, OR)
IMPL_OP(&, operator&, AND)
IMPL_OP(^, operator^, XOR)
IMPL_OP(<<, operator<<, LSH)
IMPL_OP(>>, operator>>, RSH)

IMPL_OP_EQ(|, operator|=, OR)
IMPL_OP_EQ(&, operator&=, AND)
IMPL_OP_EQ(^, operator^=, XOR)
IMPL_OP_EQ(<<, operator<<=, LSH)
IMPL_OP_EQ(>>, operator>>=, RSH)
}

#undef IMPL_OP
#undef IMPL_OP_EQ
