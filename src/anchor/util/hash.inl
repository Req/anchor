
#pragma once

#include <utility>

// https://stackoverflow.com/questions/2590677/how-do-i-combine-hash-values-in-c0x

namespace anchor {
template<typename T, typename... Rest>
void hash_combine(std::size_t &seed, const T &t, Rest... rest) noexcept {
	std::hash<T> hasher;
	seed ^= hasher(t) + 0x9e3779b9 + (seed << 6) + (seed >> 2);
	hash_combine(seed, rest...);
}

template<typename A, typename B>
std::size_t PairHash<A, B>::operator()(const std::pair<A, B> &value) const noexcept {
	std::size_t seed = 0;
	hash_combine(seed, value.first, value.second);
	return seed;
}
}
