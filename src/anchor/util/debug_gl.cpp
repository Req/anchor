
#include <iostream>
#include <format>
#include <stdexcept>

#include <anchor/platform/gl.hpp>
#include <anchor/util/debug_gl.hpp>

static void report_error(const char *name, const char *file, const int line) {
	const auto message = std::format("{} at {}:{}", name, file, line);
	std::cerr << message << std::endl; // NOLINT(performance-avoid-endl)
	throw std::runtime_error{message};
}

using anchor::gl::Error;

static const char* error_name(const Error error) noexcept {
	switch (error) {
#define check(name) case Error::name: return "GL_" #name; // NOLINT(cppcoreguidelines-macro-usage)
	check(INVALID_OPERATION)
	check(INVALID_ENUM)
	check(INVALID_VALUE)
	check(OUT_OF_MEMORY)
	check(INVALID_FRAMEBUFFER_OPERATION)
	check(STACK_UNDERFLOW)
	check(STACK_OVERFLOW)
#undef check
	default:
		return "Unknown error";
	}
}

namespace anchor::detail {
void check_gl_error_(const char *file, const int line) {
	for (auto error = gl::get_error(); error != Error::NO_ERROR; error = gl::get_error()) {
		report_error(error_name(error), file, line);
	}
}
}
