
#include <anchor/event.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>

namespace anchor {
KeyEvent::KeyEvent(const Type type, const Modifier modifiers, const int key) noexcept :
	type{type},
	modifiers{modifiers},
	key{key} {}

MouseClickEvent::MouseClickEvent(const Type type, const Button button, const Modifier modifiers, const Point absolute_location) noexcept :
	type{type},
	button{button},
	modifiers{modifiers},
	absolute_location{absolute_location} {}

MouseMoveEvent::MouseMoveEvent(const Point absolute_endpoint, const Dimension delta, const bool leaving) noexcept :
	absolute_endpoint{absolute_endpoint},
	delta{delta},
	leaving{leaving} {}

MouseDragEvent::MouseDragEvent(const Point absolute_endpoint, const Dimension delta, const Button button) noexcept :
	absolute_endpoint{absolute_endpoint},
	delta{delta},
	button{button} {}

MouseWheelEvent::MouseWheelEvent(const Modifier modifiers, const Point absolute_location, const BasicDimension<double> delta) noexcept :
	modifiers{modifiers},
	absolute_location{absolute_location},
	delta{delta} {}
}
