
#pragma once

#include <optional>

#include <anchor/component_fwd.hpp>
#include <anchor/unit/rectangle.hpp>

namespace anchor {
/**
 * A generic layout. Subclass this if you want your own custom layout manager.
 * This class only provides the interface for calculating bounds. It does not
 * prescribe an interface for declaring a component to be part of a layout,
 * as that functionality is highly specific to an individual layout manager.
 */
class Layout {
protected:
	ComponentId _owner;

public:
	explicit Layout(Component &owner) noexcept;
	virtual ~Layout() noexcept = default;

	/**
	 * Call at most once per layout per redraw to recalculate the bounds of every tracked component.
	 * Does not actually call set_bounds on the tracked components.
	 * @param owner The owner, and the same component passed to the constructor.
	 */
	virtual void redo(const Component &owner) noexcept = 0;
	/**
	 * Retrieves the calculated bounds. Does not actually calculate the bounds itself.
	 * @param component A component being tracked by this layout.
	 * @return None if the component isn't being tracked by this layout.
	 */
	[[nodiscard]] virtual std::optional<Rectangle> bounds_of(const Component &component) noexcept = 0;
};
}
