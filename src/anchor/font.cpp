
#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <format>
#include <memory>
#include <stdexcept>
#include <string>
#include <string_view>

#include <anchor/font.hpp>
#include <anchor/platform/gl.hpp>
#include <anchor/resources/extern/base64.hpp>
#include <anchor/resources/extern/stb_image.hpp>
#include <anchor/resources/fonts/Arial.inl>
#include <anchor/util/debug_gl.hpp>

namespace anchor {
Font::Texture::Texture(const gl::Texture id) noexcept : id{id} {}

Font::Texture::~Texture() noexcept {
	glDeleteTextures(1, &id);
}

Font::Font(gl::Texture texture, char start, char end, std::vector<Character> alphabet, std::uint32_t size, std::int32_t max_y_bearing) :
	_texture{std::make_shared<Texture>(texture)},
	start{start},
	end{end},
	alphabet{std::move(alphabet)},
	size{size},
	max_y_bearing{max_y_bearing} {}

Font::Font(std::shared_ptr<Texture> texture, char start, char end, std::vector<Character> alphabet, std::uint32_t size, std::int32_t max_y_bearing) :
	_texture{std::move(texture)},
	start{start},
	end{end},
	alphabet{std::move(alphabet)},
	size{size},
	max_y_bearing{max_y_bearing} {}

const Character& Font::operator[](const char c) const noexcept {
	if (start <= c && c <= end) {
		return alphabet[c - start];
	}
	return EMPTY_CHARACTER;
}

gl::Texture Font::texture() const noexcept {
	return _texture->id;
}

namespace detail {
gl::Texture create_font_texture(const std::string_view data, const int width, const int height) {
	std::string decoded;
	const auto decode_error = ext::decode_base64(data, decoded);
	if (!decode_error.empty()) {
		throw std::runtime_error{std::format("Failed to decode baked font: {}", decode_error)};
	}

	int actual_width;
	int actual_height;
	const std::unique_ptr<unsigned char[], decltype(&free)> buffer{ // NOLINT(cppcoreguidelines-avoid-c-arrays,modernize-avoid-c-arrays)
		ext::stbi_load_from_memory(
			reinterpret_cast<const unsigned char*>(decoded.data()),
			static_cast<int>(decoded.length()),
			&actual_width,
			&actual_height,
			nullptr,
			4
		),
		&free
	};
	if (buffer == nullptr) {
		throw std::runtime_error{std::format("Failed to decode baked font PNG: {}", ext::stbi_failure_reason())};
	}

	check_gl_error;
	const auto id = gl::gen_texture();
	gl::bind_texture(id);
	gl::tex_image(0, gl::PixelFormat::RGBA, width, height, 0, gl::PixelFormat::RGBA, gl::Type::UNSIGNED_BYTE, buffer.get());
	gl::tex_parameter(gl::TexParameter::TEXTURE_WRAP_S, gl::TexValue::CLAMP_TO_EDGE);
	gl::tex_parameter(gl::TexParameter::TEXTURE_WRAP_T, gl::TexValue::CLAMP_TO_EDGE);
	gl::tex_parameter(gl::TexParameter::MIN_FILTER, gl::TexValue::LINEAR);
	gl::tex_parameter(gl::TexParameter::MAG_FILTER, gl::TexValue::LINEAR);
	return id;
}
}

static std::unique_ptr<FontAtlas> global_atlas_instance; // NOLINT(cppcoreguidelines-avoid-non-const-global-variables)

FontAtlas& FontAtlas::instance() {
	if (global_atlas_instance == nullptr) {
		global_atlas_instance.reset(new FontAtlas); // NOLINT(cppcoreguidelines-owning-memory)
	}
	return *global_atlas_instance;
}

void FontAtlas::destroy_instance() {
	global_atlas_instance.reset();
}

FontAtlas::FontAtlas() noexcept : _default{"Arial"} {
	add("Arial", baked_fonts::Arial());
}

template<typename T>
[[nodiscard]] constexpr static T scale(const T value, const float ratio) noexcept {
	if constexpr (std::is_fundamental_v<T>) {
		return static_cast<T>(std::round(static_cast<float>(value) * ratio));
	} else {
		// this is probably a dimension where the operator already does what we want
		return value * ratio;
	}
}

[[nodiscard]] static FontRef scale_font(const FontRef &font, const std::uint32_t size) noexcept {
	assert(font != nullptr);

	const auto ratio = static_cast<float>(size) / static_cast<float>(font->size);
	std::vector<Character> alphabet{font->alphabet.size()};
	std::transform(font->alphabet.begin(), font->alphabet.end(), alphabet.begin(), [ratio](const Character &c) noexcept {
		return Character{
			c.uv,
			scale(c.bearing, ratio),
			scale(c.size, ratio),
			scale(c.advance, ratio)
		};
	});
	return std::make_shared<const Font>(
		font->_texture,
		font->start,
		font->end,
		alphabet,
		size,
		scale(font->max_y_bearing, ratio)
	);
}

const Font* FontAtlas::get(const std::string_view name, const std::uint32_t size) noexcept {
	const ScaleKey key{name, size};
	if (auto it = _scaled.find(key); it != _scaled.end()) {
		// already cached
		return it->second.get();
	}
	if (auto it = _loaded.find(name); it != _loaded.end()) {
		// loaded but needs to be scaled
		return _scaled.emplace(key, scale_font(it->second, size)).first->second.get();
	}
	// not loaded
	return nullptr;
}

const Font* FontAtlas::get_default(const std::uint32_t size) noexcept {
	return get(_default, size);
}

void FontAtlas::add(const std::string_view name, const FontRef &font) noexcept {
	if (font == nullptr) {
		return;
	}
	_loaded.emplace(name, font);
	_scaled.emplace(ScaleKey{name, font->size}, font);
}
}
