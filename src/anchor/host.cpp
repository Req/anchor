
#include <chrono>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/host.hpp>
#include <anchor/unit/rectangle.hpp>
#include <anchor/util/debug_gl.hpp>

namespace anchor {
Host::Host() : _root(Component::create<BaseComponent>()), _focus{&_root} {}

Component& Host::root() noexcept {
	return _root;
}

const Component &Host::root() const noexcept {
	return _root;
}

[[nodiscard]] inline static std::chrono::milliseconds time_now() noexcept {
	const auto now = std::chrono::system_clock::now().time_since_epoch();
	return std::chrono::duration_cast<std::chrono::milliseconds>(now);
}

void Host::redraw() {
	_last_redraw = time_now();
	_dirty = false;
	_root.repaint(_brush);
	_brush.draw(); check_gl_error;
}

void Host::set_size(const Dimension size) noexcept {
	_root.set_bounds({{0, 0}, size});
	_brush.set_bounds({{0, 0}, size});
}

Dimension Host::get_size() const noexcept {
	return _root.get_size();
}

Brush& Host::brush() noexcept {
	return _brush;
}

const Brush& Host::brush() const noexcept {
	return _brush;
}

[[nodiscard]] static bool is_descendant_of(const Component &child, const Component &target) noexcept {
	const auto *parent = child.parent();
	if (parent == nullptr) {
		return false;
	}
	if (*parent == target) {
		return true;
	}
	return is_descendant_of(child, *parent);
}

bool Host::request_focus(Component &component) noexcept {
	if (!component.focusable || !component.enabled || !is_descendant_of(component, _root)) {
		return false;
	}
	_focus = &component;
	return true;
}

Component& Host::current_focus() noexcept {
	return *_focus;
}

const Component& Host::current_focus() const noexcept {
	return *_focus;
}

void Host::clear_focus() noexcept {
	_focus = &_root;
}

void Host::mark_dirty() noexcept {
	_dirty = true;
}

bool Host::is_dirty() noexcept {
	// small indie clown company microsoft can't fix msvc to not warn for `using std::chrono_literals::operator""ms`
	if (!_dirty && time_now() - _last_redraw > std::chrono::milliseconds{1000}) {
		_dirty = true;
	}
	return _dirty;
}

[[nodiscard]] static Component *find_first_enabled(Component *target) noexcept {
	if (target == nullptr) {
		return nullptr;
	}
	if (target->enabled) {
		return target;
	}
	return find_first_enabled(target->parent());
}

static void fire_event_at(Component &target, const Event &event, bool &dirty) noexcept {
	if (target.fire_event(event) == EventStatus::CONSUMED) {
		dirty = true;
	}
}

void Host::fire_key_event(const int key, const KeyEvent::Type type, const KeyEvent::Modifier modifiers) noexcept {
	if (auto *target = find_first_enabled(_focus); target != nullptr) {
		fire_event_at(*target, KeyEvent{
			type,
			modifiers,
			key
		}, _dirty);
	}
}

void Host::fire_mouse_motion_event(const Point absolute_endpoint) noexcept {
	const auto delta = _last_mouse_location.distance_to(absolute_endpoint);
	_last_mouse_location = absolute_endpoint;

	auto *at_cursor = _root.component_at(absolute_endpoint);
	auto *target = find_first_enabled(at_cursor != nullptr ? at_cursor : &_root);
	if (target != nullptr) {
		fire_event_at(*target, MouseMoveEvent{
			absolute_endpoint,
			delta,
			false
		}, _dirty);
	}
	if (target != _last_motion) {
		if (_last_motion != nullptr) {
			fire_event_at(*_last_motion, MouseMoveEvent{
				absolute_endpoint,
				delta,
				true
			}, _dirty);
		}
		_last_motion = target;
	}
}

void Host::fire_mouse_drag_event(const Point absolute_endpoint, const MouseDragEvent::Button button) noexcept {
	const auto delta = _last_mouse_location.distance_to(absolute_endpoint);
	_last_mouse_location = absolute_endpoint;

	if (auto *target = find_first_enabled(_focus); target != nullptr) {
		fire_event_at(*target, MouseDragEvent{
			absolute_endpoint,
			delta,
			button
		}, _dirty);
	}
}

void Host::fire_mouse_click_event(const MouseClickEvent::Button button, const MouseClickEvent::Type type, const MouseClickEvent::Modifier modifiers, const Point absolute_location) noexcept {
	/*
	 * Press events set the focus to the topmost enabled element under the cursor.
	 * Release events go to the current focus.
	 */
	if (type == MouseClickEvent::Type::PRESS) {
		if (auto *focus = find_first_enabled(_root.component_at(absolute_location)); focus != nullptr) {
			request_focus(*focus);
		}
	}
	if (auto *target = find_first_enabled(_focus); target != nullptr) {
		fire_event_at(*target, MouseClickEvent{
			type,
			button,
			modifiers,
			absolute_location
		}, _dirty);
	}
}

void Host::fire_mouse_wheel_event(const BasicDimension<double> delta, const Point absolute_location, const MouseWheelEvent::Modifier modifiers) noexcept {
	if (auto *target = find_first_enabled(_root.component_at(absolute_location)); target != nullptr) {
		fire_event_at(*target, MouseWheelEvent{
			modifiers,
			absolute_location,
			delta
		}, _dirty);
	}
}
}
