
#include <anchor/component.hpp>
#include <anchor/layout.hpp>

namespace anchor {
Layout::Layout(Component &owner) noexcept : _owner{owner.id()} {}
}
