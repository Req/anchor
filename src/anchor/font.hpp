
#pragma once

#include <memory>
#include <string_view>
#include <unordered_map>
#include <utility>
#include <vector>

#include <anchor/platform/gl.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/rectangle.hpp>
#include <anchor/util/hash.hpp>
#include <anchor/util/tags.hpp>

namespace anchor {
struct Character {
	BasicRectangle<float> uv;
	Dimension bearing;
	Dimension size;
	std::uint32_t advance = 0;
};

constexpr Character EMPTY_CHARACTER{{0, 0, 0, 0}, {0, 0}, {0, 0}, 0};

namespace detail {
// used by baked font initializers
gl::Texture create_font_texture(std::string_view data, int width, int height);
}

/*
 * Copy/move are disabled to prevent shenanigans involving the
 * texture ID. Wrap it in a shared_ptr and give it to `FontAtlas`.
 */
struct Font final : private DisableCopy, private DisableMove {
	class Texture final : private DisableCopy, private DisableMove {
	private:
		gl::Texture id;

	public:
		explicit Texture(gl::Texture id) noexcept;
		~Texture() noexcept;

		friend struct Font;
	};

	std::shared_ptr<Texture> _texture;
	char start;
	char end;
	std::vector<Character> alphabet;
	std::uint32_t size;
	std::int32_t max_y_bearing;

	Font(             gl::Texture texture, char start, char end, std::vector<Character> alphabet, std::uint32_t size, std::int32_t max_y_bearing);
	Font(std::shared_ptr<Texture> texture, char start, char end, std::vector<Character> alphabet, std::uint32_t size, std::int32_t max_y_bearing);

	[[nodiscard]] const Character& operator[](char c) const noexcept;

	[[nodiscard]] gl::Texture texture() const noexcept;
};

using FontRef = std::shared_ptr<const Font>;

class FontAtlas final : private DisableCopy, private DisableMove {
private:
	using ScaleKey = std::pair<std::string_view, std::uint32_t>;

	// std::hash<const char*> hashes the pointer and not the string
	std::unordered_map<std::string_view, FontRef> _loaded;
	std::unordered_map<ScaleKey, FontRef, PairHashOf<ScaleKey>> _scaled;
	std::string_view _default;

	explicit FontAtlas() noexcept;

public:
	/**
	 * Will construct the singleton atlas upon the first call.
	 */
	static FontAtlas &instance();
	static void destroy_instance();

	[[nodiscard]] const Font* get(std::string_view name, std::uint32_t size) noexcept;
	[[nodiscard]] const Font* get_default(std::uint32_t size) noexcept;

	/**
	 * @param name A name used to retrieve the font later. The pointer must be valid for the lifetime of the atlas.
	 */
	void add(std::string_view name, const FontRef &font) noexcept;
};
}
