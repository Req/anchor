
use std::{
    fs::File,
    io::{
        Cursor,
        Write
    },
    path::{
        Path,
        PathBuf
    }
};
use fontdue::{
    Font,
    FontSettings,
    Metrics
};
use font_loader::system_fonts::{
    self,
    FontPropertyBuilder
};
use image::{
    ImageOutputFormat,
    Rgba,
    RgbaImage
};
use levenshtein::levenshtein;
use serde::{
    Deserialize,
    Serialize
};

const fn default_size() -> u32 {
    12
}

const fn default_range() -> (char, char) {
    (' ', '~')
}

#[derive(Deserialize, Serialize)]
struct Input {
    name: String,
    output_name: String,
    #[serde(default = "default_size")]
    size: u32,
    #[serde(default)]
    bold: bool,
    #[serde(default)]
    italic: bool,
    #[serde(default = "default_range")]
    range: (char, char)
}

#[derive(Deserialize, Serialize)]
struct Manifest {
    output_directory: PathBuf,
    inputs: Vec<Input>
}

impl Default for Manifest {
    fn default() -> Self {
        Self {
            output_directory: PathBuf::from("."),
            inputs: vec![
                Input {
                    name: String::from("Arial"),
                    output_name: String::from("Arial"),
                    size: default_size(),
                    bold: false,
                    italic: false,
                    range: default_range()
                }
            ]
        }
    }
}

struct CharMetrics {
    uv: (f32, f32, f32, f32),
    bearing: (i32, i32),
    size: (i32, i32),
    advance: i32
}

struct CharMap {
    texture: Vec<u8>,
    size: u32,
    width: u32,
    height: u32,
    metrics: Vec<(char, CharMetrics)>
}

fn f(f: f32) -> String {
    if f == f.round() {
        format!("{}.0f", f)
    } else {
        format!("{}f", f)
    }
}

impl CharMap {
    fn into_cpp(self, name: &str) -> String {
        let pre = vec![
            format!("namespace anchor::baked_fonts {{"),
            format!("[[nodiscard]] std::shared_ptr<anchor::Font> {}() {{", name),
            format!("\tconstexpr std::string_view DATA = \"{}\";", base64::encode(self.texture)),
            format!("\treturn std::make_shared<anchor::Font>("),
            format!("\t\tanchor::detail::create_font_texture(DATA, {}, {}),", self.width, self.height),
            format!("\t\t'{}',", self.metrics.first().unwrap().0),
            format!("\t\t'{}',", self.metrics.last().unwrap().0),
            format!("\t\tstd::vector<anchor::Character>{{")
        ];
        let metrics: Vec<Vec<String>> = self.metrics.iter().map(|(_, metrics)| {
            vec![
                format!("\t\t\tanchor::Character{{"),
                format!("\t\t\t\t{{{}, {}, {}, {}}},", f(metrics.uv.0), f(metrics.uv.1), f(metrics.uv.2), f(metrics.uv.3)),
                format!("\t\t\t\t{{{}, {}}},", metrics.bearing.0, metrics.bearing.1),
                format!("\t\t\t\t{{{}, {}}},", metrics.size.0, metrics.size.1),
                format!("\t\t\t\t{}", metrics.advance),
                format!("\t\t\t}},")
            ]
        }).collect();
        let post = vec![
            format!("\t\t}},"),
            format!("\t\t{},", self.size),
            format!("\t\t{}", self.metrics.iter().max_by_key(|(_, metrics)| metrics.bearing.1).unwrap().1.bearing.1),
            format!("\t);"),
            format!("}}"),
            format!("}}"),
            format!("")
        ];

        [pre, metrics.concat(), post].concat().join("\n")
    }
}

impl Input {
    fn build_properties(&self, name: &str) -> FontPropertyBuilder {
        let mut prop = FontPropertyBuilder::new().family(name);
        if self.bold {
            prop = prop.bold();
        }
        if self.italic {
            prop = prop.italic();
        }
        prop
    }

    fn create_map(&self, font: Font) -> Result<CharMap, String> {
        let rasters: Vec<(char, Metrics, RgbaImage)> = (self.range.0..self.range.1).map(|c| {
            let (metrics, buffer) = font.rasterize(c, self.size as f32);
            (c, metrics, RgbaImage::from_fn(metrics.width as u32, metrics.height as u32, |x, y| {
                Rgba::from([
                    255,
                    255,
                    255,
                    buffer[y as usize * metrics.width + x as usize]
                ])
            }))
        }).collect();

        let (full_width, max_height) = rasters.iter().fold((0, 0), |(full_width, max_height), (_, metrics, _)| {
            (full_width + metrics.width, max_height.max(metrics.height))
        });
        let mut texture = RgbaImage::from_pixel(full_width as u32, max_height as u32, Rgba::from([255, 255, 255, 0]));
        let mut offset = 0;
        let metrics: Vec<(char, CharMetrics)> = rasters.into_iter().map(|(c, metrics, raster)| {
            image::imageops::overlay(&mut texture, &raster, offset, 0);

            let left = offset as f32 / full_width as f32;
            let right = left + metrics.width as f32 / full_width as f32;
            let top = 0.0;
            let bottom = metrics.height as f32 / max_height as f32;
            let result = (c, CharMetrics {
                uv: (
                    left,
                    bottom,
                    right - left,
                    top - bottom
                ),
                bearing: (
                    /*
                     * https://freetype.org/freetype2/docs/glyphs/metrics.png
                     * `bearing.x` and `xmin` are equivalent.
                     * `bearing.y` is equivalent to `ymax` but no such value is
                     * provided, so it has to be derived from `height` and `ymin`.
                     */
                    metrics.xmin,
                    metrics.height as i32 + metrics.ymin
                ),
                size: (metrics.width as i32, metrics.height as i32),
                advance: metrics.advance_width.round() as i32
            });

            offset += metrics.width as i64;
            result
        }).collect();
        let mut buffer = Cursor::new(Vec::new());
        texture.write_to(&mut buffer, ImageOutputFormat::Png).map_err(|e| format!("Failed to compress texture: {}", e))?;

        Ok(CharMap {
            texture: buffer.into_inner(),
            size: self.size,
            width: texture.width(),
            height: texture.height(),
            metrics
        })
    }

    fn try_load(&self, available: &[&str]) -> Result<CharMap, String> {
        if available.is_empty() {
            return Err(String::from("No fonts available to select from"));
        }

        let selected = available.iter().min_by_key(|name| levenshtein(name, &self.name)).unwrap();
        let distance = levenshtein(selected, &self.name);
        if distance > self.name.len() / 2 {
            return Err(format!("Failed to find a suitable match for \"{}\". Best match was \"{}\", but it was discarded when matching for having a high edit distance ({})", self.name, selected, distance));
        }

        match system_fonts::get(&self.build_properties(selected).build()) {
            Some((buffer, index)) => {
                let settings = FontSettings {
                    collection_index: index as u32,
                    ..Default::default()
                };
                match Font::from_bytes(buffer.as_ref(), settings) {
                    Ok(font) => self.create_map(font),
                    Err(e) => Err(format!("Failed to load \"{}\": {}", selected, e))
                }
            },
            None => Err(format!("Failed to load \"{}\", which was determined to be the best match for \"{}\"", selected, self.readable_descriptor()))
        }
    }

    fn readable_descriptor(&self) -> String {
        enum Pair {
            None,
            One(&'static str),
            Two(&'static str, &'static str)
        }

        impl Pair {
            fn next(self, modifier: bool, name: &'static str) -> Pair {
                if modifier {
                    match self {
                        Self::None => Self::One(name),
                        Self::One(old) => Self::Two(old, name),
                        Self::Two(..) => unreachable!()
                    }
                } else {
                    self
                }
            }

            fn into_string(self) -> String {
                match self {
                    Self::None => String::new(),
                    Self::One(name) => name.to_string(),
                    Self::Two(a, b) => format!("{},{}", a, b)
                }
            }
        }

        Pair::None.next(self.bold, "bold").next(self.italic, "italic").into_string()
    }
}

impl Manifest {
    fn execute(self, available: &[&str], cwd: &Path) -> Result<(), String> {
        for input in &self.inputs {
            match input.try_load(available) {
                Ok(map) => {
                    let output = map.into_cpp(&input.output_name);
                    let dir = cwd.join(&self.output_directory);
                    std::fs::create_dir_all(&dir).unwrap();
                    let path = dir.join(format!("{}.inl", input.output_name));
                    let mut file = File::create(&path).unwrap();
                    file.write_all(&output.into_bytes()).unwrap();
                    println!("Successfully baked {}: {}", input.output_name, path.display());
                },
                Err(e) => return Err(format!("Failed to load font \"{}\": {}", input.name, e))
            }
        }

        Ok(())
    }
}

fn main() {
    let args: Vec<String> = std::env::args().collect();
    if args.len() != 2 {
        eprintln!("Usage:\n(1) create a default manifest: font-baker write-default\n(2) bake from manifest: font-baker path/to/manifest.json");
        return;
    }

    if args[1] == "write-default" {
        let manifest = Manifest::default();
        let file = File::create("default-font-manifest.json").unwrap();
        serde_json::to_writer_pretty(&file, &manifest).unwrap();
        return;
    }

    let path = std::fs::canonicalize(PathBuf::from(&args[1])).unwrap();
    let cwd = path.parent().unwrap();
    let file = File::open(&path).unwrap();
    let manifest: Manifest = serde_json::from_reader(file).unwrap();
    let fonts = system_fonts::query_all();
    let font_refs: Vec<&str> = fonts.iter().map(|f| f.as_ref()).collect();
    if let Err(e) = manifest.execute(&font_refs, cwd) {
        eprintln!("{}", e);
    }
}
