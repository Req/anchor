
#pragma once

#include <format>
#include <stdexcept>

template<typename T>
inline T expect_impl(std::optional<T> &&opt, const char *file, const int line) {
	if (!opt.has_value()) {
		throw std::runtime_error(std::format("{}:{}: optional did not contain a value as expected", file, line));
	}
	return *opt;
}

#define expect(opt) expect_impl(opt, __FILE__, __LINE__)
