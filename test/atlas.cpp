
#include <anchor/font.hpp>

#include "atlas.hpp"

AtlasGuard::~AtlasGuard() noexcept {
	anchor::FontAtlas::destroy_instance();
}
