
#include <variant>

#include <gtest/gtest.h>

#include <anchor/event.hpp>
#include <anchor/host.hpp>
#include <anchor/platform/window.hpp>

using namespace anchor;

struct EventWatcher {
	struct Data {};

	constexpr static auto repaint_handler = paint_nothing<Data>;

	static EventStatus event_handler(Component&, Data&, const Event &e) {
		return std::visit(match_event {
			[](const KeyEvent&) {
				return EventStatus::CONSUMED;
			},
			propagate_other_events
		}, e);
	}
};

TEST(Host, marking_dirty) { // NOLINT(cert-err58-cpp)
	Window w;
	auto &host = w.host();
	// should be dirty initially to cause an initial render
	ASSERT_TRUE(host.is_dirty());
	w.redraw();
	ASSERT_FALSE(host.is_dirty());

	// nothing to send the events to
	host.fire_mouse_motion_event({0, 0});
	host.fire_key_event(0, KeyEvent::Type::PRESS, KeyEvent::Modifier::NONE);
	ASSERT_FALSE(host.is_dirty());

	// doesn't get sent to the component because it isn't the focus
	auto &c = host.root().add(Component::create<EventWatcher>());
	host.fire_mouse_motion_event({0, 0});
	host.fire_key_event(0, KeyEvent::Type::PRESS, KeyEvent::Modifier::NONE);
	ASSERT_FALSE(host.is_dirty());

	host.request_focus(c);
	host.fire_mouse_motion_event({0, 0});
	ASSERT_FALSE(host.is_dirty());
	host.fire_key_event(0, KeyEvent::Type::PRESS, KeyEvent::Modifier::NONE);
	ASSERT_TRUE(host.is_dirty());
}
