
#include <gtest/gtest.h>

#include <anchor/util/enum_operators.hpp>

using namespace anchor;

enum class Enum {
	A = 0b01,
	B = 0b10,
	BOTH = 0b11,
	NONE = 0b00,

	METADATA_ENABLE_OPERATOR_OR [[maybe_unused]],
	METADATA_ENABLE_OPERATOR_AND [[maybe_unused]],
	METADATA_ENABLE_OPERATOR_XOR [[maybe_unused]]
};

// compile-time testing
[[maybe_unused]] constexpr auto or_ = Enum::A | Enum::B;
[[maybe_unused]] constexpr auto and_ = Enum::A & Enum::B;
[[maybe_unused]] constexpr auto xor_ = Enum::A ^ Enum::B;

TEST(EnumOperators, op) { // NOLINT(cert-err58-cpp)
	auto a = Enum::A;
	auto b = Enum::B;

	ASSERT_EQ(a | b, Enum::BOTH);
	ASSERT_EQ(a & b, Enum::NONE);
	ASSERT_EQ(Enum::BOTH ^ Enum::NONE, Enum::BOTH);
	ASSERT_EQ(Enum::A ^ Enum::BOTH, Enum::B);
}

TEST(EnumOperators, op_eq) { // NOLINT(cert-err58-cpp)
	auto a = Enum::A;
	constexpr auto b = Enum::B;

	a |= b;
	ASSERT_EQ(a, Enum::A | Enum::B);

	a &= b;
	ASSERT_EQ(a, Enum::B);

	a ^= Enum::A;
	ASSERT_EQ(a, Enum::BOTH);
}
