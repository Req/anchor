
#include <format>

#include <gtest/gtest.h>

#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>

using namespace anchor;

TEST(Point, distance_to) { // NOLINT(cert-err58-cpp)
	constexpr Point a{1, 1};
	constexpr Point b{-1, -2};
	ASSERT_EQ(a.distance_to(b), Dimension(-2, -3));
}

TEST(Point, operator_eq_neq) { // NOLINT(cert-err58-cpp)
	constexpr Point a{1, 1};
	constexpr Point b{1, 1};
	constexpr Point c{2, 2};
	ASSERT_EQ(a, b);
	ASSERT_NE(b, c);
}

TEST(Point, operator_plus_minus) { // NOLINT(cert-err58-cpp)
	constexpr Point p{1, 1};
	constexpr Dimension d{2, 3};
	ASSERT_EQ(p + d, Point(3, 4));
	ASSERT_EQ(p - d, Point(-1, -2));
}

TEST(Point, format) { // NOLINT(cert-err58-cpp)
	ASSERT_EQ(std::format("{}", Point{3, 4}), "Point{3, 4}");
}
