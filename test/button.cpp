
#include <gtest/gtest.h>

#include <anchor/component.hpp>
#include <anchor/component/button.hpp>
#include <anchor/event.hpp>
#include <anchor/platform/window.hpp>

#include "window_buffer.hpp"

using namespace anchor;

TEST(Button, mouse_interaction) { // NOLINT(cert-err58-cpp)
	Window w;
	auto &container = w.host().root();
	auto &button = container.add(Component::create<Button>());

	// default state
	w.redraw();
	w.redraw();
	auto buffer = WindowBuffer{w.screenshot(), w.get_size()};
	ASSERT_TRUE(buffer.area_is(button.get_bounds(), Button::DefaultStyle::BACKGROUND_COLOR.normal));

	const auto inside = button.get_absolute_location() + Dimension{1, 1};

	// hovered
	ASSERT_EQ(button.fire_event(MouseMoveEvent{
		inside,
		{1, 1},
		false
	}), EventStatus::CONSUMED);
	w.redraw();
	w.redraw();
	buffer = WindowBuffer{w.screenshot(), w.get_size()};
	ASSERT_TRUE(buffer.area_is(button.get_bounds(), Button::DefaultStyle::BACKGROUND_COLOR.hinting));

	// pressed
	ASSERT_EQ(button.fire_event(MouseClickEvent{
		MouseClickEvent::Type::PRESS,
		MouseClickEvent::Button::LEFT,
		MouseClickEvent::Modifier::NONE,
		inside
	}), EventStatus::CONSUMED);
	w.redraw();
	w.redraw();
	buffer = WindowBuffer{w.screenshot(), w.get_size()};
	ASSERT_TRUE(buffer.area_is(button.get_bounds(), Button::DefaultStyle::BACKGROUND_COLOR.active));

	// released, back to hovered
	ASSERT_EQ(button.fire_event(MouseClickEvent{
		MouseClickEvent::Type::RELEASE,
		MouseClickEvent::Button::LEFT,
		MouseClickEvent::Modifier::NONE,
		inside
	}), EventStatus::CONSUMED);
	w.redraw();
	w.redraw();
	buffer = WindowBuffer{w.screenshot(), w.get_size()};
	ASSERT_TRUE(buffer.area_is(button.get_bounds(), Button::DefaultStyle::BACKGROUND_COLOR.hinting));

	// back to default
	ASSERT_EQ(button.fire_event(MouseMoveEvent{
		inside - Dimension{2, 2},
		{-2, -2},
		true
	}), EventStatus::CONSUMED);
	w.redraw();
	w.redraw();
	buffer = WindowBuffer{w.screenshot(), w.get_size()};
	ASSERT_TRUE(buffer.area_is(button.get_bounds(), Button::DefaultStyle::BACKGROUND_COLOR.normal));
}
