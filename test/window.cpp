
#include <stdexcept>

#ifdef __linux__
#include <chrono>
#include <cstdlib>
#include <thread>
#include <unistd.h>
#endif

#include <gtest/gtest.h>

#include <anchor/platform/window.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>

/*
 * https://github.com/glfw/glfw/issues/1375
 * Moves/resizes aren't processed fast enough immediately after the window is created.
 */
static void sleep_x11() noexcept
{
#ifdef __linux__
	using std::chrono_literals::operator""ms;

	if (std::getenv("WAYLAND_DISPLAY") == nullptr) {
		std::this_thread::sleep_for(200ms);
	}
#endif
}

using namespace anchor;

using Dummy = BaseComponent;

TEST(Window, successful_constructor) { // NOLINT(cert-err58-cpp)
	Window w;
	ASSERT_EQ(w.get_location(), Window::DEFAULT_BOUNDS.location());
	ASSERT_EQ(w.get_size(), Window::DEFAULT_SIZE);
}

TEST(Window, successful_multiple_construction) { // NOLINT(cert-err58-cpp)
	Window a;
	{
		Window b;
	}
	Window c;
}

TEST(Window, set_get_size) { // NOLINT(cert-err58-cpp)
	Window w;
	w.set_size({123, 321});
	sleep_x11();
	ASSERT_EQ(w.get_size(), Dimension(123, 321));
}

TEST(Window, set_get_location) { // NOLINT(cert-err58-cpp)
	Window w;
	w.set_location({123, 321});
	sleep_x11();
	ASSERT_EQ(w.get_location(), Point(123, 321));
}

TEST(Window, set_get_bounds) { // NOLINT(cert-err58-cpp)
	Window w;
	w.set_bounds({123, 321, 456, 654});
	sleep_x11();
	ASSERT_EQ(w.get_bounds(), Rectangle(123, 321, 456, 654));
}

TEST(Window, set_get_title) { // NOLINT(cert-err58-cpp)
	constexpr const char *title = "relatively long testing title";
	Window w;
	w.set_title(title);
	ASSERT_EQ(title, w.get_title());
}

TEST(Window, request_focus) { // NOLINT(cert-err58-cpp)
	Window w;
	auto &host = w.host();
	auto &container = host.root();
	ASSERT_EQ(host.current_focus(), container);

	auto &f = container.add(Component::create<Dummy>());
	f.focusable = true;
	ASSERT_TRUE(host.request_focus(f));
	ASSERT_EQ(host.current_focus(), f);

	auto &uf = container.add(Component::create<Dummy>());
	uf.focusable = false;
	ASSERT_FALSE(host.request_focus(uf));
	ASSERT_EQ(host.current_focus(), f);

	auto unowned = Component::create<Dummy>();
	unowned.focusable = true;
	ASSERT_FALSE(host.request_focus(unowned));
	ASSERT_EQ(host.current_focus(), f);
}
