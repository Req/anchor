
#include <gtest/gtest.h>

#include <anchor/brush.hpp>
#include <anchor/component.hpp>
#include <anchor/platform/window.hpp>
#include <anchor/unit/color.hpp>

#include "window_buffer.hpp"

using namespace anchor;

TEST(Brush, window_position_conversion) { // NOLINT(cert-err58-cpp)
	const auto v = detail::WindowPosition{0.4f, 1.3f}.as_cartesian();
	ASSERT_FLOAT_EQ(v.x, -0.2f);
	ASSERT_FLOAT_EQ(v.y, -1.6f);
}

constexpr Rectangle WINDOW_BOUNDS{{0, 0}, Window::DEFAULT_SIZE};
constexpr Color TEST_COLOR = 0xff0000_rgb;

struct Custom {
	struct Data {
		RepaintHandler<Data> repaint;
	};

	constexpr static RepaintHandler<Data> repaint_handler = [](auto &component, auto &data, auto &brush) noexcept {
		data.repaint(component, data, brush);
	};
	constexpr static auto event_handler = propagate_event<Data>;
};

Component custom_component(const RepaintHandler<Custom::Data> repaint) noexcept {
	return Component::create<Custom>(Custom::Data{repaint});
}

TEST(Brush, draw_line) { // NOLINT(cert-err58-cpp)
	Window w;
	auto &container = w.host().root();

	// diagonal lines
	auto cc = custom_component([](auto, auto, auto &brush) noexcept {
		brush.set_bounds(WINDOW_BOUNDS);
		brush.set_clip(WINDOW_BOUNDS);
		brush.set_color(TEST_COLOR);
		brush.draw_line({5, 5}, {100, 150}, 1);
	});

	cc.set_bounds(WINDOW_BOUNDS);
	auto *cc_ref = &container.add(std::move(cc));
	// double redraw to read the correct buffer
	w.redraw();
	w.redraw();

	WindowBuffer wb{w.screenshot(), Window::DEFAULT_SIZE};
	ASSERT_TRUE(wb.area_contains(WINDOW_BOUNDS, TEST_COLOR));
	ASSERT_EQ(wb.at({5, 5}), TEST_COLOR);
	ASSERT_NE(wb.at({4, 4}), TEST_COLOR);
	ASSERT_EQ(wb.at({99, 149}), TEST_COLOR);
	ASSERT_NE(wb.at({100, 150}), TEST_COLOR);
	container.remove(*cc_ref);

	// vertical lines

	cc = custom_component([](auto, auto, auto &brush) noexcept {
		brush.set_bounds(WINDOW_BOUNDS);
		brush.set_clip(WINDOW_BOUNDS);
		brush.set_color(TEST_COLOR);
		brush.draw_line({5, 5}, {5, 100}, 1);
	});
	cc.set_bounds(WINDOW_BOUNDS);
	cc_ref = &container.add(std::move(cc));

	w.redraw();
	w.redraw();

	wb = WindowBuffer{w.screenshot(), Window::DEFAULT_SIZE};
	ASSERT_TRUE(wb.area_contains(WINDOW_BOUNDS, TEST_COLOR));
	ASSERT_EQ(wb.at({5, 5}), TEST_COLOR);
	ASSERT_NE(wb.at({5, 4}), TEST_COLOR);
	ASSERT_EQ(wb.at({5, 99}), TEST_COLOR);
	ASSERT_NE(wb.at({5, 100}), TEST_COLOR);
	container.remove(*cc_ref);

	// horizontal lines

	cc = custom_component([](auto, auto, auto &brush) noexcept {
		brush.set_bounds(WINDOW_BOUNDS);
		brush.set_clip(WINDOW_BOUNDS);
		brush.set_color(TEST_COLOR);
		brush.draw_line({5, 5}, {100, 5}, 1);
	});
	cc.set_bounds(WINDOW_BOUNDS);
	container.add(std::move(cc));

	w.redraw();
	w.redraw();

	wb = WindowBuffer{w.screenshot(), Window::DEFAULT_SIZE};
	ASSERT_TRUE(wb.area_contains(WINDOW_BOUNDS, TEST_COLOR));
	ASSERT_EQ(wb.at({5, 5}), TEST_COLOR);
	ASSERT_NE(wb.at({4, 5}), TEST_COLOR);
	ASSERT_EQ(wb.at({99, 5}), TEST_COLOR);
	ASSERT_NE(wb.at({100, 5}), TEST_COLOR);
}

TEST(Brush, draw_border) { // NOLINT(cert-err58-cpp)
	Window w;
	auto &container = w.host().root();

	auto cc = custom_component([](auto, auto, auto &brush) noexcept {
		brush.set_bounds(WINDOW_BOUNDS);
		brush.set_clip(WINDOW_BOUNDS);
		brush.set_color(TEST_COLOR);
		brush.draw_border({5, 5, 200, 20}, 2);
	});
	cc.set_bounds({{0, 0}, Window::DEFAULT_SIZE});
	container.add(std::move(cc));
	w.redraw();
	w.redraw();

	const WindowBuffer wb{w.screenshot(), Window::DEFAULT_SIZE};
	ASSERT_TRUE(wb.area_is({5, 5, 200, 2}, TEST_COLOR));
	ASSERT_TRUE(wb.area_is({5, 5, 2, 20}, TEST_COLOR));
	ASSERT_TRUE(wb.area_is({5, 23, 200, 2}, TEST_COLOR));
	ASSERT_TRUE(wb.area_is({203, 5, 2, 20}, TEST_COLOR));
	ASSERT_FALSE(wb.area_is({7, 7, 196, 16}, TEST_COLOR));
}

TEST(Brush, fill_rectangle) { // NOLINT(cert-err58-cpp)
	Window w;
	auto &container = w.host().root();

	auto cc = custom_component([](auto, auto, auto &brush) noexcept {
		brush.set_bounds(WINDOW_BOUNDS);
		brush.set_clip(WINDOW_BOUNDS);
		brush.set_color(TEST_COLOR);
		brush.fill_rectangle({5, 5, 200, 20});
	});
	cc.set_bounds({{0, 0}, Window::DEFAULT_SIZE});
	container.add(std::move(cc));
	w.redraw();
	w.redraw();

	const WindowBuffer wb{w.screenshot(), Window::DEFAULT_SIZE};
	ASSERT_TRUE(wb.area_is({5, 5, 200, 20}, TEST_COLOR));
	ASSERT_FALSE(wb.area_is({4, 4, 202, 22}, TEST_COLOR));
}

TEST(Brush, lock_clip) { // NOLINT(cert-err58-cpp)
	Window w;
	auto &brush = w.host().brush();
	auto lock = brush.lock_clip({0, 0, 100, 100});
	{
		auto lock = brush.lock_clip({20, 0, 100, 100});
		{
			auto lock = brush.lock_clip({10, 50, 80, 80});
			ASSERT_EQ(brush.get_clip(), Rectangle(20, 50, 70, 50));
			brush.set_clip({0, 40, 100, 20});
			ASSERT_EQ(brush.get_clip(), Rectangle(20, 50, 70, 10));
		}
	}
	lock.release();
	lock.release();
	lock.release();
	lock.release();
	lock.release();
}
