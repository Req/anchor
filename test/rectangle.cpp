
#include <format>

#include <gtest/gtest.h>

#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>

using namespace anchor;

TEST(Rectangle, constructors) { // NOLINT(cert-err58-cpp)
	{
		constexpr Rectangle r{1, 2, 3, 4};
		ASSERT_EQ(r.x, 1);
		ASSERT_EQ(r.y, 2);
		ASSERT_EQ(r.w, 3);
		ASSERT_EQ(r.h, 4);
	}
	{
		constexpr Rectangle r{Point{1, 2}, Point{7, 5}};
		ASSERT_EQ(r.x, 1);
		ASSERT_EQ(r.y, 2);
		ASSERT_EQ(r.w, 6);
		ASSERT_EQ(r.h, 3);
	}
	{
		constexpr Rectangle r{Point{1, 2}, Dimension{6, 5}};
		ASSERT_EQ(r.x, 1);
		ASSERT_EQ(r.y, 2);
		ASSERT_EQ(r.w, 6);
		ASSERT_EQ(r.h, 5);
	}
}

TEST(Rectangle, include) { // NOLINT(cert-err58-cpp)
	// start at 0,0,0,0 and expand to -2,-3,4,5
	Rectangle r{0, 0, 0, 0};
	r.include({0, 0, 4, 1}); // w
	ASSERT_EQ(r, Rectangle(0, 0, 4, 1));
	r.include({-2, 0, 1, 1}); // x
	ASSERT_EQ(r, Rectangle(-2, 0, 4, 1));
	r.include({0, -3, 1, 1}); // y
	ASSERT_EQ(r, Rectangle(-2, -3, 4, 1));
	r.include({0, 1, 0, 1}); // h
	ASSERT_EQ(r, Rectangle(-2, -3, 4, 5));

	r = {1, 1, 1, 1};
	r.include({1, 1, 1, 1});
	ASSERT_EQ(r, Rectangle(1, 1, 1, 1));
}

TEST(Rectangle, restrict) { // NOLINT(cert-err58-cpp)
	Rectangle r{0, 0, 10, 10};
	r.restrict_by({2, 0, 10, 10}); // left
	ASSERT_EQ(r, Rectangle(2, 0, 8, 10));
	r.restrict_by({0, 2, 10, 10}); // top
	ASSERT_EQ(r, Rectangle(2, 2, 8, 8));
	r.restrict_by({0, 0, 8, 10}); // right
	ASSERT_EQ(r, Rectangle(2, 2, 6, 8));
	r.restrict_by({0, 0, 10, 8}); // bottom
	ASSERT_EQ(r, Rectangle(2, 2, 6, 6));

	r = {0, 0, 10, 10};
	r.restrict_by({0, 0, 10, 10});
	ASSERT_EQ(r, Rectangle(0, 0, 10, 10));

	r.restrict_by({6, -1, 2, 4});
	ASSERT_EQ(r, Rectangle(6, 0, 2, 3));

	r = {390, -110, 100, 100};
	r.restrict_by({390, 10, 100, 300});
	ASSERT_EQ(r, Rectangle(390, 10, 100, 0));
}

TEST(Rectangle, contains_point) { // NOLINT(cert-err58-cpp)
	constexpr Rectangle r{0, 0, 10, 10};
	ASSERT_FALSE(r.contains({10, 10}));
	ASSERT_TRUE(r.contains({0, 0}));
	ASSERT_TRUE(r.contains({5, 5}));
	ASSERT_FALSE(r.contains({-1, -1}));
	ASSERT_FALSE(r.contains({-1, 5}));
}

TEST(Rectangle, contains_rectangle) { // NOLINT(cert-err58-cpp)
	constexpr Rectangle r{0, 0, 10, 10};
	ASSERT_TRUE(r.contains({0, 0, 0, 0}));
	ASSERT_TRUE(r.contains({0, 0, 10, 10}));
	ASSERT_FALSE(r.contains({0, 0, -1, -1}));
	ASSERT_FALSE(r.contains({0, 0, 11, 11}));
	ASSERT_FALSE(r.contains({1, 1, 10, 10}));
	ASSERT_TRUE(r.contains({10, 10, 0, 0}));
}

TEST(Rectangle, operator_eq_neq) { // NOLINT(cert-err58-cpp)
	constexpr Rectangle a{1, 1, 1, 1};
	constexpr Rectangle b{1, 1, 1, 1};
	constexpr Rectangle c{1, 2, 3, 4};
	ASSERT_EQ(a, b);
	ASSERT_NE(b, c);
}

TEST(Rectangle, format) { // NOLINT(cert-err58-cpp)
	ASSERT_EQ(std::format("{}", Rectangle(5, 6, 7, 8)), "Rectangle{Point{5, 6}, Dimension{7, 8}}");
}
