
#include <array>
#include <tuple>

#include <gtest/gtest.h>

#include <anchor/component.hpp>
#include <anchor/component/button.hpp>
#include <anchor/layout/anchor_layout.hpp>
#include <anchor/platform/window.hpp>
#include <anchor/unit/rectangle.hpp>

#include "expect.hpp"

using namespace anchor;

using Container = BaseComponent;
using Dummy = Container;

TEST(AnchorLayout, prevents_cycles) { // NOLINT(cert-err58-cpp)
	auto container = Component::create<Container>();
	auto &layout = container.use_layout<AnchorLayout>();

	auto &a = container.add(Component::create<Dummy>());
	auto &b = container.add(Component::create<Dummy>());
	auto &c = container.add(Component::create<Dummy>());

	ASSERT_TRUE(layout.set_point_acyclic(a, Anchor::CENTER, b, Anchor::CENTER));
	ASSERT_TRUE(layout.set_point_acyclic(b, Anchor::CENTER, c, Anchor::CENTER));
	ASSERT_FALSE(layout.set_point_acyclic(c, Anchor::CENTER, a, Anchor::CENTER));
}

TEST(AnchorLayout, ordering_enforced) { // NOLINT(cert-err58-cpp)
	auto container = Component::create<Container>();
	container.set_bounds({0, 0, 10, 10});
	auto &layout = container.use_layout<AnchorLayout>();

	auto &a = container.add(Component::create<Dummy>());
	auto &b = container.add(Component::create<Dummy>());

	ASSERT_TRUE(layout.set_point(a, Anchor::TOP_LEFT, b, Anchor::TOP_LEFT));
	ASSERT_TRUE(layout.set_point(a, Anchor::BOTTOM_RIGHT, b, Anchor::BOTTOM_RIGHT));

	ASSERT_TRUE(layout.set_point(b, Anchor::TOP_LEFT, container, Anchor::TOP_LEFT, {2, 2}));
	ASSERT_TRUE(layout.set_point(b, Anchor::BOTTOM_RIGHT, container, Anchor::BOTTOM_RIGHT, {-2, -2}));

	layout.redo(container);
	ASSERT_EQ(expect(layout.bounds_of(b)), Rectangle(2, 2, 6, 6));
	ASSERT_EQ(expect(layout.bounds_of(a)), Rectangle(0, 0, 0, 0));
}

TEST(AnchorLayout, accurate_single_anchors) { // NOLINT(cert-err58-cpp)
	auto container = Component::create<Container>();
	auto &layout = container.use_layout<AnchorLayout>();
	container.set_bounds({10, 10, 10, 10});

	using Setup = std::tuple<Component, Anchor, Rectangle>;
	std::array<Setup, 9> setups{
		Setup{Component::create<Dummy>(), Anchor::CENTER, Rectangle{5, 5, 0, 0}},
		Setup{Component::create<Dummy>(), Anchor::TOP, Rectangle{5, 0, 0, 0}},
		Setup{Component::create<Dummy>(), Anchor::BOTTOM, Rectangle{5, 10, 0, 0}},
		Setup{Component::create<Dummy>(), Anchor::LEFT, Rectangle{0, 5, 0, 0}},
		Setup{Component::create<Dummy>(), Anchor::RIGHT, Rectangle{10, 5, 0, 0}},
		Setup{Component::create<Dummy>(), Anchor::TOP_LEFT, Rectangle{0, 0, 0, 0}},
		Setup{Component::create<Dummy>(), Anchor::TOP_RIGHT, Rectangle{10, 0, 0, 0}},
		Setup{Component::create<Dummy>(), Anchor::BOTTOM_LEFT, Rectangle{0, 10, 0, 0}},
		Setup{Component::create<Dummy>(), Anchor::BOTTOM_RIGHT, Rectangle{10, 10, 0, 0}}
	};

	for (auto &[component, anchor, expectation] : setups) {
		auto &c = container.add(std::move(component));
		ASSERT_TRUE(layout.set_point(c, Anchor::CENTER, container, anchor));
		layout.redo(container);
		ASSERT_EQ(expect(layout.bounds_of(c)), expectation);
		ASSERT_TRUE(container.remove(component).has_value());
	}
}

TEST(AnchorLayout, accurate_multi_anchors) { // NOLINT(cert-err58-cpp)
	/*
	Build a 3x3 grid of components. The outer perimeter is placed,
	then the inner component is anchored to all of them.
	  3 4 3
	3 [][][]
	4 []$$[]
	3 [][][]
	*/

	auto container = Component::create<Container>();
	auto &layout = container.use_layout<AnchorLayout>();
	container.set_bounds({10, 10, 10, 10});

	auto &top_left = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(top_left, Anchor::TOP_LEFT, container, Anchor::TOP_LEFT));
	ASSERT_TRUE(layout.set_point(top_left, Anchor::BOTTOM_RIGHT, container, Anchor::TOP_LEFT, {3, 3}));

	auto &top = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(top, Anchor::TOP_LEFT, container, Anchor::TOP, {-2, 0}));
	ASSERT_TRUE(layout.set_point(top, Anchor::BOTTOM_RIGHT, container, Anchor::TOP, {2, 3}));

	auto &top_right = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(top_right, Anchor::TOP_RIGHT, container, Anchor::TOP_RIGHT));
	ASSERT_TRUE(layout.set_point(top_right, Anchor::BOTTOM_LEFT, container, Anchor::TOP_RIGHT, {-3, 3}));

	auto &left = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(left, Anchor::TOP_LEFT, container, Anchor::LEFT, {0, -2}));
	ASSERT_TRUE(layout.set_point(left, Anchor::BOTTOM_RIGHT, container, Anchor::LEFT, {3, 2}));

	auto &right = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(right, Anchor::TOP_RIGHT, container, Anchor::RIGHT, {0, -2}));
	ASSERT_TRUE(layout.set_point(right, Anchor::BOTTOM_LEFT, container, Anchor::RIGHT, {-3, 2}));

	auto &bottom_left = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(bottom_left, Anchor::BOTTOM_LEFT, container, Anchor::BOTTOM_LEFT));
	ASSERT_TRUE(layout.set_point(bottom_left, Anchor::TOP_RIGHT, container, Anchor::BOTTOM_LEFT, {3, -3}));

	auto &bottom = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(bottom, Anchor::BOTTOM_LEFT, container, Anchor::BOTTOM, {-2, 0}));
	ASSERT_TRUE(layout.set_point(bottom, Anchor::TOP_RIGHT, container, Anchor::BOTTOM, {2, -3}));

	auto &bottom_right = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(bottom_right, Anchor::BOTTOM_RIGHT, container, Anchor::BOTTOM_RIGHT));
	ASSERT_TRUE(layout.set_point(bottom_right, Anchor::TOP_LEFT, container, Anchor::BOTTOM_RIGHT, {-3, -3}));

	auto &center = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(center, Anchor::TOP_LEFT, top_left, Anchor::BOTTOM_RIGHT));
	ASSERT_TRUE(layout.set_point(center, Anchor::TOP, top, Anchor::BOTTOM));
	ASSERT_TRUE(layout.set_point(center, Anchor::TOP_RIGHT, top_right, Anchor::BOTTOM_LEFT));
	ASSERT_TRUE(layout.set_point(center, Anchor::BOTTOM_LEFT, bottom_left, Anchor::TOP_RIGHT));
	ASSERT_TRUE(layout.set_point(center, Anchor::BOTTOM, bottom, Anchor::TOP));
	ASSERT_TRUE(layout.set_point(center, Anchor::BOTTOM_RIGHT, bottom_right, Anchor::TOP_LEFT));
	ASSERT_TRUE(layout.set_point(center, Anchor::LEFT, left, Anchor::RIGHT));
	ASSERT_TRUE(layout.set_point(center, Anchor::RIGHT, right, Anchor::LEFT));

	layout.redo(container);
	ASSERT_EQ(expect(layout.bounds_of(top_left)), Rectangle(0, 0, 3, 3));
	ASSERT_EQ(expect(layout.bounds_of(top)), Rectangle(3, 0, 4, 3));
	ASSERT_EQ(expect(layout.bounds_of(top_right)), Rectangle(7, 0, 3, 3));
	ASSERT_EQ(expect(layout.bounds_of(left)), Rectangle(0, 3, 3, 4));
	ASSERT_EQ(expect(layout.bounds_of(right)), Rectangle(7, 3, 3, 4));
	ASSERT_EQ(expect(layout.bounds_of(bottom_left)), Rectangle(0, 7, 3, 3));
	ASSERT_EQ(expect(layout.bounds_of(bottom)), Rectangle(3, 7, 4, 3));
	ASSERT_EQ(expect(layout.bounds_of(bottom_right)), Rectangle(7, 7, 3, 3));
	ASSERT_EQ(expect(layout.bounds_of(center)), Rectangle(3, 3, 4, 4));
}

TEST(AnchorLayout, reasonably_resolved_conflicting_anchors) { // NOLINT(cert-err58-cpp)
	auto container = Component::create<Container>();
	auto &layout = container.use_layout<AnchorLayout>();
	container.set_bounds({10, 10, 10, 10});

	/*
	Anchor the top left of a center component to the left of the bottom right,
	then anchor the bottom right with the top left. This inverts the component.
	The result should be a non-inverted, positive-size rectangle.
	[====]
       $$
	  [====]
	*/

	auto &left = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(left, Anchor::TOP_LEFT, container, Anchor::TOP_LEFT));
	ASSERT_TRUE(layout.set_point(left, Anchor::BOTTOM_RIGHT, container, Anchor::TOP_LEFT, {6, 6}));

	auto &right = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(right, Anchor::BOTTOM_RIGHT, container, Anchor::BOTTOM_RIGHT));
	ASSERT_TRUE(layout.set_point(right, Anchor::TOP_LEFT, container, Anchor::BOTTOM_RIGHT, {-6, -6}));

	auto &middle = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.set_point(middle, Anchor::TOP_LEFT, right, Anchor::TOP_LEFT));
	ASSERT_TRUE(layout.set_point(middle, Anchor::BOTTOM_RIGHT, left, Anchor::BOTTOM_RIGHT));

	layout.redo(container);
	ASSERT_EQ(expect(layout.bounds_of(left)), Rectangle(0, 0, 6, 6));
	ASSERT_EQ(expect(layout.bounds_of(right)), Rectangle(4, 4, 6, 6));
	const auto result = expect(layout.bounds_of(middle));
	ASSERT_GT(result.x, 0);
	ASSERT_GT(result.y, 0);
	ASSERT_GT(result.w, 0);
	ASSERT_GT(result.h, 0);
	// the result of this scenario is fairly predictable. may as well enforce it. this can be removed without the test becoming incomplete
	ASSERT_EQ(result, Rectangle(4, 4, 2, 2));
}

TEST(AnchorLayout, anchored_to_unrelated_component) { // NOLINT(cert-err58-cpp)
	auto container = Component::create<Container>();
	auto &layout = container.use_layout<AnchorLayout>();

	auto a = Component::create<Dummy>();
	ASSERT_FALSE(layout.set_point(a, Anchor::CENTER, container, Anchor::CENTER));
}

TEST(AnchorLayout, previous_bugs) { // NOLINT(cert-err58-cpp)
	// I don't remember what caused this
	{
		Window window;
		auto &container = window.host().root();
		auto &layout = container.use_layout<AnchorLayout>();

		auto &button = container.add(Component::create<Button>());
		ASSERT_TRUE(layout.set_point(button, Anchor::TOP_LEFT, container, Anchor::TOP_LEFT, {10, 10}));
		ASSERT_TRUE(layout.set_point(button, Anchor::BOTTOM_RIGHT, container, Anchor::TOP_LEFT, {90, 40}));
		layout.redo(container);

		ASSERT_EQ(expect(layout.bounds_of(button)), Rectangle(10, 10, 80, 30));
	}

	// a bug wherein `b` was not considered to be part of `a` by the layout
	{
		Window window;
		auto &container = window.host().root();
		auto &layout = container.use_layout<AnchorLayout>();

		auto &a = container.add(Component::create<Dummy>());
		layout.set_point(a, Anchor::TOP_LEFT, container, Anchor::TOP_LEFT, {10, 10});
		layout.set_point(a, Anchor::BOTTOM_RIGHT, container, Anchor::TOP_LEFT, {90, 90});

		auto &b = a.add(Component::create<Dummy>());
		ASSERT_TRUE(a.use_layout<AnchorLayout>().fill_parent(b));
	}
}

TEST(AnchorLayout, fill_parent) { // NOLINT(cert-err58-cpp)
	auto container = Component::create<Container>();
	auto &layout = container.use_layout<AnchorLayout>();
	container.set_bounds({1, 2, 3, 4});

	auto &c = container.add(Component::create<Dummy>());
	ASSERT_TRUE(layout.fill_parent(c));
	layout.redo(container);

	ASSERT_EQ(expect(layout.bounds_of(c)), Rectangle(0, 0, 3, 4));
}
