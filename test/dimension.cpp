
#include <format>

#include <gtest/gtest.h>

#include <anchor/unit/dimension.hpp>

using namespace anchor;

TEST(Dimension, operator_eq_neq) { // NOLINT(cert-err58-cpp)
	constexpr Dimension a{1, 1};
	constexpr Dimension b{1, 1};
	constexpr Dimension c{1, 2};
	ASSERT_EQ(a, b);
	ASSERT_NE(b, c);
}

TEST(Dimension, operator_plus_minus) { // NOLINT(cert-err58-cpp)
	constexpr Dimension a{1, 1};
	constexpr Dimension b{2, 3};
	ASSERT_EQ(a + b, Dimension(3, 4));
	ASSERT_EQ(a - b, Dimension(-1, -2));
}

TEST(Dimension, operator_mul_div) { // NOLINT(cert-err58-cpp)
	ASSERT_EQ(Dimension(1, 1) * 5, Dimension(5, 5));
	ASSERT_EQ(Dimension(10, 10) / 2, Dimension(5, 5));
}

TEST(Dimension, format) { // NOLINT(cert-err58-cpp)
	ASSERT_EQ(std::format("{}", Dimension{1, 2}), "Dimension{1, 2}");
}
