
file(GLOB_RECURSE ANCHOR_TEST_HEADERS *.hpp)
file(GLOB_RECURSE ANCHOR_TEST_SOURCES *.cpp)
add_executable(run-tests
	${ANCHOR_TEST_HEADERS}
	${ANCHOR_TEST_SOURCES}
)
target_link_libraries(run-tests PRIVATE
	anchor::anchor
	gtest
	gtest_main
)
