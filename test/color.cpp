
#include <format>

#include <gtest/gtest.h>

#include <anchor/unit/color.hpp>

using namespace anchor;
using TestColor = BasicColor<std::uint8_t>;

TEST(Color, hex_literal) { // NOLINT(cert-err58-cpp)
	constexpr auto literal = 0xaabbccdd_rgba;
	constexpr auto actual = TestColor(literal);
	ASSERT_NEAR(literal.r, actual.r, 1);
	ASSERT_NEAR(literal.g, actual.g, 1);
	ASSERT_NEAR(literal.b, actual.b, 1);
	ASSERT_NEAR(literal.a, actual.a, 1);

	ASSERT_EQ(0xaabbccff_rgba, 0xaabbcc_rgb);
}

TEST(Color, type_converting_constructor) { // NOLINT(cert-err58-cpp)
	{
		constexpr BasicColor<float> f32{1.f, 0.75f, 0.5f, 0.25f};
		constexpr BasicColor<std::uint8_t> u8{f32};
		ASSERT_EQ(u8.r, 255);
		ASSERT_EQ(u8.g, 191);
		ASSERT_EQ(u8.b, 127);
		ASSERT_EQ(u8.a, 63);
		constexpr BasicColor<float> f32_again{u8};
		constexpr auto epsilon = 0.01f;
		ASSERT_NEAR(f32_again.r, 1.f, epsilon);
		ASSERT_NEAR(f32_again.g, 0.75f, epsilon);
		ASSERT_NEAR(f32_again.b, 0.5f, epsilon);
		ASSERT_NEAR(f32_again.a, 0.25f, epsilon);
	}
	{
		constexpr BasicColor<float> f{0x0dff0dff_rgba};
		constexpr BasicColor<float> expectation{13.f / 255, 1.f, 13.f / 255, 1.f};
		constexpr auto epsilon = 0.01f;
		ASSERT_NEAR(f.r, expectation.r, epsilon);
		ASSERT_NEAR(f.g, expectation.g, epsilon);
		ASSERT_NEAR(f.b, expectation.b, epsilon);
		ASSERT_NEAR(f.a, expectation.a, epsilon);
	}
}

TEST(Color, is_transparent) { // NOLINT(cert-err58-cpp)
	ASSERT_TRUE(TestColor(1, 1, 1, 0).is_transparent());
	ASSERT_FALSE(TestColor(1, 1, 1, 1).is_transparent());
}

TEST(Color, as_greyscale) { // NOLINT(cert-err58-cpp)
	constexpr TestColor c{1, 2, 3, 4};
	constexpr auto grey = 2 + 4 + 6;
	ASSERT_EQ(c.as_greyscale(2, 2, 2), TestColor(grey, grey, grey, 4));
}

TEST(Color, operator_eq_neq) { // NOLINT(cert-err58-cpp)
	constexpr TestColor a{1, 1, 1, 1};
	constexpr TestColor b{1, 1, 1, 1};
	constexpr TestColor c{1, 2, 3, 4};
	ASSERT_EQ(a, b);
	ASSERT_NE(b, c);
}

TEST(Color, scale) { // NOLINT(cert-err58-cpp)
	constexpr TestColor c{10, 20, 30, 40};
	ASSERT_EQ(c.scale(1), c);
	ASSERT_EQ(c.scale(2), TestColor(20, 40, 60, 40));
	ASSERT_EQ(c.scale(0.5), TestColor(5, 10, 15, 40));
}

TEST(Color, format) { // NOLINT(cert-err58-cpp)
	ASSERT_EQ(std::format("{}", Color{0, 1, 2, 255}), "Color{0, 1, 2, 255}");
}
