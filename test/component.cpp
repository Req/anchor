
#include <type_traits>
#include <variant>

#include <gtest/gtest.h>

#include <anchor/component.hpp>
#include <anchor/inheritance.hpp>
#include <anchor/platform/window.hpp>

using namespace anchor;

struct EventTester {
	struct Data {
		bool *fired;
	};

	constexpr static EventHandler<Data> event_handler = [](auto, auto &data, auto) {
		*data.fired = true;
		return EventStatus::CONSUMED;
	};
	constexpr static RepaintHandler<Data> repaint_handler = paint_nothing<Data>;
};

[[nodiscard]] static Event junk_event() noexcept {
	return KeyEvent{
		KeyEvent::Type::PRESS,
		KeyEvent::Modifier::NONE,
		0
	};
}

TEST(Component, event_firing) { // NOLINT(cert-err58-cpp)
	bool fired = false;
	auto c = Component::create<EventTester>(&fired);
	c.fire_event(junk_event());
	ASSERT_TRUE(fired);
}

using Dummy = BaseComponent;

TEST(Component, operator_eq_neq) { // NOLINT(cert-err58-cpp)
	auto a = Component::create<Dummy>();
	auto b = Component::create<Dummy>();
	ASSERT_EQ(b, b);
	ASSERT_NE(a, b);
}

TEST(Component, component_at) { // NOLINT(cert-err58-cpp)
	auto container = Component::create<Dummy>();
	container.set_bounds({0, 0, 100, 100});

	auto &panel = container.add(Component::create<Dummy>());
	panel.set_bounds({20, 20, 60, 60});

	auto &square = panel.add(Component::create<Dummy>());
	square.set_bounds({20, 20, 20, 20});

	ASSERT_EQ(container.component_at({-1, -1}), nullptr);
	ASSERT_EQ(container.component_at({1, 1}), &container);
	ASSERT_EQ(container.component_at({21, 21}), &panel);
	ASSERT_EQ(container.component_at({41, 41}), &square);
	ASSERT_EQ(panel.component_at({-1, -1}), nullptr);
	ASSERT_EQ(panel.component_at({1, 1}), &panel);
	ASSERT_EQ(panel.component_at({21, 21}), &square);
	ASSERT_EQ(square.component_at({-1, -1}), nullptr);
	ASSERT_EQ(square.component_at({1, 1}), &square);
}

TEST(Component, add_remove_sets_parent) { // NOLINT(cert-err58-cpp)
	auto container = Component::create<Dummy>();
	auto &c = container.add(Component::create<Dummy>());
	ASSERT_EQ(c.parent(), &container);
	ASSERT_EQ(container.parent(), nullptr);
	auto removed = container.remove(c);
	ASSERT_TRUE(removed.has_value());
	ASSERT_EQ(removed->parent(), nullptr);
	ASSERT_EQ(container.parent(), nullptr);
}

struct DrawCounter {
	struct Data {
		int *counter;
	};

	constexpr static EventHandler<Data> event_handler = propagate_event<Data>;
	constexpr static RepaintHandler<Data> repaint_handler = [](auto &self, auto &data, auto &brush) noexcept {
		*data.counter += 1;
		self.repaint_children(brush);
	};
};

TEST(Component, draws_all_child_components) { // NOLINT(cert-err58-cpp)
	auto counter = 0;
	auto container = Component::create<DrawCounter>(&counter);
	container.add(Component::create<DrawCounter>(&counter));
	auto &b = container.add(Component::create<DrawCounter>(&counter));
	b.add(Component::create<DrawCounter>(&counter));

	[[maybe_unused]] Window need_an_active_gl_context;
	Brush brush;
	container.repaint(brush);
	ASSERT_EQ(counter, 4);
}

struct EventRefuser {
	struct Data {};

	constexpr static EventHandler<Data> event_handler = [](auto, auto, auto) {
		ADD_FAILURE();
		return EventStatus::CONSUMED;
	};
	constexpr static RepaintHandler<Data> repaint_handler = paint_nothing<Data>;
};

TEST(Component, disabling_prevents_events) { // NOLINT(cert-err58-cpp)
	auto c = Component::create<EventRefuser>();
	c.enabled = false;
	c.fire_event(junk_event());
}

struct HatesEvents {
	struct Data {};

	constexpr static RepaintHandler<Data> repaint_handler = paint_nothing<Data>;
	constexpr static EventHandler<Data> event_handler = [](auto, auto, auto) -> EventStatus {
		throw std::runtime_error{"this is an intentional error. ignore it"};
	};
};

TEST(Component, throwing_events_are_caught) { // NOLINT(cert-err58-cpp)
	auto component = Component::create<HatesEvents>();
	ASSERT_NO_THROW(component.fire_event(junk_event()));
}

// static testing

struct X {
	int x;
};

struct Y {
	int y;
};

using A = ExtendRepaint<Dummy, X, paint_nothing<X>>;
using B = ExtendEvent<Dummy, Y, propagate_event<Y>>;
using C = Inherit<A, B>;
static_assert(std::is_same_v<decltype(C::Data::x), int>);
static_assert(std::is_same_v<decltype(C::Data::y), int>);
