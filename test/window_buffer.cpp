
#include <array>
#include <cstdint>
#include <cstring>
#include <format>
#include <stdexcept>
#include <vector>

#include <gtest/gtest.h>

#include <anchor/unit/color.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>

#include "window_buffer.hpp"

using namespace anchor;

WindowBuffer::WindowBuffer(std::vector<std::uint8_t> &&buffer, const Dimension size) : _buffer{std::move(buffer)}, _size{size} {
	if (_buffer.size() != static_cast<std::size_t>(_size.w * _size.h * 4)) {
		throw std::invalid_argument(std::format("WindowBuffer() called with buffer with unexpected size (buffer size = {}, expected size = {} ({} * {} * 4))", _buffer.size(), _size.w * _size.h * 4, _size.w, _size.h));
	}
}

Color WindowBuffer::at(const Point point) const {
	if (!Rectangle{{0, 0}, _size}.contains(point)) {
		throw std::invalid_argument(std::format("WindowBuffer::at called with {} not inside buffer bounds {}", point, _size));
	}

	const auto actual = Point{point.x, _size.h - point.y - 1};
	const auto *index = _buffer.data() + (actual.y * _size.w + actual.x) * 4;
	return {index[0], index[1], index[2], index[3]};
}

bool WindowBuffer::is(const Point point, const Color color) const {
	return at(point) == color;
}

bool WindowBuffer::area_is(const Rectangle area, const Color color) const {
	for (Rectangle::ValueType y = area.y; y < area.y + area.h; ++y) {
		for (Rectangle::ValueType x = area.x; x < area.x + area.w; ++x) {
			if (!is({x, y}, color)) {
				return false;
			}
		}
	}
	return true;
}

bool WindowBuffer::area_contains(const Rectangle area, const Color color) const {
	for (Rectangle::ValueType y = area.y; y < area.y + area.h; ++y) {
		for (Rectangle::ValueType x = area.x; x < area.x + area.w; ++x) {
			if (is({x, y}, color)) {
				return true;
			}
		}
	}
	return false;
}

template<std::size_t N>
[[nodiscard]] static std::vector<std::uint8_t> create_buffer(const std::array<Color, N> &colors) noexcept {
	std::vector<std::uint8_t> buffer(N * 4);
	std::memcpy(buffer.data(), colors.data(), buffer.size());
	return buffer;
}

TEST(WindowBuffer, at) {
	const WindowBuffer wb{create_buffer<9>({
		colors::BLACK, colors::WHITE, colors::RED,
		colors::GREEN, colors::BLUE, colors::CYAN,
		colors::MAGENTA, colors::YELLOW, colors::GREY
	}), {3, 3}};

	ASSERT_EQ(wb.at({0, 0}), colors::MAGENTA);
	ASSERT_EQ(wb.at({1, 0}), colors::YELLOW);
	ASSERT_EQ(wb.at({2, 0}), colors::GREY);
	ASSERT_EQ(wb.at({0, 1}), colors::GREEN);
	ASSERT_EQ(wb.at({1, 1}), colors::BLUE);
	ASSERT_EQ(wb.at({2, 1}), colors::CYAN);
	ASSERT_EQ(wb.at({0, 2}), colors::BLACK);
	ASSERT_EQ(wb.at({1, 2}), colors::WHITE);
	ASSERT_EQ(wb.at({2, 2}), colors::RED);

	ASSERT_THROW(static_cast<void>(wb.at({-1, 0})), std::invalid_argument);
	ASSERT_THROW(static_cast<void>(wb.at({0, 3})), std::invalid_argument);
}

TEST(WindowBuffer, area) {
	const WindowBuffer wb{create_buffer<9>({
		colors::BLACK, colors::BLACK, colors::BLACK,
		colors::BLACK, colors::WHITE, colors::BLACK,
		colors::BLACK, colors::BLACK, colors::BLACK
	}), {3, 3}};

	ASSERT_TRUE(wb.area_is({0, 0, 3, 1}, colors::BLACK));
	ASSERT_FALSE(wb.area_is({0, 0, 2, 2}, colors::BLACK));
	ASSERT_TRUE(wb.area_contains({0, 0, 3, 3}, colors::WHITE));
	ASSERT_FALSE(wb.area_contains({0, 0, 3, 1}, colors::WHITE));

	ASSERT_THROW(static_cast<void>(wb.area_is({-1, 0, 3, 1}, colors::BLACK)), std::invalid_argument);
	ASSERT_THROW(static_cast<void>(wb.area_contains({-1, 0, 3, 1}, colors::BLACK)), std::invalid_argument);
}
