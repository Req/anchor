
#include <gtest/gtest.h>

#include <anchor/util/debug_gl.hpp>
#include <anchor/font.hpp>
#include <anchor/platform/window.hpp>

#include "atlas.hpp"
#include "expect.hpp"

using namespace anchor;

TEST(Font, loads_default_font) { // NOLINT(cert-err58-cpp)
	[[maybe_unused]] Window _;
	auto &atlas = FontAtlas::instance();
	[[maybe_unused]] AtlasGuard atlas_guard;
	const auto *font = atlas.get_default(16);
	ASSERT_NE(font, nullptr);
	glBindTexture(GL_TEXTURE_2D, font->texture()); check_gl_error;
}
