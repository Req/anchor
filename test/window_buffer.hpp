
#pragma once

#include <cstdint>
#include <vector>

#include <anchor/unit/color.hpp>
#include <anchor/unit/dimension.hpp>
#include <anchor/unit/point.hpp>
#include <anchor/unit/rectangle.hpp>

using namespace anchor;

class WindowBuffer {
private:
	std::vector<std::uint8_t> _buffer;
	anchor::Dimension _size;

public:
	explicit WindowBuffer(std::vector<std::uint8_t> &&buffer, Dimension size);

	[[nodiscard]] Color at(Point point) const;
	[[nodiscard]] bool is(Point point, Color color) const;
	[[nodiscard]] bool area_is(Rectangle area, Color color) const;
	[[nodiscard]] bool area_contains(Rectangle, Color color) const;
};
