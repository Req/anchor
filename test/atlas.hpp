
#pragma once

/*
 * Having `FontAtlas` as a singleton causes problems because it results in
 * texture IDs being carried into contexts where they aren't valid. This
 * behavior only exists in the test environment, so my solution is to trash
 * the atlas after every test to force it to reload textures in each context.
 */
struct AtlasGuard {
	constexpr AtlasGuard() noexcept = default;
	~AtlasGuard() noexcept;
};
