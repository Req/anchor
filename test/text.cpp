
#include <cassert>
#include <vector>

#include <gtest/gtest.h>

#include <anchor/component.hpp>
#include <anchor/component/text.hpp>
#include <anchor/font.hpp>
#include <anchor/layout/anchor_layout.hpp>
#include <anchor/platform/window.hpp>
#include <anchor/util/debug_gl.hpp>

#include "atlas.hpp"
#include "expect.hpp"
#include "window_buffer.hpp"

using namespace anchor;

constexpr static auto TEST_CHAR_WIDTH = 10;

template<char Start, char End>
[[nodiscard]] static std::vector<Character> test_alphabet() noexcept {
	static_assert(Start <= ' ' && ' ' <= End);
	static_assert(Start <= 'a' && 'a' <= End);

	std::vector<Character> alphabet{static_cast<size_t>(End - Start), EMPTY_CHARACTER};
	alphabet[' ' - Start].advance = TEST_CHAR_WIDTH;
	alphabet['a' - Start].advance = TEST_CHAR_WIDTH;
	return alphabet;
}

[[nodiscard]] static const Font& test_font() noexcept {
	const static Font font{
		{},
		' ',
		'~',
		test_alphabet<' ', '~'>(),
		18,
		0
	};
	return font;
}

TEST(Text, dynamic_constructors) { // NOLINT(cert-err58-cpp)
	const auto &font = test_font();

	[[maybe_unused]] const auto _ = Text::Data{"foo", font};

	{
		const auto data = Text::Data{"foo", font, Text::Alignment::RIGHT};
		ASSERT_EQ(data.alignment, Text::Alignment::RIGHT);
		ASSERT_EQ(data.wrap, Text::Wrap::NO_WRAP);
	}

	// wrap => alignment
	{
		const auto data = Text::Data{"foo", font, Text::Wrap::WRAP, Text::Alignment::CENTER};
		ASSERT_EQ(data.alignment, Text::Alignment::CENTER);
		ASSERT_EQ(data.wrap, Text::Wrap::WRAP);
	}

	// alignment => wrap, checking that order is irrelevant
	{
		const auto data = Text::Data{"foo", font, Text::Alignment::CENTER, Text::Wrap::WRAP};
		ASSERT_EQ(data.alignment, Text::Alignment::CENTER);
		ASSERT_EQ(data.wrap, Text::Wrap::WRAP);
	}
}

TEST(Text, renders_something) { // NOLINT(cert-err58-cpp)
	Window w;
	auto &atlas = FontAtlas::instance();
	[[maybe_unused]] AtlasGuard atlas_guard;
	const auto *font = atlas.get_default(18);
	ASSERT_NE(font, nullptr);
	// asserting that the texture is okay
	glBindTexture(GL_TEXTURE_2D, font->texture()); check_gl_error;
	glBindTexture(GL_TEXTURE_2D, 0); check_gl_error;

	auto &container = w.host().root();
	auto &layout = container.use_layout<AnchorLayout>();
	const auto &text = container.add(Component::create<Text>("foo", *font));
	layout.fill_parent(text);

	w.redraw();
	w.redraw();
	const auto buffer = WindowBuffer{w.screenshot(), w.get_size()};
	ASSERT_TRUE(buffer.area_contains(text.get_absolute_bounds(), Text::DefaultStyle::TEXT_COLOR));
}

TEST(Text, fitting) { // NOLINT(cert-err58-cpp)
	const auto &font = test_font();

	{
		const std::string_view full =
			"aaaaaaaaaa" // should fit on a single line
			"aaaa aaaaaaaaaa" // should get split on the space
			"aa\naaa"; // should split on the newline
		detail::TextFitter fitter{full, font};

		constexpr auto WIDTH = TEST_CHAR_WIDTH * 10;
		ASSERT_EQ(expect(fitter.next(WIDTH)), std::string_view{"aaaaaaaaaa"});
		ASSERT_EQ(expect(fitter.next(WIDTH)), std::string_view{"aaaa"});
		ASSERT_EQ(expect(fitter.next(WIDTH)), std::string_view{"aaaaaaaaaa"});
		ASSERT_EQ(expect(fitter.next(WIDTH)), std::string_view{"aa"});
		ASSERT_EQ(expect(fitter.next(WIDTH)), std::string_view{"aaa"});
	}

	{
		detail::TextFitter fitter{"a", font};
		ASSERT_EQ(fitter.next(TEST_CHAR_WIDTH).value(), std::string_view{"a"});
	}

	{
		detail::TextFitter fitter{"a", font};
		ASSERT_EQ(fitter.next(TEST_CHAR_WIDTH - 1).value(), std::string_view{"a"});
	}
}
