
### TLDR
___

```c++
Window window;
auto &container = window.host().root();

const auto &button = container.add(Component::create<Button>());
container.use_layout<AnchorLayout>().fill_parent(button);

auto &atlas = FontAtlas::instance();
const auto &arial = atlas.get("Arial", 16);
const auto &text = container.add(Component::create<Text>("Hello World", arial));
button.use_layout<AnchorLayout>().fill_parent(text);

window.loop_redraw();
```

### Components
___

A `Component` is a wrapper for a type-erased instance of a `ComponentSpecialization`. You call `Component::create` with any type that satisfies the `ComponentSpecialization` concept to instantiate a component.

A `ComponentSpecialization` (referred to here as `C`) minimally satisfies the following properties:
- A subtype `C::Data`.
- A static function `repaint_handler` of type `void (Component&, C::Data&, Brush&) noexcept`.
- A static function `event_handler` of type `EventStatus (Component&, C::Data&, const Event&)`.

When you inevitably want your own custom component, all you have to do is write a sufficient specialization:
```c++
struct MyCustomComponent {
	struct Data {};

	static void repaint_handler(Component&, Data&, Brush&) noexcept {}

	static EventStatus event_handler(Component&, Data, const Event&) {}
};

// and then...
auto c = Component::create<MyCustomComponent>();
```

There may only be one owner of a `Component` at any given time. `Component::add` takes ownership and returns a reference, and `Component::remove` takes a reference and returns ownership. Thus, it is idiomatic to add components at the time of their creation:
```c++
auto &new_component = some_component.add(Component::create<SomeSpecialization>());
```

### DSL
___

A few shortcuts are provided to reduce tedium.

Simple implementation of handlers:
```c++
struct Lazy {
	struct Data {};

	constexpr static auto repaint_handler = paint_nothing<Data>;
	constexpr static auto event_handler = propagate_event<Data>;
};
```

Creating specializations out of existing pieces:
```c++
struct ExistingData {};

void some_repaint_handler(Component&, NewData&, Brush&) noexcept {}

using ExistingDataComponent = ExtentRepaint<BaseComponent, ExistingData, some_repaint_handler>;
```

Combining or extending specializations:
```c++
// this is a specialization that calls `Button`'s handlers before `Text`'s handlers
using ButtonWithText = Inherit<Button, Text>;
```

Handling events:
```c++
EventStatus some_event_handler(Component&, Data&, const Event &e) {
	// you can provide multiple handlers to `match_event`
	std::visit(match_event {
		[](const KeyEvent&) {
			return EventStatus::CONSUMED;
		},
		[](const MouseClickEvent&) {
			return EventStatus::CONSUMED;
		},
		// all variants must be handled, but you can ignore them by using the default handler
		propagate_other_events
	}, e);
}
```

### Fonts
___

Cross-platform font loading is problematic, so this library's chosen solution is to not load them. Instead, fonts must be baked and/or manually added to the library in order to be used.

This repository contains a `font-baker` subproject that loads fonts as specified from a manifest and outputs compileable C++ files containing their relevant data. See [the built-in manifest](font-manifest.json) or run `font-baker write-default` for an example manifest.

Invoke the baker using `font-baker path/to/manifest.json`, compile the resulting files along with your project, and (optionally) add the fonts to the `FontAtlas` at runtime:
```c++
auto my_font = anchor::baked_fonts::MyFontName();
// `my_font` is usable and doesn't have to be added to the atlas unless you want to

auto &atlas = FontAtlas::instance();
atlas.add("MyFontName", my_font);
// the font can then be retrieved elsewhere using:
atlas.get("MyFontName", some_integer_size_maybe_16pt);
```
Alternatively, you can manually create your own font. None of the required functionality is private.
